﻿using System;
using System.Collections.Generic;

using Goldfynger.Utils.Parser;

namespace CrcCalculator
{
    internal static class CrcConverter
    {
        public static Dictionary<string, Func<ParseValueContainer[], ParseValueContainer>> Converters { get; } = new Dictionary<string, Func<ParseValueContainer[], ParseValueContainer>>
        {
            { "CRC-32: STM32F1 software impl. with 8-bit input (CRC-32/zlib ).", Crc32.CalculateSoftwareChecksum },
            { "CRC-32: STM32F1 hardware impl. with 32-bit input.", Crc32.CalculateHardwareChecksum },
        };
    }
}
