﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Parser;

namespace CrcCalculator
{
    /// <remarks>
    /// <see href="https://crccalc.com/"/>,
    /// <see href="https://www.scadacore.com/tools/programming-calculators/online-checksum-calculator/"/>,
    /// <see href="https://www.lammertbies.nl/comm/info/crc-calculation"/>
    /// </remarks>
    internal partial class MainWindow : Window
    {
        private static readonly StringParser __digitParser = new StringParser(StringParser.AllowedInputs.Binary | StringParser.AllowedInputs.Decimal | StringParser.AllowedInputs.Hexadecimal);
        private static readonly StringParser __crcParser = new StringParser();


        public MainWindow()
        {
            InitializeComponent();

            cbSelectCrc.ItemsSource = CrcConverter.Converters.Keys;

            cbSelectCrc.SelectedItem = CrcConverter.Converters.Keys.FirstOrDefault();
        }


        private void TbConverterInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            ConvertDigitalInput(tbConverterInput.Text);
        }

        private void CbSelectCrc_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsLoaded)
            {
                return;
            }

            Trace.WriteLine($"CRC type selected: \"{cbSelectCrc.SelectedItem as string}\".");

            ConvertCrcInput(tbCrcInput.Text, CrcConverter.Converters.TryGetValue(cbSelectCrc.SelectedItem as string, out Func<ParseValueContainer[], ParseValueContainer> converter) ? converter : null);
        }

        private void TbCrcInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            ConvertCrcInput(tbCrcInput.Text, CrcConverter.Converters.TryGetValue(cbSelectCrc.SelectedItem as string, out Func<ParseValueContainer[], ParseValueContainer> converter) ? converter : null);
        }


        private void ConvertDigitalInput(string text)
        {
            Trace.WriteLine($"Converter input: \"{text}\".");

            var values = __digitParser.Parse(text);

            if (values == null || values.Count != 1)
            {
                FillConversionTextBoxes();
            }
            else
            {
                Tuple<sbyte, byte> tuple8 = null;
                Tuple<short, ushort> tuple16 = null;
                Tuple<int, uint> tuple32 = null;
                Tuple<long, ulong> tuple64;

                var value = values[0];
                
                switch (value.ValueType)
                {
                    case ParseValueContainer.ValueTypes.U8:
                        var u8 = value.U8Value ?? throw new ApplicationException();
                        var s8 = unchecked((sbyte)u8);
                        tuple8 = new Tuple<sbyte, byte>(s8, u8);
                        tuple16 = new Tuple<short, ushort>(s8, u8);
                        tuple32 = new Tuple<int, uint>(s8, u8);
                        tuple64 = new Tuple<long, ulong>(s8, u8);
                        break;

                    case ParseValueContainer.ValueTypes.U16:
                        var u16 = value.U16Value ?? throw new ApplicationException();
                        var s16 = unchecked((short)u16);
                        tuple16 = new Tuple<short, ushort>(s16, u16);
                        tuple32 = new Tuple<int, uint>(s16, u16);
                        tuple64 = new Tuple<long, ulong>(s16, u16);
                        break;

                    case ParseValueContainer.ValueTypes.U32:
                        var u32 = value.U32Value ?? throw new ApplicationException();
                        var s32 = unchecked((int)u32);
                        tuple32 = new Tuple<int, uint>(s32, u32);
                        tuple64 = new Tuple<long, ulong>(s32, u32);
                        break;

                    case ParseValueContainer.ValueTypes.U64:
                        var u64 = value.U64Value ?? throw new ApplicationException();
                        var s64 = unchecked((long)u64);
                        tuple64 = new Tuple<long, ulong>(s64, u64);
                        break;

                    default:
                        throw new ApplicationException();
                }

                FillConversionTextBoxes(tuple8, tuple16, tuple32, tuple64);
            }
        }

        private void FillConversionTextBoxes(Tuple<sbyte, byte> tuple8 = null, Tuple<short, ushort> tuple16 = null, Tuple<int, uint> tuple32 = null, Tuple<long, ulong> tuple64 = null)
        {
            tbInt8Dec.Text = tuple8?.Item1.ToString();
            tbInt8Hex.Text = tuple8?.Item1.ToString("X2").AddHexHeader();
            tbInt8Bin.Text = tuple8?.Item1.ConvertToBinString().AddBinHeader();

            tbUInt8Dec.Text = tuple8?.Item2.ToString();
            tbUInt8Hex.Text = tuple8?.Item2.ToString("X2").AddHexHeader();
            tbUInt8Bin.Text = tuple8?.Item2.ConvertToBinString().AddBinHeader();

            tbInt16Dec.Text = tuple16?.Item1.ToString();
            tbInt16Hex.Text = tuple16?.Item1.ToString("X4").AddHexHeader();
            tbInt16Bin.Text = tuple16?.Item1.ConvertToBinString().AddBinHeader();

            tbUInt16Dec.Text = tuple16?.Item2.ToString();
            tbUInt16Hex.Text = tuple16?.Item2.ToString("X4").AddHexHeader();
            tbUInt16Bin.Text = tuple16?.Item2.ConvertToBinString().AddBinHeader();

            tbInt32Dec.Text = tuple32?.Item1.ToString();
            tbInt32Hex.Text = tuple32?.Item1.ToString("X8").AddHexHeader();
            tbInt32Bin.Text = tuple32?.Item1.ConvertToBinString().AddBinHeader();

            tbUInt32Dec.Text = tuple32?.Item2.ToString();
            tbUInt32Hex.Text = tuple32?.Item2.ToString("X8").AddHexHeader();
            tbUInt32Bin.Text = tuple32?.Item2.ConvertToBinString().AddBinHeader();

            tbInt64Dec.Text = tuple64?.Item1.ToString();
            tbInt64Hex.Text = tuple64?.Item1.ToString("X16").AddHexHeader();
            tbInt64Bin.Text = tuple64?.Item1.ConvertToBinString().AddBinHeader();

            tbUInt64Dec.Text = tuple64?.Item2.ToString();
            tbUInt64Hex.Text = tuple64?.Item2.ToString("X16").AddHexHeader();
            tbUInt64Bin.Text = tuple64?.Item2.ConvertToBinString().AddBinHeader();
        }


        private void ConvertCrcInput(string text, Func<ParseValueContainer[], ParseValueContainer> converter)
        {
            Trace.WriteLine($"Converter input: \"{text}\".");

            var values = __crcParser.Parse(text);

            if (values == null)
            {
                FillNormalizedTextBox();
                FillCrcTextBox();
            }
            else
            {
                FillNormalizedTextBox(values.ConvertToStringWithSeparator());

                if (converter == null)
                {
                    Trace.WriteLine($"Converter not selected: \"{text}\".");
                    FillCrcTextBox();
                }
                else
                {
                    var crc = converter(values.ToArray());

                    if (crc == null)
                    {
                        Trace.WriteLine($"CRC conversion failed.");
                        FillCrcTextBox();
                    }
                    else
                    {
                        FillCrcTextBox(crc.ToString());
                    }
                }
            }
        }

        private void FillNormalizedTextBox(string normalizedHex = null)
        {
            tbCrcNormalized.Text = normalizedHex;
        }

        private void FillCrcTextBox(string crcHex = null)
        {
            tbCrcHex.Text = crcHex;
        }
    }
}
