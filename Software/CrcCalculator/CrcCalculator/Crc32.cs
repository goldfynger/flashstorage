﻿using System.Collections.Generic;
using System.Text;

using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Parser;
using Goldfynger.Utils.Stm32.Crc32;

namespace CrcCalculator
{
    internal static class Crc32
    {
        public static ParseValueContainer CalculateHardwareChecksum(ParseValueContainer[] values)
        {
            if (values == null)
            {
                return null;
            }

            var list = new List<uint>();

            foreach (var value in values)
            {
                if (value == null)
                {
                    return null;
                }

                switch (value.ValueType)
                {
                    case ParseValueContainer.ValueTypes.U8:
                    case ParseValueContainer.ValueTypes.U16:
                    case ParseValueContainer.ValueTypes.U32:
                        {
                            list.Add(value.U32Value.Value);
                        }
                        break;

                    case ParseValueContainer.ValueTypes.U64:
                        {
                            var u64 = value.U64Value.Value;

                            list.Add((uint)(u64 & uint.MaxValue));
                            list.Add((uint)((u64 >> 32) & uint.MaxValue));
                        }
                        break;

                    case ParseValueContainer.ValueTypes.String:
                        {
                            list.AddRange(Encoding.ASCII.GetBytes(value.String).GetUInt32List());
                        }
                        break;
                }
            }

            return new ParseValueContainer(Crc32Calculator.CalculateHardwareChecksum(list));
        }

        public static ParseValueContainer CalculateSoftwareChecksum(ParseValueContainer[] values)
        {
            if (values == null)
            {
                return null;
            }

            var list = new List<byte>();

            foreach (var value in values)
            {
                if (value == null)
                {
                    return null;
                }

                switch (value.ValueType)
                {
                    case ParseValueContainer.ValueTypes.U8:
                        {
                            list.Add(value.U8Value.Value);
                        }
                        break;

                    case ParseValueContainer.ValueTypes.U16:
                        {
                            var u16 = value.U16Value.Value;

                            list.Add((byte)(u16 & byte.MaxValue));
                            list.Add((byte)((u16 >> 8) & byte.MaxValue));
                        }
                        break;

                    case ParseValueContainer.ValueTypes.U32:
                        {
                            var u32 = value.U32Value.Value;

                            list.Add((byte)(u32 & byte.MaxValue));
                            list.Add((byte)((u32 >> 8) & byte.MaxValue));
                            list.Add((byte)((u32 >> 16) & byte.MaxValue));
                            list.Add((byte)((u32 >> 24) & byte.MaxValue));
                        }
                        break;

                    case ParseValueContainer.ValueTypes.U64:
                        {
                            var u64 = value.U64Value.Value;

                            list.Add((byte)(u64 & byte.MaxValue));
                            list.Add((byte)((u64 >> 8) & byte.MaxValue));
                            list.Add((byte)((u64 >> 16) & byte.MaxValue));
                            list.Add((byte)((u64 >> 24) & byte.MaxValue));
                            list.Add((byte)((u64 >> 32) & byte.MaxValue));
                            list.Add((byte)((u64 >> 40) & byte.MaxValue));
                            list.Add((byte)((u64 >> 48) & byte.MaxValue));
                            list.Add((byte)((u64 >> 56) & byte.MaxValue));
                        }
                        break;

                    case ParseValueContainer.ValueTypes.String:
                        {
                            list.AddRange(Encoding.ASCII.GetBytes(value.String));
                        }
                        break;
                }
            }

            return new ParseValueContainer(Crc32Calculator.CalculateSoftwareChecksum(list));
        }
    }
}
