#include <stdbool.h>
#include <stdint.h>
#include "flash_storage.h"
#include "flash_storage_config.h"
#include "flash_storage_hal.h"

#ifdef FLASH_STORAGE_CONFIG_OS2
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#endif

#ifdef STM32F103xB
    #include "stm32f1xx_hal.h"
    #include "crc32.h"
#else
    #error "Device not specified."
#endif


#ifdef FLASH_STORAGE_CONFIG_HAL

#define FLASH_STORAGE_UINT32_ERASED         ((uint32_t) 0xFFFFFFFF)

#define __FLASH__                           const volatile


#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_ERROR)
    #define FLASH_STORAGE_HAL_ERROR(fmt, args...)   FLASH_STORAGE_TraceFormat(fmt, ##args)
    #define FLASH_STORAGE_HAL_ASSERT_FAILED()       FLASH_STORAGE_AssertFailed(__FILE__, __FUNCTION__, __LINE__)
    #define FLASH_STORAGE_HAL_ASSERT(condition)     do { if (!(condition)) FLASH_STORAGE_HAL_ASSERT_FAILED(); } while (false)
#else
    #define FLASH_STORAGE_HAL_ERROR(fmt, args...)   do { } while (false)
    #define FLASH_STORAGE_HAL_ASSERT_FAILED()       do { } while (false)
    #define FLASH_STORAGE_HAL_ASSERT(condition)     do { } while (false)
#endif

#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_INFO)
    #define FLASH_STORAGE_HAL_INFO(fmt, args...)    FLASH_STORAGE_TraceFormat(fmt, ##args)
#else
    #define FLASH_STORAGE_HAL_INFO(fmt, args...)    do { } while (false)
#endif

#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_TRACE)
    #define FLASH_STORAGE_HAL_TRACE(fmt, args...)   FLASH_STORAGE_TraceFormat(fmt, ##args)
#else
    #define FLASH_STORAGE_HAL_TRACE(fmt, args...)   do { } while (false)
#endif


#define FLASH_STORAGE_HAL_TRACE_FUNCTION_INVOKED()  FLASH_STORAGE_HAL_TraceFunctionInvoked(__FUNCTION__)


extern CRC_HandleTypeDef FLASH_STORAGE_CONFIG_CRC_HANDLE;

#ifdef FLASH_STORAGE_CONFIG_OS2
extern osMutexId_t FLASH_STORAGE_CONFIG_FLASH_MUTEX;
extern osMutexId_t FLASH_STORAGE_CONFIG_CRC32_MUTEX;
extern osSemaphoreId_t FLASH_STORAGE_CONFIG_ERASE_SEMAPHORE;


static volatile bool _eraseError = false;
#endif


static void     FLASH_STORAGE_HAL_TraceFunctionInvoked  (const char *pFunctionName);


static void FLASH_STORAGE_HAL_TraceFunctionInvoked(const char *pFunctionName)
{
#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_TRACE)

    static const char sTraceFunctionInvoked[] = "%s invoked.\r\n";

#endif

    FLASH_STORAGE_HAL_TRACE(sTraceFunctionInvoked, pFunctionName);
}

/* Weak function. */
FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_HAL_Write(uint32_t writeAddress, const volatile uint16_t buffer[], uint_fast16_t bufferLength)
{
    FLASH_STORAGE_HAL_TRACE_FUNCTION_INVOKED();


    bool atPage0 = (writeAddress >= FLASH_STORAGE_CONFIG_PAGE_0_ADDR) && (writeAddress < (FLASH_STORAGE_CONFIG_PAGE_0_ADDR + FLASH_STORAGE_CONFIG_PAGE_SIZE));
    bool atPage1 = (writeAddress >= FLASH_STORAGE_CONFIG_PAGE_1_ADDR) && (writeAddress < (FLASH_STORAGE_CONFIG_PAGE_1_ADDR + FLASH_STORAGE_CONFIG_PAGE_SIZE));

    if (atPage0 && (writeAddress + (bufferLength * sizeof(uint16_t))) > (FLASH_STORAGE_CONFIG_PAGE_0_ADDR + FLASH_STORAGE_CONFIG_PAGE_SIZE))
    {
        FLASH_STORAGE_HAL_ASSERT_FAILED();
    }
    else if (atPage1 && (writeAddress + (bufferLength * sizeof(uint16_t))) > (FLASH_STORAGE_CONFIG_PAGE_1_ADDR + FLASH_STORAGE_CONFIG_PAGE_SIZE))
    {
        FLASH_STORAGE_HAL_ASSERT_FAILED();
    }
    else if (!atPage0 && !atPage1)
    {
        FLASH_STORAGE_HAL_ASSERT_FAILED();
    }


#ifdef FLASH_STORAGE_CONFIG_OS2
    FLASH_STORAGE_HAL_ASSERT(FLASH_STORAGE_CONFIG_FLASH_MUTEX != NULL);
    FLASH_STORAGE_HAL_ASSERT(!osExIsInISR());

    if (osMutexAcquireWait(FLASH_STORAGE_CONFIG_FLASH_MUTEX) != osOK)
    {
        return FLASH_STORAGE_ERR_HAL_WRITE;
    }
    __schedule_barrier();
#endif

    HAL_FLASH_Unlock();

    for (uint_fast16_t idx = 0; idx < bufferLength; idx++)
    {
        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, writeAddress + (idx * sizeof(uint16_t)), buffer[idx]) != HAL_OK)
        {
            HAL_FLASH_Lock();

        #ifdef FLASH_STORAGE_CONFIG_OS2
            __schedule_barrier();
            if (osMutexRelease(FLASH_STORAGE_CONFIG_FLASH_MUTEX) != osOK)
            {
                FLASH_STORAGE_HAL_ASSERT_FAILED();
            }
        #endif

            return FLASH_STORAGE_ERR_HAL_WRITE;
        }

        if (*((__FLASH__ uint16_t *)writeAddress + idx) != buffer[idx])
        {
            HAL_FLASH_Lock();

        #ifdef FLASH_STORAGE_CONFIG_OS2
            __schedule_barrier();
            if (osMutexRelease(FLASH_STORAGE_CONFIG_FLASH_MUTEX) != osOK)
            {
                FLASH_STORAGE_HAL_ASSERT_FAILED();
            }
        #endif

            return FLASH_STORAGE_ERR_HAL_WRITE;
        }
    }

    HAL_FLASH_Lock();

#ifdef FLASH_STORAGE_CONFIG_OS2
    __schedule_barrier();
    if (osMutexRelease(FLASH_STORAGE_CONFIG_FLASH_MUTEX) != osOK)
    {
        FLASH_STORAGE_HAL_ASSERT_FAILED();

        return FLASH_STORAGE_ERR_HAL_WRITE;
    }
#endif


    return FLASH_STORAGE_ERR_OK;
}

/* Weak function. */
FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_HAL_CalculateCrc32(const volatile uint8_t buffer[], uint_fast16_t bufferLength, uint32_t *pCrc32)
{
    FLASH_STORAGE_HAL_TRACE_FUNCTION_INVOKED();


    FLASH_STORAGE_HAL_ASSERT(((uint32_t)buffer % 2) == 0);


#ifdef FLASH_STORAGE_CONFIG_OS2
    FLASH_STORAGE_HAL_ASSERT(FLASH_STORAGE_CONFIG_CRC32_MUTEX != NULL);
    FLASH_STORAGE_HAL_ASSERT(!osExIsInISR());


    if (osMutexAcquireWait(FLASH_STORAGE_CONFIG_CRC32_MUTEX) != osOK)
    {
        return FLASH_STORAGE_ERR_HAL_CRC32;
    }
    __schedule_barrier();
#endif

    *pCrc32 = CRC32_Calculate(&FLASH_STORAGE_CONFIG_CRC_HANDLE, buffer, bufferLength);

#ifdef FLASH_STORAGE_CONFIG_OS2
    __schedule_barrier();
    if (osMutexRelease(FLASH_STORAGE_CONFIG_CRC32_MUTEX) != osOK)
    {
        FLASH_STORAGE_HAL_ASSERT_FAILED();

        return FLASH_STORAGE_ERR_HAL_CRC32;
    }
#endif

    return FLASH_STORAGE_ERR_OK;
}

/* Weak function. */
FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_HAL_ErasePage(uint32_t pageAddress)
{
    FLASH_STORAGE_HAL_TRACE_FUNCTION_INVOKED();


    FLASH_STORAGE_HAL_ASSERT(pageAddress == FLASH_STORAGE_CONFIG_PAGE_1_ADDR || pageAddress == FLASH_STORAGE_CONFIG_PAGE_0_ADDR);

#ifdef FLASH_STORAGE_CONFIG_OS2
    FLASH_STORAGE_HAL_ASSERT(FLASH_STORAGE_CONFIG_FLASH_MUTEX != NULL);
    FLASH_STORAGE_HAL_ASSERT(!osExIsInISR());
    FLASH_STORAGE_HAL_ASSERT(FLASH_STORAGE_CONFIG_ERASE_SEMAPHORE != NULL);
    FLASH_STORAGE_HAL_ASSERT(osSemaphoreGetCount(FLASH_STORAGE_CONFIG_ERASE_SEMAPHORE) == 0);
#endif


    FLASH_EraseInitTypeDef eraseInit =
    {
        .TypeErase = FLASH_TYPEERASE_PAGES,
        .PageAddress = pageAddress,
        .NbPages = 1
    };

#ifdef FLASH_STORAGE_CONFIG_OS2
    if (osMutexAcquireWait(FLASH_STORAGE_CONFIG_FLASH_MUTEX) != osOK)
    {
        return FLASH_STORAGE_ERR_HAL_ERASE;
    }
    __schedule_barrier();

    HAL_FLASH_Unlock();

    if (HAL_FLASHEx_Erase_IT(&eraseInit) != HAL_OK)
    {
        HAL_FLASH_Lock();

        __schedule_barrier();
        if (osMutexRelease(FLASH_STORAGE_CONFIG_FLASH_MUTEX) != osOK)
        {
            FLASH_STORAGE_HAL_ASSERT_FAILED();
        }

        return FLASH_STORAGE_ERR_HAL_ERASE;
    }

    __schedule_barrier();
    if (osSemaphoreAcquireWait(FLASH_STORAGE_CONFIG_ERASE_SEMAPHORE) != osOK)
    {
        HAL_FLASH_Lock();

        __schedule_barrier();
        if (osMutexRelease(FLASH_STORAGE_CONFIG_FLASH_MUTEX) != osOK)
        {
            FLASH_STORAGE_HAL_ASSERT_FAILED();
        }

        return FLASH_STORAGE_ERR_HAL_ERASE;
    }
    __schedule_barrier();

    __memory_changed();
    if (_eraseError)
    {
        HAL_FLASH_Lock();

        __schedule_barrier();
        if (osMutexRelease(FLASH_STORAGE_CONFIG_FLASH_MUTEX) != osOK)
        {
            FLASH_STORAGE_HAL_ASSERT_FAILED();
        }

        return FLASH_STORAGE_ERR_HAL_ERASE;
    }

    HAL_FLASH_Lock();

    __schedule_barrier();
    if (osMutexRelease(FLASH_STORAGE_CONFIG_FLASH_MUTEX) != osOK)
    {
        FLASH_STORAGE_HAL_ASSERT_FAILED();

        return FLASH_STORAGE_ERR_HAL_ERASE;
    }
#else
    uint32_t pageError = 0; // Error page address.

    HAL_FLASH_Unlock();

    if (HAL_FLASHEx_Erase(&eraseInit, &pageError) != HAL_OK)
    {
        HAL_FLASH_Lock();

        return FLASH_STORAGE_ERR_HAL_ERASE;
    }

    HAL_FLASH_Lock();
#endif


    uint32_t currentAddress = pageAddress;
    uint32_t nextPageAddress = pageAddress + FLASH_STORAGE_CONFIG_PAGE_SIZE;

    while (currentAddress < nextPageAddress)
    {
        if (*(__FLASH__ uint32_t *)(currentAddress) != FLASH_STORAGE_UINT32_ERASED)
        {
            return FLASH_STORAGE_ERR_HAL_ERASE;
        }

        currentAddress += sizeof(uint32_t);
    }


    return FLASH_STORAGE_ERR_OK;
}

#ifdef FLASH_STORAGE_CONFIG_OS2

/* Weak function. */
void HAL_FLASH_EndOfOperationCallback(uint32_t ReturnValue)
{
    (void)ReturnValue;

    FLASH_STORAGE_HAL_ASSERT(FLASH_STORAGE_CONFIG_ERASE_SEMAPHORE != NULL);
    FLASH_STORAGE_HAL_ASSERT(osSemaphoreGetCount(FLASH_STORAGE_CONFIG_ERASE_SEMAPHORE) == 0);

    _eraseError = false;
    __force_stores();

    if (osSemaphoreRelease(FLASH_STORAGE_CONFIG_ERASE_SEMAPHORE) != osOK)
    {
        FLASH_STORAGE_HAL_ASSERT_FAILED();
    }
}

/* Weak function. */
void HAL_FLASH_OperationErrorCallback(uint32_t ReturnValue)
{
    (void)ReturnValue;

    FLASH_STORAGE_HAL_ASSERT(FLASH_STORAGE_CONFIG_ERASE_SEMAPHORE != NULL);
    FLASH_STORAGE_HAL_ASSERT(osSemaphoreGetCount(FLASH_STORAGE_CONFIG_ERASE_SEMAPHORE) == 0);

    _eraseError = true;
    __force_stores();

    if (osSemaphoreRelease(FLASH_STORAGE_CONFIG_ERASE_SEMAPHORE) != osOK)
    {
        FLASH_STORAGE_HAL_ASSERT_FAILED();
    }
}

#endif

#endif
