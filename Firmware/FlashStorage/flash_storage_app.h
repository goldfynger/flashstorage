#ifndef __FLASH_STORAGE_APP_H
#define __FLASH_STORAGE_APP_H


#include <stdbool.h>
#include <stdint.h>
#include "flash_storage.h"
#include "flash_storage_config.h"

#ifdef FLASH_STORAGE_CONFIG_OS2
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#endif


FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_APP_Initialise        (void);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_APP_IsInitialised     (bool *pIsInitialized);
#ifdef FLASH_STORAGE_CONFIG_OS2
osStatus_t                  FLASH_STORAGE_APP_OpenTry           (void);
osStatus_t                  FLASH_STORAGE_APP_OpenWait          (void);
bool                        FLASH_STORAGE_APP_IsOpened          (void);
osStatus_t                  FLASH_STORAGE_APP_Close             (void);
#endif

FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_APP_WriteRecord       (FLASH_STORAGE_RecordNameTypeDef *pName, void *pContent, uint_fast8_t contentSize);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_APP_ReadRecord        (FLASH_STORAGE_RecordNameTypeDef *pName, void *pBuffer, size_t bufferSize, uint_fast8_t *pContentSize);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_APP_RemoveRecord      (FLASH_STORAGE_RecordNameTypeDef *pName);

FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_APP_GetRecordsCount   (uint_fast8_t *pCount);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_APP_GetRecordsNames   (FLASH_STORAGE_RecordNameTypeDef *pNamesList, uint_fast8_t namesCount);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_APP_GetRecordSize     (FLASH_STORAGE_RecordNameTypeDef *pName, uint_fast8_t *pContentSize);


#endif /* __FLASH_STORAGE_APP_H */
