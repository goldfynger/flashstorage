/*

Based on AN2594 "EEPROM emulation in STM32F10x microcontrollers"
https://www.st.com/resource/en/application_note/cd00165693-eeprom-emulation-in-stm32f10x-microcontrollers-stmicroelectronics.pdf

Page status combinations:
------------------------------------------------------------------------------------------------------------------------------------------------------------
|    \PAGE0|              ERASED               |              TRANSIENT            |              ACTIVE               |              OTHER                |
|PAGE1\    |                                   |                                   |                                   |                                   |
------------------------------------------------------------------------------------------------------------------------------------------------------------
|          |Initial state of storage.          |Transient state of storage.        |Normal state of storage.           |Invalid state of storage.          |
|ERASED    |Formatting required.               |Page 1 was already erased but      |Page 0 is active page.             |Formatting required.               |
|          |                                   |page 0 still in TRANSIENT state.   |                                   |                                   |
|          |                                   |                                   |                                   |                                   |
|          |1. Format storage.                 |1. Check PAGE_VERSION of page 0.   |1. Check PAGE_VERSION of page 0.   |1. Format storage.                 |
|          |                                   |--UNKNOWN VERSION--                |--UNKNOWN VERSION--                |                                   |
|          |                                   |2. Format storage.                 |2. Format storage.                 |                                   |
|          |                                   |--KNOWN VERSION--                  |--KNOWN VERSION--                  |                                   |
|          |                                   |2.Check PAGE_CRC32 of page 0.      |2.Check PAGE_CRC32 of page 0.      |                                   |
|          |                                   |--PAGE_CRC32 INVALID--             |--PAGE_CRC32 INVALID--             |                                   |
|          |                                   |3. Format storage.                 |3. Format storage.                 |                                   |
|          |                                   |--PAGE_CRC32 VALID--               |--PAGE_CRC32 VALID--               |                                   |
|          |                                   |3. Write PAGE_STATUS ACTIVE        |3. Check and erase page 1 if need. |                                   |
|          |                                   |to page 0.                         |                                   |                                   |
|          |                                   |4. Check and erase page 1 if need. |                                   |                                   |
------------------------------------------------------------------------------------------------------------------------------------------------------------
|          |Transient state of storage.        |Invalid state of storage.          |Transient state of storage.        |Invalid state of storage.          |
|TRANSIENT |Page 0 was already erased but      |Attempt to recovery storage or     |Page 0 is full page and page 1 is  |Attempt to recovery storage or     |
|          |page 1 still in TRANSIENT state.   |format if can not recovery.        |in transient state.                |format if can not recovery.        |
|          |                                   |                                   |                                   |                                   |
|          |1. Check PAGE_VERSION of page 1.   |1. Check both pages PAGE_VERSION.  |1. Check PAGE_VERSION of page 0.   |1. Check PAGE_VERSION of page 1.   |
|          |--UNKNOWN VERSION--                |--UNKNOWN VERSION ON BOTH PAGES--  |--UNKNOWN VERSION--                |--UNKNOWN VERSION--                |
|          |2. Format storage.                 |2. Format storage.                 |2. Format storage.                 |2. Format storage.                 |
|          |--KNOWN VERSION--                  |--KNOWN VERSION OF PAGE 0(1)--     |--KNOWN VERSION--                  |--KNOWN VERSION--                  |
|          |2.Check PAGE_CRC32 of page 1.      |2.Check PAGE_CRC32 of page 0(1).   |2.Check PAGE_CRC32 of page 0.      |2.Check PAGE_CRC32 of page 1.      |
|          |--PAGE_CRC32 INVALID--             |--PAGE_CRC32 INVALID--             |--PAGE_CRC32 INVALID--             |--PAGE_CRC32 INVALID--             |
|          |3. Format storage.                 |3. Format storage.                 |3. Format storage.                 |3. Format storage.                 |
|          |--PAGE_CRC32 VALID--               |--PAGE_CRC32 VALID--               |--PAGE_CRC32 VALID--               |--PAGE_CRC32 VALID--               |
|          |3. Write PAGE_STATUS ACTIVE        |3. Write PAGE_STATUS ACTIVE        |3. Check and erase page 1 if need. |3. Write PAGE_STATUS ACTIVE        |
|          |to page 1.                         |to page 0(1).                      |                                   |to page 1.                         |
|          |4. Check and erase page 0 if need. |4. Check and erase page 1(0)       |                                   |4. Check and erase page 0 if need. |
|          |                                   |if need.                           |                                   |                                   |
------------------------------------------------------------------------------------------------------------------------------------------------------------
|          |Normal state of storage.           |Transient state of storage.        |Invalid state of storage.          |Invalid state of storage.          |
|ACTIVE    |Page 1 is active page.             |Page 1 is full page and page 0 is  |Attempt to recovery storage or     |Attempt to recovery storage or     |
|          |                                   |in transient state.                |format if can not recovery.        |format if can not recovery.        |
|          |                                   |                                   |                                   |                                   |
|          |1. Check PAGE_VERSION of page 1.   |1. Check PAGE_VERSION of page 1.   |1. Check both pages PAGE_VERSION.  |1. Check PAGE_VERSION of page 1.   |
|          |--UNKNOWN VERSION--                |--UNKNOWN VERSION--                |--UNKNOWN VERSION ON BOTH PAGES--  |--UNKNOWN VERSION--                |
|          |2. Format storage.                 |2. Format storage.                 |2. Format storage.                 |2. Format storage.                 |
|          |--KNOWN VERSION--                  |--KNOWN VERSION--                  |-KNOWN VERSION OF PAGE 0(1)--      |--KNOWN VERSION--                  |
|          |2.Check PAGE_CRC32 of page 1.      |2.Check PAGE_CRC32 of page 1.      |2.Check PAGE_CRC32 of page 0(1).   |2.Check PAGE_CRC32 of page 1.      |
|          |--PAGE_CRC32 INVALID--             |--PAGE_CRC32 INVALID--             |--PAGE_CRC32 INVALID--             |--PAGE_CRC32 INVALID--             |
|          |3. Format storage.                 |3. Format storage.                 |3. Format storage.                 |3. Format storage.                 |
|          |--PAGE_CRC32 VALID--               |--PAGE_CRC32 VALID--               |--PAGE_CRC32 VALID--               |--PAGE_CRC32 VALID--               |
|          |3. Check and erase page 0 if need. |3. Check and erase page 0 if need. |3. Check and erase page 1(0)       |3. Check and erase page 0 if need. |
|          |                                   |                                   |if need.                           |                                   |
------------------------------------------------------------------------------------------------------------------------------------------------------------
|          |Invalid state of storage.          |Invalid state of storage.          |Invalid state of storage.          |Invalid state of storage.          |
|OTHER     |Formatting required.               |Attempt to recovery storage or     |Attempt to recovery storage or     |Formatting required.               |
|          |                                   |format if can not recovery.        |format if can not recovery.        |                                   |
|          |                                   |                                   |                                   |                                   |
|          |1. Format storage.                 |1. Check PAGE_VERSION of page 0.   |1. Check PAGE_VERSION of page 0.   |1. Format storage.                 |
|          |                                   |--UNKNOWN VERSION--                |--UNKNOWN VERSION--                |                                   |
|          |                                   |2. Format storage.                 |2. Format storage.                 |                                   |
|          |                                   |--KNOWN VERSION--                  |--KNOWN VERSION--                  |                                   |
|          |                                   |2.Check PAGE_CRC32 of page 0.      |2.Check PAGE_CRC32 of page 0.      |                                   |
|          |                                   |--PAGE_CRC32 INVALID--             |--PAGE_CRC32 INVALID--             |                                   |
|          |                                   |3. Format storage.                 |3. Format storage.                 |                                   |
|          |                                   |--PAGE_CRC32 VALID--               |--PAGE_CRC32 VALID--               |                                   |
|          |                                   |3. Write PAGE_STATUS ACTIVE        |3. Check and erase page 1 if need. |                                   |
|          |                                   |to page 0.                         |                                   |                                   |
|          |                                   |4. Check and erase page 1 if need. |                                   |                                   |
------------------------------------------------------------------------------------------------------------------------------------------------------------

Stable states:
---------------------------- | Check and erase page if need:                        | Used in case of page has status ERASED but not fully erased in fact.
|Initial:                  | | 1. Check is page is fully erased.
|Page 0 status:     ERASED | | 2. Erase page if need.
|Page 0 version:    -----  |
|Page 1 status:     ERASED |
|Page 1 version:    -----  | | Format storage (change state):
---------------------------- | 1. Check and erase both pages if need.               | Failure when incomplete -> both statuses is ERASED, goto 1 again.
        >> FORMAT >>         | 2. Write PAGE_STATUS ACTIVE to page 0.               | Failure when incomplete -> both statuses is ERASED, goto 1 again.
---------------------------- | 3. Write PAGE_VERSION to page 0.                     | Failure when incomplete -> page 0 version invalid, page 1 ERASED, goto 1 again.
|Normal (page 0 active):   | | 4. Write PAGE_CRC32 to page 0.                       | Failure when incomplete -> page 0 CRC invalid, page 1 ERASED, goto 1 again.
|Page 0 status:     ACTIVE |
|Page 0 version:    KNOWN  |
|Page 1 status:     ERASED | | Transfer records from page to page (change state):
|Page 1 version:    -----  | | 1. Check and erase destination page if need.         | Failure when incomplete -> source ACTIVE, destintaion ERASED, goto 1 again.
---------------------------- | 2. Write PAGE_STATUS TRANSIENT to destination page.  | Failure when incomplete -> source ACTIVE, destintaion ERASED, goto 1 again.
       << TRANSFER >>        | 3. Write PAGE_VERSION to destination page.           | Failure when incomplete -> source ACTIVE, destintaion TRANSIENT, goto 1 again.
---------------------------- | 4. Transfer all records from source to destination.  | Failure when incomplete -> source ACTIVE, destintaion TRANSIENT, goto 1 again.
|Normal (page 1 active):   | | 5. Write PAGE_CRC32 to destination page.             | Failure when incomplete -> source ACTIVE, destintaion TRANSIENT, goto 1 again.
|Page 0 status:     ERASED | | 6. Erase source page.                                | Failure when incomplete -> source ACTIVE, destintaion TRANSIENT, goto 1 again.
|Page 0 version:    -----  | | 7. Write PAGE_STATUS ACTIVE to destination page.     | Failure when incomplete -> source ERASED, destintaion TRANSIENT, goto 6 again.
|Page 1 status:     ACTIVE |
|Page 1 version:    KNOWN  |
----------------------------

Memory map example:
---------------------------------------------------------------------------------------------- |
| \  |          0          |          1          |          2          |          3          | | PAGE_ADDRESS: address of fist byte of page.
---------------------------------------------------------------------------------------------- | PAGE_SIZE: size of page in bytes.
| 0  | PAGE_STATUS         | ...                 | ...                 | ...                 | |
------ (0x0000)            - *ChipTemp*          - *25600 m°C*         - *ChipVolt*          - | PAGE_HEADER: first 8 bytes in page.
| 1  | *ACTIVE*            |                     |                     |                     | |   PAGE_STATUS: status of page, 2 bytes (valid values: ERASED, TRANSIENT, ACTIVE).
----------------------------                     --------------------------------------------- |     ERASED: page in initial erased state (0xFFFF).
| 2  | PAGE_VERSION        |                     |                     | RECORD_CONTENT      | |     TRANSIENT: transfer from full to erased page is in process (0xEEEE).
------ (0x0100)            -                     -                     - (0x0CE4)            - |     ACTIVE: normal state of active page.
| 3  | *v1.00*             |                     | RECORD_CRC32        | *3300 mV*           | |   PAGE_VERSION: version in BCD, 2 bytes (0x0100 is v1.00).
-------------------------------------------------- (0x********)        ----------------------- |   PAGE_CRC32: CRC32 of PAGE_ADDRESS, PAGE_SIZE and PAGE_VERSION, 4 bytes.
| 4  |                     |                     | RECORD_CONTENT (4)* |                     | |
------ PAGE_CRC32          -                     -                     -                     - | RECORD_INFO: first 2 bytes of each record.
| 5  | (0x********)        | RECORD_CONTENT      |                     | RECORD_CRC32        | |   RECORD_ID: uinique identifier of record, 1 byte (valid values 0..126).
------ PAGE_ADDRESS   (4)  - (0x0000639C)        ----------------------- (0x********)        - |     If MSB of ID is set (128-254) it is first record with this ID.
| 6  | PAGE_SIZE      (4)  | *25500 m°C*         | RECORD_ID (0x81)    | RECORD_NAME    (8)  | |   RECORD_SIZE: size of RECORD_CONTENT, 1 byte (valid values 2..254 even numbers).
------ PAGE_VERSION   (2)* -                     ----------------------- RECORD_CONTENT (2)* - |
| 7  |                     |                     | RECORD_SIZE (0x02)  |                     | | RECORD_META: 10 bytes that exists only in first record with same ID.
---------------------------------------------------------------------------------------------- |   RECORD_STATUS: status of record with this ID, 2 bytes (valid values: ACTUAL, GARBAGE).
| 8  | RECORD_ID (0x80)    |                     | RECORD_STATUS       | RECORD_ID (0x01)    | |     ACTUAL: ID of this record is in active state (0xCCCC).
----------------------------                     - (0xCCCC)            ----------------------- |     GARBAGE: ID of this record is removed or deprecated or CRC32 is invalid (0x0000).
| 9  | RECORD_SIZE (0x04)  | RECORD_CRC32        | *ACTUAL*            | RECORD_SIZE (0x02)  | |   RECORD_NAME: 8 bytes of name of record with this ID.
---------------------------- (0x********)        --------------------------------------------- |
| 10 | RECORD_STATUS       | RECORD_NAME    (8)  |                     | RECORD_CONTENT      | | RECORD_CONTENT: any content with size equal to RECORD_SIZE.
------ (0xCCCC)            - RECORD_CONTENT (4)* -                     - (0x0D16)            - | RECORD_CRC32: CRC32 of record, 4 bytes.
| 11 | *ACTUAL*            |                     |                     | *3350 mV*           | |   If it is first record with same ID CRC32 includes RECORD_NAME and RECORD_CONTENT.
--------------------------------------------------                     ----------------------- |   If it is following record with same ID CRC32 includes RECORD_CONTENT.
| 12 |                     | RECORD_ID (0x00)    |                     |                     | |
------                     -----------------------                     -                     - | In table on the left:
| 13 |                     | RECORD_SIZE (0x04)  |                     | RECORD_CRC32        | |   PAGE_HEADER at 0-7 bytes.
------                     -----------------------                     - (0x********)        - |   First record with ID 0x80 (0x00 + MSB) (RECORD_NAME = "ChipTemp" and RECORD_CONTENT = *25500 m�C*).
| 14 |                     | RECORD_CONTENT      |                     | RECORD_CONTENT (2)* | |   Following record with same ID 0x00 (RECORD_CONTENT = *25600 m�C* updates first record).
------ RECORD_NAME         - (0x00006400)        - RECORD_NAME         -                     - |   First record with ID 0x81 (0x01 + MSB) (RECORD_NAME = "ChipVolt" and RECORD_CONTENT = *3300 mV*).
| 15 | ...                 | ...                 | ...                 |                     | |   Following record with same ID 0x01 (RECORD_CONTENT = *3350 mV* updates first record).
---------------------------------------------------------------------------------------------- |

CRC32: byte-by-byte, Width: 32, Polynomial: 0x04C11DB7, Init: 0xFFFFFFFF, RefIn: true, RefOut: true, XorOut: 0xFFFFFFFF, Check: 0xCBF43926 (zlib).

*/

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "flash_storage.h"


#define FLASH_STORAGE_UINT16_ERASED         ((uint16_t) 0xFFFF)
#define FLASH_STORAGE_UINT32_ERASED         ((uint32_t) 0xFFFFFFFF)
#define FLASH_STORAGE_INVALID_RECORD_ID     ((uint8_t)  0xFF)
#define FLASH_STORAGE_FIRST_RECORD_ID_FLAG  ((uint8_t)  0x80)
#define FLASH_STORAGE_MIN_RECORD_ID         ((uint8_t)  0x00)
#define FLASH_STORAGE_MAX_RECORD_ID         ((uint8_t)  0x7E)

#define FLASH_STORAGE_CURRENT_VERSION       ((FLASH_STORAGE_VersionTypeDef) FLASH_STORAGE_VERSION_00_10)

#define FLASH_STORAGE_PAGE_STATUS_OFFSET    ((uint32_t) 0)
#define FLASH_STORAGE_PAGE_STATUS_SIZE      ((size_t)   2)
#define FLASH_STORAGE_PAGE_VERSION_OFFSET   ((uint32_t) (FLASH_STORAGE_PAGE_STATUS_OFFSET + FLASH_STORAGE_PAGE_STATUS_SIZE))
#define FLASH_STORAGE_PAGE_VERSION_SIZE     ((size_t)   2)
#define FLASH_STORAGE_PAGE_CRC32_OFFSET     ((uint32_t) (FLASH_STORAGE_PAGE_VERSION_OFFSET + FLASH_STORAGE_PAGE_VERSION_SIZE))
#define FLASH_STORAGE_PAGE_CRC32_SIZE       ((size_t)   4)
#define FLASH_STORAGE_PAGE_HEADER_OFFSET    ((uint32_t) (FLASH_STORAGE_PAGE_STATUS_OFFSET))
#define FLASH_STORAGE_PAGE_HEADER_SIZE      ((size_t)   (FLASH_STORAGE_PAGE_STATUS_SIZE + FLASH_STORAGE_PAGE_VERSION_SIZE + FLASH_STORAGE_PAGE_CRC32_SIZE))

#define FLASH_STORAGE_RECORD_ID_OFFSET      ((uint32_t) 0)
#define FLASH_STORAGE_RECORD_ID_SIZE        ((size_t)   1)
#define FLASH_STORAGE_RECORD_SIZE_OFFSET    ((uint32_t) (FLASH_STORAGE_RECORD_ID_OFFSET + FLASH_STORAGE_RECORD_ID_SIZE))
#define FLASH_STORAGE_RECORD_SIZE_SIZE      ((size_t)   1)
#define FLASH_STORAGE_RECORD_INFO_OFFSET    ((uint32_t) (FLASH_STORAGE_RECORD_ID_OFFSET))
#define FLASH_STORAGE_RECORD_INFO_SIZE      ((size_t)   (FLASH_STORAGE_RECORD_ID_SIZE + FLASH_STORAGE_RECORD_SIZE_SIZE))

#define FLASH_STORAGE_RECORD_STATUS_OFFSET  ((size_t)   (FLASH_STORAGE_RECORD_INFO_OFFSET + FLASH_STORAGE_RECORD_INFO_SIZE))
#define FLASH_STORAGE_RECORD_STATUS_SIZE    ((size_t)   2)
#define FLASH_STORAGE_RECORD_NAME_OFFSET    ((size_t)   (FLASH_STORAGE_RECORD_STATUS_OFFSET + FLASH_STORAGE_RECORD_STATUS_SIZE))
#define FLASH_STORAGE_RECORD_NAME_SIZE      ((size_t)   8)
#define FLASH_STORAGE_RECORD_META_OFFSET    ((uint32_t) (FLASH_STORAGE_RECORD_STATUS_OFFSET))
#define FLASH_STORAGE_RECORD_META_SIZE      ((size_t)   (FLASH_STORAGE_RECORD_STATUS_SIZE + FLASH_STORAGE_RECORD_NAME_SIZE))

#define FLASH_STORAGE_RECORD_CRC32_SIZE     ((size_t)   4)

#define __FLASH__                           const volatile


#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_ERROR)
    #define FLASH_STORAGE_ERROR(fmt, args...)   FLASH_STORAGE_TraceFormat(fmt, ##args)
    #define FLASH_STORAGE_ASSERT_FAILED()       FLASH_STORAGE_AssertFailed(__FILE__, __FUNCTION__, __LINE__)
    #define FLASH_STORAGE_ASSERT(condition)     do { if (!(condition)) FLASH_STORAGE_ASSERT_FAILED(); } while (false)
#else
    #define FLASH_STORAGE_ERROR(fmt, args...)   do { } while (false)
    #define FLASH_STORAGE_ASSERT_FAILED()       do { } while (false)
    #define FLASH_STORAGE_ASSERT(condition)     do { } while (false)
#endif

#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_INFO)
    #define FLASH_STORAGE_INFO(fmt, args...)    FLASH_STORAGE_TraceFormat(fmt, ##args)
#else
    #define FLASH_STORAGE_INFO(fmt, args...)    do { } while (false)
#endif

#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_TRACE)
    #define FLASH_STORAGE_TRACE(fmt, args...)   FLASH_STORAGE_TraceFormat(fmt, ##args)
#else
    #define FLASH_STORAGE_TRACE(fmt, args...)   do { } while (false)
#endif


#define FLASH_STORAGE_TRACE_FUNCTION_INVOKED()      FLASH_STORAGE_TraceFunctionInvoked(__FUNCTION__)
#define FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error) FLASH_STORAGE_ErrorAndReturn(error, __FUNCTION__)


typedef enum
{
    FLASH_STORAGE_PAGE_STATUS_ERASED    = ((uint16_t)   0xFFFF),
    FLASH_STORAGE_PAGE_STATUS_TRANSIENT = ((uint16_t)   0xEEEE),
    FLASH_STORAGE_PAGE_STATUS_ACTIVE    = ((uint16_t)   0x0000),
}
FLASH_STORAGE_PageStatusTypeDef;

typedef enum
{
    FLASH_STORAGE_RECORD_STATUS_ACTUAL  = ((uint16_t)   0xCCCC),
    FLASH_STORAGE_RECORD_STATUS_GARBAGE = ((uint16_t)   0x0000),
}
FLASH_STORAGE_RecordStatusTypeDef;


static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_CheckPageVersionAndCrc32  (FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_PageNumberTypeDef pageNumber, bool *pResult);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_Format                    (FLASH_STORAGE_HandleTypeDef *hFlashStorage);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_WritePageStatus           (FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_PageNumberTypeDef pageNumber, FLASH_STORAGE_PageStatusTypeDef pageStatus);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_ErasePage                 (FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_PageNumberTypeDef pageNumber, bool checkBeforeErase);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_UpdateRecordsInfo         (FLASH_STORAGE_HandleTypeDef *hFlashStorage);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_Transfer                  (FLASH_STORAGE_HandleTypeDef *hFlashStorage);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_GetRecordBaseByName       (FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName, uint32_t *pBaseAddress);
static uint8_t                      FLASH_STORAGE_AddInCache                (FLASH_STORAGE_HandleTypeDef *hFlashStorage, uint32_t baseAddress, uint32_t actualAddress);
static bool                         FLASH_STORAGE_SearchInCacheByBaseAddress(FLASH_STORAGE_HandleTypeDef *hFlashStorage, uint32_t baseAddress, size_t *pCacheIndex);
static bool                         FLASH_STORAGE_SearchInCacheById         (FLASH_STORAGE_HandleTypeDef *hFlashStorage, uint8_t id, size_t *pCacheIndex);
static bool                         FLASH_STORAGE_SearchInCacheByName       (FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName, size_t *pCacheIndex);
static void                         FLASH_STORAGE_ClearCache                (FLASH_STORAGE_HandleTypeDef *hFlashStorage);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_CalculatePageCrc32        (FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_PageNumberTypeDef pageNumber, FLASH_STORAGE_VersionTypeDef pageVersion, uint32_t *pCrc32);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_Write16                   (uint32_t writeAddress, uint16_t value);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_Write32                   (uint32_t writeAddress, uint32_t value);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_WriteFirstRecord          (uint32_t recordAddress, uint8_t recordId, uint8_t recordContentSize, __FLASH__ uint16_t *pName, __FLASH__ uint16_t *pRecordContent);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_WriteFollowingRecord      (uint32_t recordAddress, uint8_t recordId, uint8_t recordContentSize, __FLASH__ uint16_t *pRecordContent);
static bool                         FLASH_STORAGE_IsNameValid               (FLASH_STORAGE_RecordNameTypeDef *pName);
static void                         FLASH_STORAGE_TraceFunctionInvoked      (const char *pFunctionName);
static FLASH_STORAGE_ErrorTypeDef   FLASH_STORAGE_ErrorAndReturn            (FLASH_STORAGE_ErrorTypeDef error, const char *pFunctionName);

#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_ERROR)
static const char                 * FLASH_STORAGE_ErrorToString             (FLASH_STORAGE_ErrorTypeDef error);
#endif


FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_Initialise(FLASH_STORAGE_HandleTypeDef *hFlashStorage)
{
    /* Recover state of flash storage pages and gets ready for operations. */
    /* FLASH_STORAGE_HandleTypeDef.Configuration struct of FLASH_STORAGE_HandleTypeDef must be configured before. */
    /* FLASH_STORAGE_HandleTypeDef.Internal struct will be cleared here. */

    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    if (hFlashStorage == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;
    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;

    if (pConfiguration->PageSize < FLASH_STORAGE_PAGE_MIN_SIZE || pConfiguration->PageSize > FLASH_STORAGE_PAGE_MAX_SIZE || (pConfiguration->PageSize & (pConfiguration->PageSize - 1)) != 0)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    }

    if (pConfiguration->Page0Address == NULL || pConfiguration->Page1Address == NULL || pConfiguration->Page0Address == pConfiguration->Page1Address ||
        ((uint64_t)pConfiguration->Page0Address + pConfiguration->PageSize) > SIZE_MAX || ((uint64_t)pConfiguration->Page1Address + pConfiguration->PageSize) > SIZE_MAX ||
        (pConfiguration->Page0Address < pConfiguration->Page1Address && (pConfiguration->Page0Address + pConfiguration->PageSize) > pConfiguration->Page1Address) ||
        (pConfiguration->Page1Address < pConfiguration->Page0Address && (pConfiguration->Page1Address + pConfiguration->PageSize) > pConfiguration->Page0Address) ||
        (pConfiguration->Page0Address % 2) != 0 || (pConfiguration->Page1Address % 2) != 0)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    }

    if (pConfiguration->Cache == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    }

    if (pConfiguration->CacheSize == 0U)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    }


    memset(pInternal, 0, sizeof(struct __FLASH_STORAGE_InternalTypeDef));


    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_ERR_OK;

    pInternal->ActivePage = FLASH_STORAGE_PAGE_NONE;

    __FLASH__ FLASH_STORAGE_PageStatusTypeDef *pPage0Status = (__FLASH__ FLASH_STORAGE_PageStatusTypeDef *)(pConfiguration->Page0Address + FLASH_STORAGE_PAGE_STATUS_OFFSET);
    __FLASH__ FLASH_STORAGE_PageStatusTypeDef *pPage1Status = (__FLASH__ FLASH_STORAGE_PageStatusTypeDef *)(pConfiguration->Page1Address + FLASH_STORAGE_PAGE_STATUS_OFFSET);

    FLASH_STORAGE_INFO("%s: page 0 at 0x%08X, page 1 at 0x%08X, page size %u.\r\n", __FUNCTION__, pConfiguration->Page0Address, pConfiguration->Page1Address, pConfiguration->PageSize);
    FLASH_STORAGE_INFO("%s: page 0 status 0x%04X, page 1 status 0x%04X.\r\n", __FUNCTION__, *pPage0Status, *pPage1Status);


    switch (*pPage0Status)
    {
        case FLASH_STORAGE_PAGE_STATUS_TRANSIENT:
        {
            switch (*pPage1Status)
            {
                /* Page 0 TRANSIENT | Page 1 TRANSIENT. */
                case FLASH_STORAGE_PAGE_STATUS_TRANSIENT:
                {
                    bool page0Ok = false;
                    bool page1Ok = false;

                    /* 1. Check both pages PAGE_VERSION. */
                    /* 2.Check PAGE_CRC32 of page 0. */
                    if ((error = FLASH_STORAGE_CheckPageVersionAndCrc32(hFlashStorage, FLASH_STORAGE_PAGE_0, &page0Ok)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* 1. Check both pages PAGE_VERSION. */
                    /* 2.Check PAGE_CRC32 of page 1. */
                    if ((error = FLASH_STORAGE_CheckPageVersionAndCrc32(hFlashStorage, FLASH_STORAGE_PAGE_1, &page1Ok)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* --UNKNOWN VERSION ON BOTH PAGES-- */
                    /* --PAGE_CRC32 INVALID-- */
                    if (!page0Ok && !page1Ok)
                    {
                        /* 2. Format storage. */
                        /* 3. Format storage. */
                        if ((error = FLASH_STORAGE_Format(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                        {
                            /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress, ActivePage, updated in FLASH_STORAGE_Format(). */
                            pInternal->IsInitialised = true;
                        }

                        return error;
                    }

                    FLASH_STORAGE_PageNumberTypeDef activePage = FLASH_STORAGE_PAGE_NONE;

                    /* --KNOWN VERSION OF PAGE 0-- */
                    /* --PAGE_CRC32 VALID-- */
                    if (page0Ok)
                    {
                        activePage = FLASH_STORAGE_PAGE_0;
                    }
                    /* --KNOWN VERSION OF PAGE 1-- */
                    /* --PAGE_CRC32 VALID-- */
                    else
                    {
                        activePage = FLASH_STORAGE_PAGE_1;
                    }

                    /* 3. Write PAGE_STATUS ACTIVE to page 0(1). */
                    if ((error = FLASH_STORAGE_WritePageStatus(hFlashStorage, activePage, FLASH_STORAGE_PAGE_STATUS_ACTIVE)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* 4. Check and erase page 1(0) if need. */
                    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, ((activePage == FLASH_STORAGE_PAGE_0) ? FLASH_STORAGE_PAGE_1 : FLASH_STORAGE_PAGE_0), false)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    pInternal->ActivePage = activePage;

                    if ((error = FLASH_STORAGE_UpdateRecordsInfo(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                    {
                        /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress updated in FLASH_STORAGE_UpdateRecordsInfo(). */
                        pInternal->IsInitialised = true;
                    }

                    return error;
                }


                /* Page 0 TRANSIENT | Page 1 ACTIVE. */
                case FLASH_STORAGE_PAGE_STATUS_ACTIVE:
                {
                    bool page1Ok = false;

                    /* 1. Check PAGE_VERSION of page 1. */
                    /* 2.Check PAGE_CRC32 of page 1. */
                    if ((error = FLASH_STORAGE_CheckPageVersionAndCrc32(hFlashStorage, FLASH_STORAGE_PAGE_1, &page1Ok)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* --UNKNOWN VERSION-- */
                    /* --PAGE_CRC32 INVALID-- */
                    if (!page1Ok)
                    {
                        /* 2. Format storage. */
                        /* 3. Format storage. */
                        if ((error = FLASH_STORAGE_Format(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                        {
                            /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress, ActivePage, updated in FLASH_STORAGE_Format(). */
                            pInternal->IsInitialised = true;
                        }
                    }

                    /* --KNOWN VERSION-- */
                    /* --PAGE_CRC32 VALID-- */
                    /* 3. Check and erase page 0 if need. */
                    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, FLASH_STORAGE_PAGE_0, false)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    pInternal->ActivePage = FLASH_STORAGE_PAGE_1;

                    if ((error = FLASH_STORAGE_UpdateRecordsInfo(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                    {
                        /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress updated in FLASH_STORAGE_UpdateRecordsInfo(). */
                        pInternal->IsInitialised = true;
                    }

                    return error;
                }


                /* Page 0 TRANSIENT | Page 1 ERASED. */
                case FLASH_STORAGE_PAGE_STATUS_ERASED:
                /* No break. */

                /* Page 0 TRANSIENT | Page 1 INVALID. */
                default:
                {
                    bool page0Ok = false;

                    /* 1. Check PAGE_VERSION of page 0. */
                    /* 2. Check PAGE_CRC32 of page 0. */
                    if ((error = FLASH_STORAGE_CheckPageVersionAndCrc32(hFlashStorage, FLASH_STORAGE_PAGE_0, &page0Ok)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* --UNKNOWN VERSION-- */
                    /* --PAGE_CRC32 INVALID-- */
                    if (!page0Ok)
                    {
                        /* 2. Format storage. */
                        /* 3. Format storage. */
                        if ((error = FLASH_STORAGE_Format(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                        {
                            /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress, ActivePage, updated in FLASH_STORAGE_Format(). */
                            pInternal->IsInitialised = true;
                        }
                    }

                    /* --KNOWN VERSION-- */
                    /* --PAGE_CRC32 VALID-- */
                    /* 3. Write PAGE_STATUS ACTIVE to page 0. */
                    if ((error = FLASH_STORAGE_WritePageStatus(hFlashStorage, FLASH_STORAGE_PAGE_0, FLASH_STORAGE_PAGE_STATUS_ACTIVE)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* 4. Check and erase page 1 if need. */
                    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, FLASH_STORAGE_PAGE_1, true)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    pInternal->ActivePage = FLASH_STORAGE_PAGE_0;

                    if ((error = FLASH_STORAGE_UpdateRecordsInfo(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                    {
                        /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress updated in FLASH_STORAGE_UpdateRecordsInfo(). */
                        pInternal->IsInitialised = true;
                    }

                    return error;
                }
            }
        }


        case FLASH_STORAGE_PAGE_STATUS_ACTIVE:
        {
            switch (*pPage1Status)
            {
                /* Page 0 ACTIVE | Page 1 TRANSIENT. */
                case FLASH_STORAGE_PAGE_STATUS_TRANSIENT:
                {
                    bool page0Ok = false;

                    /* 1. Check PAGE_VERSION of page 0. */
                    /* 2. Check PAGE_CRC32 of page 0. */
                    if ((error = FLASH_STORAGE_CheckPageVersionAndCrc32(hFlashStorage, FLASH_STORAGE_PAGE_0, &page0Ok)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* --UNKNOWN VERSION-- */
                    /* --PAGE_CRC32 INVALID-- */
                    if (!page0Ok)
                    {
                        /* 2. Format storage. */
                        /* 3. Format storage. */
                        if ((error = FLASH_STORAGE_Format(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                        {
                            /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress, ActivePage, updated in FLASH_STORAGE_Format(). */
                            pInternal->IsInitialised = true;
                        }
                    }

                    /* --KNOWN VERSION-- */
                    /* --PAGE_CRC32 VALID-- */
                    /* 3. Check and erase page 1 if need. */
                    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, FLASH_STORAGE_PAGE_1, false)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    pInternal->ActivePage = FLASH_STORAGE_PAGE_0;

                    if ((error = FLASH_STORAGE_UpdateRecordsInfo(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                    {
                        /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress updated in FLASH_STORAGE_UpdateRecordsInfo(). */
                        pInternal->IsInitialised = true;
                    }

                    return error;
                }


                /* Page 0 ACTIVE | Page 1 ACTIVE. */
                case FLASH_STORAGE_PAGE_STATUS_ACTIVE:
                {
                    bool page0Ok = false;
                    bool page1Ok = false;

                    /* 1. Check both pages PAGE_VERSION. */
                    /* 2.Check PAGE_CRC32 of page 0. */
                    if ((error = FLASH_STORAGE_CheckPageVersionAndCrc32(hFlashStorage, FLASH_STORAGE_PAGE_0, &page0Ok)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* 1. Check both pages PAGE_VERSION. */
                    /* 2.Check PAGE_CRC32 of page 1. */
                    if ((error = FLASH_STORAGE_CheckPageVersionAndCrc32(hFlashStorage, FLASH_STORAGE_PAGE_1, &page1Ok)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* --UNKNOWN VERSION ON BOTH PAGES-- */
                    /* --PAGE_CRC32 INVALID-- */
                    if (!page1Ok && !page0Ok)
                    {
                        /* 2. Format storage. */
                        /* 3. Format storage. */
                        if ((error = FLASH_STORAGE_Format(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                        {
                            /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress, ActivePage, updated in FLASH_STORAGE_Format(). */
                            pInternal->IsInitialised = true;
                        }

                        return error;
                    }

                    FLASH_STORAGE_PageNumberTypeDef activePage = FLASH_STORAGE_PAGE_NONE;

                    /* --KNOWN VERSION OF PAGE 0-- */
                    /* --PAGE_CRC32 VALID-- */
                    if (page0Ok)
                    {
                        activePage = FLASH_STORAGE_PAGE_0;
                    }
                    /* --KNOWN VERSION OF PAGE 1-- */
                    /* --PAGE_CRC32 VALID-- */
                    else
                    {
                        activePage = FLASH_STORAGE_PAGE_1;
                    }

                    /* 3. Check and erase page 1(0) if need. */
                    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, ((activePage == FLASH_STORAGE_PAGE_0) ? FLASH_STORAGE_PAGE_1 : FLASH_STORAGE_PAGE_0), false)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    pInternal->ActivePage = activePage;

                    if ((error = FLASH_STORAGE_UpdateRecordsInfo(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                    {
                        /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress updated in FLASH_STORAGE_UpdateRecordsInfo(). */
                        pInternal->IsInitialised = true;
                    }

                    return error;
                }


                /* Page 0 ACTIVE | Page 1 ERASED. */
                case FLASH_STORAGE_PAGE_STATUS_ERASED:
                /* No break. */

                /* Page 0 ACTIVE | Page 1 INVALID. */
                default:
                {
                    bool page0Ok = false;

                    /* 1. Check PAGE_VERSION of page 0. */
                    /* 2. Check PAGE_CRC32 of page 0. */
                    if ((error = FLASH_STORAGE_CheckPageVersionAndCrc32(hFlashStorage, FLASH_STORAGE_PAGE_0, &page0Ok)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* --UNKNOWN VERSION-- */
                    /* --PAGE_CRC32 INVALID-- */
                    if (!page0Ok)
                    {
                        /* 2. Format storage. */
                        /* 3. Format storage. */
                        if ((error = FLASH_STORAGE_Format(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                        {
                            /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress, ActivePage, updated in FLASH_STORAGE_Format(). */
                            pInternal->IsInitialised = true;
                        }
                    }

                    /* --KNOWN VERSION-- */
                    /* --PAGE_CRC32 VALID-- */
                    /* 3. Check and erase page 1 if need. */
                    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, FLASH_STORAGE_PAGE_1, true)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    pInternal->ActivePage = FLASH_STORAGE_PAGE_0;

                    if ((error = FLASH_STORAGE_UpdateRecordsInfo(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                    {
                        /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress updated in FLASH_STORAGE_UpdateRecordsInfo(). */
                        pInternal->IsInitialised = true;
                    }

                    return error;
                }
            }
        }


        case FLASH_STORAGE_PAGE_STATUS_ERASED:
        /* No break. */

        default:
        {
            switch (*pPage1Status)
            {
                /* Page 0 ERASED | Page 1 TRANSIENT. */
                case FLASH_STORAGE_PAGE_STATUS_TRANSIENT:
                {
                    bool page1Ok = false;

                    /* 1. Check PAGE_VERSION of page 1. */
                    /* 2.Check PAGE_CRC32 of page 1. */
                    if ((error = FLASH_STORAGE_CheckPageVersionAndCrc32(hFlashStorage, FLASH_STORAGE_PAGE_1, &page1Ok)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* --UNKNOWN VERSION-- */
                    /* --PAGE_CRC32 INVALID-- */
                    if (!page1Ok)
                    {
                        /* 2. Format storage. */
                        /* 3. Format storage. */
                        if ((error = FLASH_STORAGE_Format(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                        {
                            /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress, ActivePage, updated in FLASH_STORAGE_Format(). */
                            pInternal->IsInitialised = true;
                        }
                    }

                    /* --KNOWN VERSION-- */
                    /* --PAGE_CRC32 VALID-- */
                    /* 3. Write PAGE_STATUS ACTIVE to page 1. */
                    if ((error = FLASH_STORAGE_WritePageStatus(hFlashStorage, FLASH_STORAGE_PAGE_1, FLASH_STORAGE_PAGE_STATUS_ACTIVE)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* 4. Check and erase page 0 if need. */
                    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, FLASH_STORAGE_PAGE_0, true)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    pInternal->ActivePage = FLASH_STORAGE_PAGE_1;

                    if ((error = FLASH_STORAGE_UpdateRecordsInfo(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                    {
                        /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress updated in FLASH_STORAGE_UpdateRecordsInfo(). */
                        pInternal->IsInitialised = true;
                    }

                    return error;
                }


                /* Page 0 ERASED | Page 1 ACTIVE. */
                case FLASH_STORAGE_PAGE_STATUS_ACTIVE:
                {
                    bool page1Ok = false;

                    /* 1. Check PAGE_VERSION of page 1. */
                    /* 2.Check PAGE_CRC32 of page 1. */
                    if ((error = FLASH_STORAGE_CheckPageVersionAndCrc32(hFlashStorage, FLASH_STORAGE_PAGE_1, &page1Ok)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    /* --UNKNOWN VERSION-- */
                    /* --PAGE_CRC32 INVALID-- */
                    if (!page1Ok)
                    {
                        /* 2. Format storage. */
                        /* 3. Format storage. */
                        if ((error = FLASH_STORAGE_Format(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                        {
                            /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress, ActivePage, updated in FLASH_STORAGE_Format(). */
                            pInternal->IsInitialised = true;
                        }
                    }

                    /* --KNOWN VERSION-- */
                    /* --PAGE_CRC32 VALID-- */
                    /* 3. Check and erase page 0 if need. */
                    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, FLASH_STORAGE_PAGE_0, true)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }

                    pInternal->ActivePage = FLASH_STORAGE_PAGE_1;

                    if ((error = FLASH_STORAGE_UpdateRecordsInfo(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                    {
                        /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress updated in FLASH_STORAGE_UpdateRecordsInfo(). */
                        pInternal->IsInitialised = true;
                    }

                    return error;
                }


                /* Page 0 ERASED | Page 1 ERASED. */
                case FLASH_STORAGE_PAGE_STATUS_ERASED:
                /* No break. */

                /* Page 0 ERASED | Page 1 INVALID. */
                default:
                {
                    /* 1. Format storage. */
                    if ((error = FLASH_STORAGE_Format(hFlashStorage)) == FLASH_STORAGE_ERR_OK)
                    {
                        /* CacheIndex, RecordsCount, NextRecordId, EmptyAddress, ActivePage, updated in FLASH_STORAGE_Format(). */
                        pInternal->IsInitialised = true;
                    }

                    return error;
                }
            }
        }
    }
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_IsInitialised(FLASH_STORAGE_HandleTypeDef *hFlashStorage, bool *pIsInitialised)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    if (hFlashStorage == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    *pIsInitialised = hFlashStorage->Internal.IsInitialised;

    return FLASH_STORAGE_ERR_OK;
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_WriteRecord(FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName, void *pContent, uint_fast8_t contentSize)
{
    /* Valid content size is even (% 2 = 0) uint8_t values (2..254). */

    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    if (hFlashStorage == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (pName == NULL || !FLASH_STORAGE_IsNameValid(pName))
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (pContent == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (contentSize == 0U || contentSize > UINT8_MAX || (contentSize % 2) != 0)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;
    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;

    if (!pInternal->IsInitialised)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
    }


    FLASH_STORAGE_ASSERT(pConfiguration->PageSize != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page0Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page1Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Cache != NULL);
    FLASH_STORAGE_ASSERT(pInternal->ActivePage == FLASH_STORAGE_PAGE_0 || pInternal->ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_ASSERT(pInternal->EmptyAddress != NULL);
    FLASH_STORAGE_ASSERT(pInternal->EmptyAddress % 2 == 0);


    FLASH_STORAGE_INFO("%s: name \"%.*s\" size %u.\r\n", __FUNCTION__, FLASH_STORAGE_RECORD_NAME_SIZE, pName, contentSize);

    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_ERR_OK;

    size_t pageSize = pConfiguration->PageSize;
    FLASH_STORAGE_RecordDescriptorTypeDef *pCache = pConfiguration->Cache;

    uint32_t pageAddress = pInternal->ActivePage == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;
    uint32_t nextPageAddress = pageAddress + pageSize;
    uint32_t recordAddress = pInternal->EmptyAddress;

    bool nameFound = false;
    uint32_t existingRecordBaseAddress = NULL;
    size_t fullRecordSize = FLASH_STORAGE_RECORD_INFO_SIZE + FLASH_STORAGE_RECORD_META_SIZE + contentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;

    /* We should search for records with same name. If existing record with same name has another size, we will remove old record here. */

    if (pInternal->RecordsCount != 0)
    {
        switch (error = FLASH_STORAGE_GetRecordBaseByName(hFlashStorage, pName, &existingRecordBaseAddress))
        {
            case FLASH_STORAGE_ERR_OK:
                nameFound = true;
                break;

            case FLASH_STORAGE_ERR_RECORD_NOT_FOUND:
                nameFound = false;
                break;

            default:
                return error;
        }

        if (nameFound)
        {
            uint8_t existingRecordContentSize = *(__FLASH__ uint8_t *)(existingRecordBaseAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET);

            /* New record with same name (overwrite). */
            if (existingRecordContentSize != contentSize)
            {
                FLASH_STORAGE_INFO("%s: found same name with different size %u: remove.\r\n", __FUNCTION__, existingRecordContentSize);

                if ((error = FLASH_STORAGE_RemoveRecord(hFlashStorage, pName)) != FLASH_STORAGE_ERR_OK)
                {
                    FLASH_STORAGE_ASSERT(error != FLASH_STORAGE_ERR_RECORD_NOT_FOUND);

                    return error;
                }

                nameFound = false;
            }
            else
            {
                fullRecordSize -= FLASH_STORAGE_RECORD_META_SIZE;
            }
        }
    }

    /* If there is no place in active page, or there is no free ID anymore, we should transfer data to another page. */
    /* If record with same name and size found, we will transfer existing record at new page before writing new value. */

    if ((recordAddress + fullRecordSize) > nextPageAddress || (!nameFound && pInternal->NextRecordId > FLASH_STORAGE_MAX_RECORD_ID))
    {
        FLASH_STORAGE_INFO("%s: not enough space: transfer.\r\n", __FUNCTION__);

        if ((error = FLASH_STORAGE_Transfer(hFlashStorage)) != FLASH_STORAGE_ERR_OK)
        {
            return error;
        }

        pageAddress = pInternal->ActivePage == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;
        nextPageAddress = pageAddress + pageSize;
        recordAddress = pInternal->EmptyAddress; /* EmptyAddress updated in FLASH_STORAGE_Transfer function. */

        if ((recordAddress + fullRecordSize) > nextPageAddress)
        {
            return FLASH_STORAGE_ERR_NOT_ENOUGH_SPACE;
        }

        if (!nameFound && pInternal->NextRecordId > FLASH_STORAGE_MAX_RECORD_ID)
        {
            return FLASH_STORAGE_ERR_NOT_ENOUGH_ID;
        }

        if (pInternal->RecordsCount != 0)
        {
            error = FLASH_STORAGE_GetRecordBaseByName(hFlashStorage, pName, &existingRecordBaseAddress);

            if (error != FLASH_STORAGE_ERR_OK && error != FLASH_STORAGE_ERR_RECORD_NOT_FOUND)
            {
                return error;
            }
        }
    }

    /* Write new record - "following" if record with same name and size already exists or "first" otherwise. */

    if (nameFound)
    {
        uint8_t recordId = ((*(__FLASH__ uint8_t *)(existingRecordBaseAddress + FLASH_STORAGE_RECORD_ID_OFFSET)) & (~FLASH_STORAGE_FIRST_RECORD_ID_FLAG));

        if ((error = FLASH_STORAGE_WriteFollowingRecord(recordAddress, recordId, contentSize, (uint16_t *)pContent)) != FLASH_STORAGE_ERR_OK)
        {
            return error;
        }

        size_t cacheIndex = 0;

        if (FLASH_STORAGE_SearchInCacheByBaseAddress(hFlashStorage, existingRecordBaseAddress, &cacheIndex))
        {
            pCache[cacheIndex].Actual = (__FLASH__ void *)recordAddress;
        }

        pInternal->EmptyAddress += fullRecordSize;
    }
    else
    {
        uint8_t recordId = pInternal->NextRecordId;

        __align(2) FLASH_STORAGE_RecordNameTypeDef nameCopy; /* __align(2) copy to use as pointer to 16-bit value further. */
        strncpy(nameCopy.Name, pName->Name, FLASH_STORAGE_RECORD_NAME_SIZE); /* Clear all chars after '\0'. */

        if ((error = FLASH_STORAGE_WriteFirstRecord(recordAddress, recordId, contentSize, (uint16_t *)&nameCopy, (uint16_t *)pContent)) != FLASH_STORAGE_ERR_OK)
        {
            return error;
        }

        FLASH_STORAGE_AddInCache(hFlashStorage, recordAddress, recordAddress);

        pInternal->RecordsCount += 1;
        pInternal->NextRecordId += 1;
        pInternal->EmptyAddress += fullRecordSize;
    }

    return FLASH_STORAGE_ERR_OK;
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_ReadRecord(FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName, void *pBuffer, size_t bufferSize, uint_fast8_t *pContentSize)
{
    /* Copies record with specified name to buffer. */

    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    if (hFlashStorage == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (pName == NULL || !FLASH_STORAGE_IsNameValid(pName))
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (pBuffer == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (bufferSize == 0U)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (pContentSize == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;
    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;

    if (!pInternal->IsInitialised)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
    }

    if (pInternal->RecordsCount == 0)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_EMPTY);
    }


    FLASH_STORAGE_ASSERT(pConfiguration->PageSize != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page0Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page1Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Cache != NULL);
    FLASH_STORAGE_ASSERT(pInternal->ActivePage == FLASH_STORAGE_PAGE_0 || pInternal->ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_ASSERT(pInternal->EmptyAddress != NULL);
    FLASH_STORAGE_ASSERT(pInternal->EmptyAddress % 2 == 0);


    FLASH_STORAGE_INFO("%s: name \"%.*s\".\r\n", __FUNCTION__, FLASH_STORAGE_RECORD_NAME_SIZE, pName);

    FLASH_STORAGE_RecordDescriptorTypeDef *pCache = pConfiguration->Cache;

    size_t cacheIndex = 0;

    /* Record can be found in cache. */

    if (FLASH_STORAGE_SearchInCacheByName(hFlashStorage, pName, &cacheIndex))
    {
        *pContentSize = *(__FLASH__ uint8_t *)((uint32_t)(pCache[cacheIndex].Actual) + FLASH_STORAGE_RECORD_SIZE_OFFSET);

        if (*pContentSize > bufferSize)
        {
            return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
        }

        uint32_t recordContentOffset = FLASH_STORAGE_RECORD_INFO_SIZE + (pCache[cacheIndex].Actual == pCache[cacheIndex].Base ? FLASH_STORAGE_RECORD_META_SIZE : 0);

        for (uint8_t contentIndex = 0; contentIndex < *pContentSize; contentIndex++)
        {
            ((uint8_t *)pBuffer)[contentIndex] = ((__FLASH__ uint8_t *)((uint32_t)(pCache[cacheIndex].Actual) + recordContentOffset))[contentIndex];
        }

        return FLASH_STORAGE_ERR_OK;
    }


    uint32_t pageAddress = pInternal->ActivePage == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;
    uint32_t recordAddress = pageAddress + FLASH_STORAGE_PAGE_HEADER_SIZE;

    uint8_t targetRecordId = FLASH_STORAGE_INVALID_RECORD_ID;
    size_t targetRecordCacheIdx = NULL;

    /* There is no record with specified name in cache. We should iterate over all existing records to find actual value of record. */

    while (recordAddress < pInternal->EmptyAddress)
    {
        uint8_t recordId = *(__FLASH__ uint8_t *)(recordAddress + FLASH_STORAGE_RECORD_ID_OFFSET);
        uint8_t recordContentSize = *(__FLASH__ uint8_t *)(recordAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET);

        FLASH_STORAGE_ASSERT((recordId | FLASH_STORAGE_FIRST_RECORD_ID_FLAG) != FLASH_STORAGE_INVALID_RECORD_ID);
        FLASH_STORAGE_ASSERT(recordContentSize != 0 && (recordContentSize % 2) == 0);

        if (recordId & FLASH_STORAGE_FIRST_RECORD_ID_FLAG) /* Record in this iteration is "first" record. */
        {
            recordId &= (~FLASH_STORAGE_FIRST_RECORD_ID_FLAG);

            FLASH_STORAGE_ASSERT(targetRecordId != recordId); /* If "first" record found, all another records must have different identifiers. If not, "first" records must have not invalid ID. */

            FLASH_STORAGE_RecordStatusTypeDef recordStatus = *(__FLASH__ FLASH_STORAGE_RecordStatusTypeDef *)(recordAddress + FLASH_STORAGE_RECORD_STATUS_OFFSET);

            if (recordStatus == FLASH_STORAGE_RECORD_STATUS_ACTUAL)
            {
                if (!FLASH_STORAGE_SearchInCacheByBaseAddress(hFlashStorage, recordAddress, &cacheIndex)) /* We already checked cache so we can skip record if it is in cache. */
                {
                    bool equal = true;

                    for (uint8_t charIndex = 0; charIndex < FLASH_STORAGE_RECORD_NAME_SIZE; charIndex++)
                    {
                        char searchChar = pName->Name[charIndex];
                        char compareChar = *(__FLASH__ char *)(recordAddress + FLASH_STORAGE_RECORD_NAME_OFFSET + charIndex);

                        if (searchChar != compareChar)
                        {
                            equal = false;
                            break;
                        }

                        if (searchChar == '\0')
                        {
                            break;
                        }
                    }

                    if (equal)
                    {
                        FLASH_STORAGE_ASSERT(targetRecordId == FLASH_STORAGE_INVALID_RECORD_ID);
                        FLASH_STORAGE_ASSERT(targetRecordCacheIdx == NULL);

                        *pContentSize = recordContentSize;

                        if (*pContentSize > bufferSize)
                        {
                            return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
                        }

                        targetRecordId = recordId;

                        targetRecordCacheIdx = FLASH_STORAGE_AddInCache(hFlashStorage, recordAddress, recordAddress);
                    }
                }
            }

            recordAddress += FLASH_STORAGE_RECORD_INFO_SIZE + FLASH_STORAGE_RECORD_META_SIZE + recordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;

            FLASH_STORAGE_ASSERT(recordAddress <= pInternal->EmptyAddress);
        }
        else /* Record in this iteration is "following" record. */
        {
            /* Update cache is this is "following" record of target record. */

            if (recordId == targetRecordId)
            {
                pCache[targetRecordCacheIdx].Actual = (__FLASH__ void *)recordAddress;
            }

            recordAddress += FLASH_STORAGE_RECORD_INFO_SIZE + recordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;

            FLASH_STORAGE_ASSERT(recordAddress <= pInternal->EmptyAddress);
        }
    }

    if (targetRecordId == FLASH_STORAGE_INVALID_RECORD_ID)
    {
        return FLASH_STORAGE_ERR_RECORD_NOT_FOUND;
    }


    uint32_t recordContentOffset = FLASH_STORAGE_RECORD_INFO_SIZE + (pCache[targetRecordCacheIdx].Actual == pCache[targetRecordCacheIdx].Base ? FLASH_STORAGE_RECORD_META_SIZE : 0);

    for (uint8_t contentIndex = 0; contentIndex < *pContentSize; contentIndex++)
    {
        ((uint8_t *)pBuffer)[contentIndex] = ((__FLASH__ uint8_t *)((uint32_t)(pCache[targetRecordCacheIdx].Actual) + recordContentOffset))[contentIndex];
    }

    return FLASH_STORAGE_ERR_OK;
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_RemoveRecord(FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName)
{
    /* Searches for record with specified name and mark record as removed. */

    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    if (hFlashStorage == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (pName == NULL || !FLASH_STORAGE_IsNameValid(pName))
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;
    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;

    if (!pInternal->IsInitialised)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
    }

    if (pInternal->RecordsCount == 0)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_EMPTY);
    }


    FLASH_STORAGE_ASSERT(pConfiguration->Cache != NULL);


    FLASH_STORAGE_INFO("%s: name \"%.*s\".\r\n", __FUNCTION__, FLASH_STORAGE_RECORD_NAME_SIZE, pName);

    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_ERR_OK;

    uint32_t recordBaseAddress = NULL;

    if ((error = FLASH_STORAGE_GetRecordBaseByName(hFlashStorage, pName, &recordBaseAddress)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    if ((error = FLASH_STORAGE_Write16(recordBaseAddress + FLASH_STORAGE_RECORD_STATUS_OFFSET, (uint16_t)FLASH_STORAGE_RECORD_STATUS_GARBAGE)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }

    pInternal->RecordsCount -= 1;


    FLASH_STORAGE_RecordDescriptorTypeDef *pCache = pConfiguration->Cache;

    size_t cacheIndex = 0;

    /* Remove record from cache. */

    if (FLASH_STORAGE_SearchInCacheByBaseAddress(hFlashStorage, recordBaseAddress, &cacheIndex))
    {
        pCache[cacheIndex].Base = NULL;
        pCache[cacheIndex].Actual = NULL;
    }


    return FLASH_STORAGE_ERR_OK;
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_GetRecordsCount(FLASH_STORAGE_HandleTypeDef *hFlashStorage, uint_fast8_t *pCount)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    if (hFlashStorage == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (pCount == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;

    if (!pInternal->IsInitialised)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
    }


    *pCount = pInternal->RecordsCount;

    FLASH_STORAGE_INFO("%s: %u.\r\n", __FUNCTION__, pInternal->RecordsCount);

    return FLASH_STORAGE_ERR_OK;
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_GetRecordsNames(FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pNamesList, uint_fast8_t namesCount)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    if (hFlashStorage == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (pNamesList == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;
    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;

    if (namesCount != pInternal->RecordsCount)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (!pInternal->IsInitialised)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
    }

    if (pInternal->RecordsCount == 0)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_EMPTY);
    }


    FLASH_STORAGE_ASSERT(pConfiguration->Page0Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page1Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Cache != NULL);
    FLASH_STORAGE_ASSERT(pInternal->ActivePage == FLASH_STORAGE_PAGE_0 || pInternal->ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_ASSERT(pInternal->EmptyAddress != NULL);
    FLASH_STORAGE_ASSERT(pInternal->EmptyAddress % 2 == 0);


    uint32_t pageAddress = pInternal->ActivePage == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;

    uint8_t nameIndex = 0;
    uint32_t recordAddress = pageAddress + FLASH_STORAGE_PAGE_HEADER_SIZE;

    while (nameIndex < pInternal->RecordsCount)
    {
        uint8_t recordId = *(__FLASH__ uint8_t *)(recordAddress + FLASH_STORAGE_RECORD_ID_OFFSET);
        uint8_t recordContentSize = *(__FLASH__ uint8_t *)(recordAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET);

        FLASH_STORAGE_ASSERT((recordId | FLASH_STORAGE_FIRST_RECORD_ID_FLAG) != FLASH_STORAGE_INVALID_RECORD_ID);
        FLASH_STORAGE_ASSERT(recordContentSize != 0 && (recordContentSize % 2) == 0);

        if (recordId & FLASH_STORAGE_FIRST_RECORD_ID_FLAG)
        {
            recordId &= (~FLASH_STORAGE_FIRST_RECORD_ID_FLAG);

            FLASH_STORAGE_RecordStatusTypeDef recordStatus = *(__FLASH__ FLASH_STORAGE_RecordStatusTypeDef *)(recordAddress + FLASH_STORAGE_RECORD_STATUS_OFFSET);

            if (recordStatus == FLASH_STORAGE_RECORD_STATUS_ACTUAL)
            {
                for (uint8_t charIndex = 0; charIndex < FLASH_STORAGE_RECORD_NAME_SIZE; charIndex++)
                {
                    pNamesList[nameIndex].Name[charIndex] = *(__FLASH__ char *)(recordAddress + FLASH_STORAGE_RECORD_NAME_OFFSET + charIndex);
                }

                nameIndex += 1;
            }

            recordAddress += FLASH_STORAGE_RECORD_INFO_SIZE + FLASH_STORAGE_RECORD_META_SIZE + recordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;

            FLASH_STORAGE_ASSERT(recordAddress <= pInternal->EmptyAddress);
        }
        else
        {
            recordAddress += FLASH_STORAGE_RECORD_INFO_SIZE + recordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;

            FLASH_STORAGE_ASSERT(recordAddress <= pInternal->EmptyAddress);
        }
    }

    return FLASH_STORAGE_ERR_OK;
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_GetRecordSize(FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName, uint_fast8_t *pContentSize)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    if (hFlashStorage == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (pName == NULL || !FLASH_STORAGE_IsNameValid(pName))
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    if (pContentSize == NULL)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_PARAMETER);
    }

    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;

    if (!pInternal->IsInitialised)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
    }

    if (pInternal->RecordsCount == 0)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_EMPTY);
    }


    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_ERR_OK;

    uint32_t recordBaseAddress = NULL;

    if ((error = FLASH_STORAGE_GetRecordBaseByName(hFlashStorage, pName, &recordBaseAddress)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    *pContentSize = *(__FLASH__ uint8_t *)(recordBaseAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET);

    FLASH_STORAGE_INFO("%s: name \"%.*s\" size %u.\r\n", __FUNCTION__, FLASH_STORAGE_RECORD_NAME_SIZE, pName, *pContentSize);

    return FLASH_STORAGE_ERR_OK;
}


static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_Format(FLASH_STORAGE_HandleTypeDef *hFlashStorage)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;
    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page0Address != NULL);


    FLASH_STORAGE_INFO("%s: format both pages.\r\n", __FUNCTION__);

    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_ERR_OK;

    uint32_t pageAddress = pConfiguration->Page0Address;

    /* 1. Check and erase both pages if need. */
    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, FLASH_STORAGE_PAGE_0, true)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }

    /* 1. Check and erase both pages if need. */
    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, FLASH_STORAGE_PAGE_1, true)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }

    /* 2. Write PAGE_STATUS ACTIVE to page 0. */
    if ((error = FLASH_STORAGE_WritePageStatus(hFlashStorage, FLASH_STORAGE_PAGE_0, FLASH_STORAGE_PAGE_STATUS_ACTIVE)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    /* 3. Write PAGE_VERSION to page 0. */
    if ((error = FLASH_STORAGE_Write16(pageAddress + FLASH_STORAGE_PAGE_VERSION_OFFSET, FLASH_STORAGE_CURRENT_VERSION)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    uint32_t crc32 = 0;

    if ((error = FLASH_STORAGE_CalculatePageCrc32(hFlashStorage, FLASH_STORAGE_PAGE_0, FLASH_STORAGE_CURRENT_VERSION, &crc32)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }

    /* 4. Write PAGE_CRC32 to page 0. */
    if ((error = FLASH_STORAGE_Write32(pageAddress + FLASH_STORAGE_PAGE_CRC32_OFFSET, crc32)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }

    FLASH_STORAGE_ClearCache(hFlashStorage);

    pInternal->RecordsCount = 0;
    pInternal->NextRecordId = 0;
    pInternal->ActivePage = FLASH_STORAGE_PAGE_0;
    pInternal->EmptyAddress = pageAddress + FLASH_STORAGE_PAGE_HEADER_SIZE;

    return FLASH_STORAGE_ERR_OK;
}

static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_CheckPageVersionAndCrc32(FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_PageNumberTypeDef pageNumber, bool *pResult)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page0Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page1Address != NULL);
    FLASH_STORAGE_ASSERT(pageNumber == FLASH_STORAGE_PAGE_0 || pageNumber == FLASH_STORAGE_PAGE_1);


    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_ERR_OK;

    *pResult = false;

    uint32_t pageAddress = pageNumber == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;

    if (*(__FLASH__ FLASH_STORAGE_VersionTypeDef *)(pageAddress + FLASH_STORAGE_PAGE_VERSION_OFFSET) != FLASH_STORAGE_CURRENT_VERSION)
    {
        FLASH_STORAGE_INFO("%s: page %u invalid version.\r\n", __FUNCTION__, pageNumber);

        return FLASH_STORAGE_ERR_OK;
    }

    uint32_t crc32 = 0;

    if ((error = FLASH_STORAGE_CalculatePageCrc32(hFlashStorage, pageNumber, FLASH_STORAGE_CURRENT_VERSION, &crc32)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }

    if (*(__FLASH__ uint32_t *)(pageAddress + FLASH_STORAGE_PAGE_CRC32_OFFSET) != crc32)
    {
        FLASH_STORAGE_INFO("%s: page %u invalid CRC32.\r\n", __FUNCTION__, pageNumber);

        return FLASH_STORAGE_ERR_OK;
    }

    *pResult = true;

    return FLASH_STORAGE_ERR_OK;
}

static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_WritePageStatus(FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_PageNumberTypeDef pageNumber, FLASH_STORAGE_PageStatusTypeDef pageStatus)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page0Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page1Address != NULL);
    FLASH_STORAGE_ASSERT(pageNumber == FLASH_STORAGE_PAGE_0 || pageNumber == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_ASSERT(pageStatus == FLASH_STORAGE_PAGE_STATUS_ERASED || pageStatus == FLASH_STORAGE_PAGE_STATUS_TRANSIENT || pageStatus == FLASH_STORAGE_PAGE_STATUS_ACTIVE);


    FLASH_STORAGE_INFO("%s: write status 0x%04X at page %u.\r\n", __FUNCTION__, pageStatus, pageNumber);

    uint32_t pageAddress = pageNumber == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;

    return FLASH_STORAGE_Write16(pageAddress + FLASH_STORAGE_PAGE_STATUS_OFFSET, (uint16_t)pageStatus);
}

static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_ErasePage(FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_PageNumberTypeDef pageNumber, bool checkBeforeErase)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->PageSize != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page0Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page1Address != NULL);
    FLASH_STORAGE_ASSERT(pageNumber == FLASH_STORAGE_PAGE_0 || pageNumber == FLASH_STORAGE_PAGE_1);


    uint32_t pageAddress = pageNumber == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;

    bool eraseNeeded = true;

    if (checkBeforeErase)
    {
        uint32_t currentAddress = pageAddress;
        uint32_t nextPageAddress = pageAddress + pConfiguration->PageSize;

        eraseNeeded = false;

        /* 1. Check is page is fully erased. */
        while (currentAddress < nextPageAddress)
        {
            if (*(__FLASH__ uint32_t *)(currentAddress) != FLASH_STORAGE_UINT32_ERASED)
            {
                eraseNeeded = true;

                break;
            }

            currentAddress += sizeof(uint32_t);
        }
    }

    /* 2. Erase page if need. */
    if (eraseNeeded)
    {
        FLASH_STORAGE_INFO("%s: erase page %u at 0x%08X.\r\n", __FUNCTION__, pageNumber, pageAddress);

        FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_HAL_ErasePage(pageAddress);

        if (error != FLASH_STORAGE_ERR_OK)
        {
            return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
        }
    }

    return FLASH_STORAGE_ERR_OK;
}

static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_UpdateRecordsInfo(FLASH_STORAGE_HandleTypeDef *hFlashStorage)
{
    /* Recovers pages and fills FLASH_STORAGE_HandleTypeDef.Internal struct fields. */

    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;
    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->PageSize != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page0Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page1Address != NULL);
    FLASH_STORAGE_ASSERT(pInternal->ActivePage == FLASH_STORAGE_PAGE_0 || pInternal->ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_ASSERT(pConfiguration->Cache != NULL);


    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_ERR_OK;

    FLASH_STORAGE_RecordDescriptorTypeDef *pCache = pConfiguration->Cache;

    pInternal->CacheIndex = 0;

    uint8_t *pRecordsCount = &pInternal->RecordsCount;
    uint8_t *pNextRecordId = &pInternal->NextRecordId;

    *pRecordsCount = 0;
    *pNextRecordId = 0;

    uint32_t pageAddress = pInternal->ActivePage == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;
    uint32_t nextPageAddress = pageAddress + pConfiguration->PageSize;
    uint32_t recordAddress = pageAddress + FLASH_STORAGE_PAGE_HEADER_SIZE;

    uint32_t emptyAddress = nextPageAddress;

    while (emptyAddress >= recordAddress)
    {
        emptyAddress -= sizeof(uint32_t);

        if (*(__FLASH__ uint32_t *)(emptyAddress) != FLASH_STORAGE_UINT32_ERASED)
        {
            if (*((__FLASH__ uint16_t *)(emptyAddress + sizeof(uint16_t))) == FLASH_STORAGE_UINT16_ERASED)
            {
                emptyAddress += sizeof(uint16_t);
            }
            else
            {
                emptyAddress += sizeof(uint32_t);
            }

            break;
        }
    }

    pInternal->EmptyAddress = emptyAddress;


    FLASH_STORAGE_ClearCache(hFlashStorage);


    while (recordAddress < emptyAddress)
    {
        uint8_t recordId = *(__FLASH__ uint8_t *)(recordAddress + FLASH_STORAGE_RECORD_ID_OFFSET);
        uint8_t recordContentSize = *(__FLASH__ uint8_t *)(recordAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET);

        if ((recordId | FLASH_STORAGE_FIRST_RECORD_ID_FLAG) == FLASH_STORAGE_INVALID_RECORD_ID)
        {
            FLASH_STORAGE_INFO("%s: found invalid ID %u at 0x%08X: transfer.\r\n", __FUNCTION__, recordId, recordAddress + FLASH_STORAGE_RECORD_ID_OFFSET);

            return (error = FLASH_STORAGE_Transfer(hFlashStorage)); /* Corrupted memory starts here. */
        }

        if (recordContentSize == 0 || (recordContentSize % 2) != 0)
        {
            FLASH_STORAGE_INFO("%s: found invalid size %u at 0x%08X: transfer.\r\n", __FUNCTION__, recordContentSize, recordAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET);

            return (error = FLASH_STORAGE_Transfer(hFlashStorage)); /* Corrupted memory starts here. */
        }

        if (recordId & FLASH_STORAGE_FIRST_RECORD_ID_FLAG)
        {
            recordId &= (~FLASH_STORAGE_FIRST_RECORD_ID_FLAG);

            if ((recordAddress + FLASH_STORAGE_RECORD_INFO_SIZE + FLASH_STORAGE_RECORD_META_SIZE + recordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE) > emptyAddress)
            {
                FLASH_STORAGE_INFO("%s: found invalid size %u at 0x%08X: transfer.\r\n", __FUNCTION__, recordContentSize, recordAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET);

                return (error = FLASH_STORAGE_Transfer(hFlashStorage)); /* Corrupted memory starts here. */
            }

            FLASH_STORAGE_RecordStatusTypeDef recordStatus = *(__FLASH__ FLASH_STORAGE_RecordStatusTypeDef *)(recordAddress + FLASH_STORAGE_RECORD_STATUS_OFFSET);

            if (recordStatus == FLASH_STORAGE_RECORD_STATUS_ACTUAL)
            {
                uint32_t crc32BaseAddress = recordAddress + FLASH_STORAGE_RECORD_NAME_OFFSET;
                uint_fast16_t crc32BaseLength = FLASH_STORAGE_RECORD_NAME_SIZE + recordContentSize;

                uint32_t crc32 = 0;

                if ((error = FLASH_STORAGE_HAL_CalculateCrc32((__FLASH__ uint8_t *)(crc32BaseAddress), crc32BaseLength, &crc32)) != FLASH_STORAGE_ERR_OK)
                {
                    return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
                }

                if (*(__FLASH__ uint32_t *)(recordAddress + FLASH_STORAGE_RECORD_INFO_SIZE + FLASH_STORAGE_RECORD_META_SIZE + recordContentSize) == crc32)
                {
                    *pRecordsCount += 1;

                    if (*pNextRecordId <= recordId)
                    {
                        *pNextRecordId = recordId += 1;
                    }

                    FLASH_STORAGE_AddInCache(hFlashStorage, recordAddress, recordAddress);
                }
                else
                {
                    if ((error = FLASH_STORAGE_Write16(recordAddress + FLASH_STORAGE_RECORD_STATUS_OFFSET, (uint16_t)FLASH_STORAGE_RECORD_STATUS_GARBAGE)) != FLASH_STORAGE_ERR_OK)
                    {
                        return error;
                    }
                }
            }
            else if (recordStatus != FLASH_STORAGE_RECORD_STATUS_GARBAGE)
            {
                if ((error = FLASH_STORAGE_Write16(recordAddress + FLASH_STORAGE_RECORD_STATUS_OFFSET, (uint16_t)FLASH_STORAGE_RECORD_STATUS_GARBAGE)) != FLASH_STORAGE_ERR_OK)
                {
                    return error;
                }
            }

            recordAddress += FLASH_STORAGE_RECORD_INFO_SIZE + FLASH_STORAGE_RECORD_META_SIZE + recordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;
        }
        else
        {
            if ((recordAddress + FLASH_STORAGE_RECORD_INFO_SIZE + recordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE) > emptyAddress)
            {
                FLASH_STORAGE_INFO("%s: found invalid size %u at 0x%08X: transfer.\r\n", __FUNCTION__, recordContentSize, recordAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET);

                return (error = FLASH_STORAGE_Transfer(hFlashStorage)); /* Corrupted memory starts here. */
            }

            uint32_t crc32 = 0;

            if ((error = FLASH_STORAGE_HAL_CalculateCrc32((__FLASH__ uint8_t *)(recordAddress + FLASH_STORAGE_RECORD_INFO_SIZE), recordContentSize, &crc32)) != FLASH_STORAGE_ERR_OK)
            {
                return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
            }

            if (*(__FLASH__ uint32_t *)(recordAddress + FLASH_STORAGE_RECORD_INFO_SIZE + recordContentSize) == crc32)
            {
                size_t cacheIndex = 0;

                if (FLASH_STORAGE_SearchInCacheById(hFlashStorage, recordId, &cacheIndex))
                {
                    pCache[cacheIndex].Actual = (__FLASH__ void *)recordAddress;
                }
            }

            recordAddress += FLASH_STORAGE_RECORD_INFO_SIZE + recordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;
        }
    }

    FLASH_STORAGE_INFO("%s: records found %u.\r\n", __FUNCTION__, *pRecordsCount);

    return FLASH_STORAGE_ERR_OK;
}

static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_Transfer(FLASH_STORAGE_HandleTypeDef *hFlashStorage)
{
    /* Transfers valid records from active page to another, makes another page active, and erases previous active page. */
    /* All records has new ID after transfer. */

    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;
    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->PageSize != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page0Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page1Address != NULL);
    FLASH_STORAGE_ASSERT(pInternal->ActivePage == FLASH_STORAGE_PAGE_0 || pInternal->ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_ASSERT(pInternal->EmptyAddress != NULL);


    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_ERR_OK;

    FLASH_STORAGE_PageNumberTypeDef sourcePage = pInternal->ActivePage;
    FLASH_STORAGE_PageNumberTypeDef destinationPage = pInternal->ActivePage == FLASH_STORAGE_PAGE_0 ? FLASH_STORAGE_PAGE_1 : FLASH_STORAGE_PAGE_0;
    uint32_t sourcePageAddress = sourcePage == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;
    uint32_t destinationPageAddress = destinationPage == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;

    FLASH_STORAGE_INFO("%s: src page %u at 0x%08X dst page %u at 0x%08X.\r\n", __FUNCTION__, sourcePage, sourcePageAddress, destinationPage, destinationPageAddress);

    /* 1. Check and erase destination page if need. */
    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, destinationPage, true)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }

    /* 2. Write PAGE_STATUS TRANSIENT to destination page. */
    if ((error = FLASH_STORAGE_WritePageStatus(hFlashStorage, destinationPage, FLASH_STORAGE_PAGE_STATUS_TRANSIENT)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }

    /* 3. Write PAGE_VERSION to destination page. */
    if ((error = FLASH_STORAGE_Write16(destinationPageAddress + FLASH_STORAGE_PAGE_VERSION_OFFSET, FLASH_STORAGE_CURRENT_VERSION)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    FLASH_STORAGE_ClearCache(hFlashStorage);


    uint32_t sourceRecordAddress = sourcePageAddress + FLASH_STORAGE_PAGE_HEADER_SIZE;
    uint32_t destinationRecordAddress = destinationPageAddress + FLASH_STORAGE_PAGE_HEADER_SIZE;

    uint8_t destinationRecordsCount = 0;

    /* 4. Transfer all records from source to destination. */
    for (uint8_t destinationRecordId = 0; destinationRecordId < pInternal->RecordsCount; destinationRecordId++)
    {
        uint8_t targetRecordId = FLASH_STORAGE_INVALID_RECORD_ID;
        uint32_t targetRecordBaseAddress = NULL;
        uint32_t targetRecordActualAddress = NULL;
        uint32_t sourceNextRecordAddress = NULL;

        FLASH_STORAGE_ASSERT(sourceRecordAddress != NULL);

        while (sourceRecordAddress < pInternal->EmptyAddress)
        {
            uint8_t sourceRecordId = *(__FLASH__ uint8_t *)(sourceRecordAddress + FLASH_STORAGE_RECORD_ID_OFFSET);
            uint8_t sourceRecordContentSize = *(__FLASH__ uint8_t *)(sourceRecordAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET);

            if ((sourceRecordId | FLASH_STORAGE_FIRST_RECORD_ID_FLAG) == FLASH_STORAGE_INVALID_RECORD_ID)
            {
                break; /* Corrupted memory starts here. */
            }

            if (sourceRecordContentSize == 0 || (sourceRecordContentSize % 2) != 0)
            {
                break; /* Corrupted memory starts here. */
            }

            if (sourceRecordId & FLASH_STORAGE_FIRST_RECORD_ID_FLAG)
            {
                sourceRecordId &= (~FLASH_STORAGE_FIRST_RECORD_ID_FLAG);

                if ((sourceRecordAddress + FLASH_STORAGE_RECORD_INFO_SIZE + FLASH_STORAGE_RECORD_META_SIZE + sourceRecordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE) > pInternal->EmptyAddress)
                {
                    break; /* Corrupted memory starts here. */
                }

                FLASH_STORAGE_RecordStatusTypeDef recordStatus = *(__FLASH__ FLASH_STORAGE_RecordStatusTypeDef *)(sourceRecordAddress + FLASH_STORAGE_RECORD_STATUS_OFFSET);

                if (recordStatus == FLASH_STORAGE_RECORD_STATUS_ACTUAL)
                {
                    if (targetRecordId == FLASH_STORAGE_INVALID_RECORD_ID)
                    {
                        targetRecordId = sourceRecordId;
                        targetRecordBaseAddress = sourceRecordAddress;
                        targetRecordActualAddress = sourceRecordAddress;
                    }
                    else
                    {
                        if (sourceNextRecordAddress == NULL)
                        {
                            sourceNextRecordAddress = sourceRecordAddress;
                        }
                    }
                }
                else
                {
                    FLASH_STORAGE_ASSERT(recordStatus == FLASH_STORAGE_RECORD_STATUS_GARBAGE);
                }

                sourceRecordAddress += FLASH_STORAGE_RECORD_INFO_SIZE + FLASH_STORAGE_RECORD_META_SIZE + sourceRecordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;

                FLASH_STORAGE_ASSERT(sourceRecordAddress <= pInternal->EmptyAddress);
            }
            else
            {
                if ((sourceRecordAddress + FLASH_STORAGE_RECORD_INFO_SIZE + sourceRecordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE) > pInternal->EmptyAddress)
                {
                    break; /* Corrupted memory starts here. */
                }

                if (targetRecordId == sourceRecordId)
                {
                    FLASH_STORAGE_ASSERT(sourceRecordContentSize == *(__FLASH__ uint8_t *)(targetRecordBaseAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET));

                    uint32_t crc32 = 0;

                    if ((error = FLASH_STORAGE_HAL_CalculateCrc32((__FLASH__ uint8_t *)(sourceRecordAddress + FLASH_STORAGE_RECORD_INFO_SIZE), sourceRecordContentSize, &crc32)) != FLASH_STORAGE_ERR_OK)
                    {
                        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
                    }

                    if (*(__FLASH__ uint32_t *)(sourceRecordAddress + FLASH_STORAGE_RECORD_INFO_SIZE + sourceRecordContentSize) == crc32)
                    {
                        FLASH_STORAGE_ASSERT(*(__FLASH__ uint8_t *)(targetRecordBaseAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET) == sourceRecordContentSize);

                        targetRecordActualAddress = sourceRecordAddress;
                    }
                }

                sourceRecordAddress += FLASH_STORAGE_RECORD_INFO_SIZE + sourceRecordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;
            }
        }

        sourceRecordAddress = sourceNextRecordAddress;


        FLASH_STORAGE_ASSERT(targetRecordId != FLASH_STORAGE_INVALID_RECORD_ID);

        FLASH_STORAGE_ASSERT(targetRecordBaseAddress != NULL);
        FLASH_STORAGE_ASSERT(targetRecordActualAddress != NULL);
        FLASH_STORAGE_ASSERT(*(__FLASH__ uint16_t *)(targetRecordBaseAddress + FLASH_STORAGE_RECORD_INFO_OFFSET) != 0);
        FLASH_STORAGE_ASSERT(*(__FLASH__ uint16_t *)(targetRecordActualAddress + FLASH_STORAGE_RECORD_INFO_OFFSET) != 0);
        FLASH_STORAGE_ASSERT(((*(__FLASH__ uint8_t *)(targetRecordBaseAddress   + FLASH_STORAGE_RECORD_ID_OFFSET)) | FLASH_STORAGE_FIRST_RECORD_ID_FLAG) ==
                             ((*(__FLASH__ uint8_t *)(targetRecordActualAddress + FLASH_STORAGE_RECORD_ID_OFFSET)) | FLASH_STORAGE_FIRST_RECORD_ID_FLAG));
        FLASH_STORAGE_ASSERT(*(__FLASH__ uint8_t *)(targetRecordBaseAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET) == *(__FLASH__ uint8_t *)(targetRecordActualAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET));


        uint8_t destinationRecordContentSize = *(__FLASH__ uint8_t *)(targetRecordBaseAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET);

        __FLASH__ uint16_t *pTargetRecordName = (__FLASH__ uint16_t *)(targetRecordBaseAddress + FLASH_STORAGE_RECORD_NAME_OFFSET);

        uint32_t targetRecordContentAddress = targetRecordActualAddress + FLASH_STORAGE_RECORD_INFO_SIZE + (targetRecordBaseAddress == targetRecordActualAddress ? FLASH_STORAGE_RECORD_META_SIZE : 0);
        __FLASH__ uint16_t *pTargetRecordContent = (__FLASH__ uint16_t *)(targetRecordContentAddress);

        if ((error = FLASH_STORAGE_WriteFirstRecord(destinationRecordAddress, destinationRecordId, destinationRecordContentSize, pTargetRecordName, pTargetRecordContent)) != FLASH_STORAGE_ERR_OK)
        {
            return error;
        }

        FLASH_STORAGE_AddInCache(hFlashStorage, destinationRecordAddress, destinationRecordAddress);

        destinationRecordAddress += FLASH_STORAGE_RECORD_INFO_SIZE + FLASH_STORAGE_RECORD_META_SIZE + destinationRecordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;
        destinationRecordsCount += 1;
    }

    FLASH_STORAGE_ASSERT(pInternal->RecordsCount == destinationRecordsCount);

    uint32_t crc32 = 0;

    if ((error = FLASH_STORAGE_CalculatePageCrc32(hFlashStorage, destinationPage, FLASH_STORAGE_CURRENT_VERSION, &crc32)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }

    /* 5. Write PAGE_CRC32 to destination page. */
    if ((error = FLASH_STORAGE_Write32(destinationPageAddress + FLASH_STORAGE_PAGE_CRC32_OFFSET, crc32)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    /* 6. Erase source page. */
    if ((error = FLASH_STORAGE_ErasePage(hFlashStorage, sourcePage, false)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    /* 7. Write PAGE_STATUS ACTIVE to destination page. */
    if ((error = FLASH_STORAGE_WritePageStatus(hFlashStorage, destinationPage, FLASH_STORAGE_PAGE_STATUS_ACTIVE)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    pInternal->RecordsCount = destinationRecordsCount;
    pInternal->NextRecordId = destinationRecordsCount;
    pInternal->ActivePage = destinationPage;
    pInternal->EmptyAddress = destinationRecordAddress;

    return FLASH_STORAGE_ERR_OK;
}

static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_GetRecordBaseByName(FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName, uint32_t *pBaseAddress)
{
    /* Searches for record base with specified name. */

    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;
    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(pName != NULL);
    FLASH_STORAGE_ASSERT(FLASH_STORAGE_IsNameValid(pName));
    FLASH_STORAGE_ASSERT(pBaseAddress != NULL);
    FLASH_STORAGE_ASSERT(pInternal->RecordsCount != 0);
    FLASH_STORAGE_ASSERT(pConfiguration->Page0Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page1Address != NULL);
    FLASH_STORAGE_ASSERT(pInternal->ActivePage == FLASH_STORAGE_PAGE_0 || pInternal->ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_ASSERT(pConfiguration->Cache != NULL);
    FLASH_STORAGE_ASSERT(pInternal->EmptyAddress != NULL);
    FLASH_STORAGE_ASSERT(pInternal->EmptyAddress % 2 == 0);


    FLASH_STORAGE_RecordDescriptorTypeDef *pCache = pConfiguration->Cache;

    size_t cacheIndex = 0;

    if (FLASH_STORAGE_SearchInCacheByName(hFlashStorage, pName, &cacheIndex))
    {
        *pBaseAddress = (uint32_t)pCache[cacheIndex].Base;

        return FLASH_STORAGE_ERR_OK;
    }


    uint32_t pageAddress = pInternal->ActivePage == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;

    uint8_t nameIndex = 0;
    uint32_t recordAddress = pageAddress + FLASH_STORAGE_PAGE_HEADER_SIZE;

    while (nameIndex < pInternal->RecordsCount)
    {
        uint8_t recordId = *(__FLASH__ uint8_t *)(recordAddress + FLASH_STORAGE_RECORD_ID_OFFSET);
        uint8_t recordContentSize = *(__FLASH__ uint8_t *)(recordAddress + FLASH_STORAGE_RECORD_SIZE_OFFSET);

        FLASH_STORAGE_ASSERT((recordId | FLASH_STORAGE_FIRST_RECORD_ID_FLAG) != FLASH_STORAGE_INVALID_RECORD_ID);
        FLASH_STORAGE_ASSERT(recordContentSize != 0 && (recordContentSize % 2) == 0);

        if (recordId & FLASH_STORAGE_FIRST_RECORD_ID_FLAG)
        {
            recordId &= (~FLASH_STORAGE_FIRST_RECORD_ID_FLAG);

            FLASH_STORAGE_RecordStatusTypeDef recordStatus = *(__FLASH__ FLASH_STORAGE_RecordStatusTypeDef *)(recordAddress + FLASH_STORAGE_RECORD_STATUS_OFFSET);

            if (recordStatus == FLASH_STORAGE_RECORD_STATUS_ACTUAL)
            {
                if (!FLASH_STORAGE_SearchInCacheByBaseAddress(hFlashStorage, recordAddress, &cacheIndex))
                {
                    bool equal = true;

                    for (uint8_t charIndex = 0; charIndex < FLASH_STORAGE_RECORD_NAME_SIZE; charIndex++)
                    {
                        char searchChar = pName->Name[charIndex];
                        char compareChar = *(__FLASH__ char *)(recordAddress + FLASH_STORAGE_RECORD_NAME_OFFSET + charIndex);

                        if (searchChar != compareChar)
                        {
                            equal = false;
                            break;
                        }

                        if (searchChar == '\0')
                        {
                            break;
                        }
                    }

                    if (equal)
                    {
                        *pBaseAddress = recordAddress;

                        return FLASH_STORAGE_ERR_OK;
                    }
                }

                nameIndex += 1;
            }

            recordAddress += FLASH_STORAGE_RECORD_INFO_SIZE + FLASH_STORAGE_RECORD_META_SIZE + recordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;

            FLASH_STORAGE_ASSERT(recordAddress <= pInternal->EmptyAddress);
        }
        else
        {
            recordAddress += FLASH_STORAGE_RECORD_INFO_SIZE + recordContentSize + FLASH_STORAGE_RECORD_CRC32_SIZE;

            FLASH_STORAGE_ASSERT(recordAddress <= pInternal->EmptyAddress);
        }
    }

    return FLASH_STORAGE_ERR_RECORD_NOT_FOUND;
}

static uint8_t FLASH_STORAGE_AddInCache(FLASH_STORAGE_HandleTypeDef *hFlashStorage, uint32_t baseAddress, uint32_t actualAddress)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;
    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Cache != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->CacheSize != NULL);


    FLASH_STORAGE_RecordDescriptorTypeDef *pCache = pConfiguration->Cache;
    size_t cacheSize = pConfiguration->CacheSize;

    size_t *pCacheIndex = &pInternal->CacheIndex;

    pCache[*pCacheIndex].Base = (__FLASH__ void *)baseAddress;
    pCache[*pCacheIndex].Actual = (__FLASH__ void *)actualAddress;

    size_t cacheIndex = *pCacheIndex;

    if (*pCacheIndex < (cacheSize - 1))
    {
        *pCacheIndex += 1;
    }
    else
    {
        *pCacheIndex = 0;
    }

    return cacheIndex;
}

static bool FLASH_STORAGE_SearchInCacheByBaseAddress(FLASH_STORAGE_HandleTypeDef *hFlashStorage, uint32_t baseAddress, size_t *pCacheIndex)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(baseAddress != NULL);
    FLASH_STORAGE_ASSERT(pCacheIndex != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Cache != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->CacheSize != NULL);


    FLASH_STORAGE_RecordDescriptorTypeDef *pCache = pConfiguration->Cache;
    size_t cacheSize = pConfiguration->CacheSize;

    for (size_t idx = 0; idx < cacheSize; idx++)
    {
        if (pCache[idx].Base != NULL)
        {
            FLASH_STORAGE_ASSERT(pCache[idx].Actual != NULL);

            if ((uint32_t)(pCache[idx].Base) == baseAddress)
            {
                *pCacheIndex = idx;

                return true;
            }
        }
    }

    return false;
}

static bool FLASH_STORAGE_SearchInCacheById(FLASH_STORAGE_HandleTypeDef *hFlashStorage, uint8_t id, size_t *pCacheIndex)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(id != FLASH_STORAGE_INVALID_RECORD_ID);
    FLASH_STORAGE_ASSERT(pCacheIndex != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Cache != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->CacheSize != NULL);


    FLASH_STORAGE_RecordDescriptorTypeDef *pCache = pConfiguration->Cache;
    size_t cacheSize = pConfiguration->CacheSize;

    for (size_t idx = 0; idx < cacheSize; idx++)
    {
        if (pCache[idx].Base != NULL)
        {
            FLASH_STORAGE_ASSERT(pCache[idx].Actual != NULL);

            if (((*(__FLASH__ uint8_t *)((uint32_t)(pCache[idx].Base) + FLASH_STORAGE_RECORD_ID_OFFSET)) & (~FLASH_STORAGE_FIRST_RECORD_ID_FLAG)) == id)
            {
                *pCacheIndex = idx;

                return true;
            }
        }
    }

    return false;
}

static bool FLASH_STORAGE_SearchInCacheByName(FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName, size_t *pCacheIndex)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(pName != NULL);
    FLASH_STORAGE_ASSERT(FLASH_STORAGE_IsNameValid(pName));
    FLASH_STORAGE_ASSERT(pCacheIndex != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Cache != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->CacheSize != NULL);


    FLASH_STORAGE_RecordDescriptorTypeDef *pCache = pConfiguration->Cache;
    size_t cacheSize = pConfiguration->CacheSize;

    for (size_t idx = 0; idx < cacheSize; idx++)
    {
        if (pCache[idx].Base != NULL)
        {
            FLASH_STORAGE_ASSERT(pCache[idx].Actual != NULL);

            bool equal = true;

            for (uint8_t charIndex = 0; charIndex < FLASH_STORAGE_RECORD_NAME_SIZE; charIndex++)
            {
                char searchChar = pName->Name[charIndex];
                char compareChar = *(__FLASH__ char *)((uint32_t)(pCache[idx].Base) + FLASH_STORAGE_RECORD_NAME_OFFSET + charIndex);

                if (searchChar != compareChar)
                {
                    equal = false;
                    break;
                }

                if (searchChar == '\0')
                {
                    break;
                }
            }

            if (equal)
            {
                *pCacheIndex = idx;

                return true;
            }
        }
    }

    return false;
}

static void FLASH_STORAGE_ClearCache(FLASH_STORAGE_HandleTypeDef *hFlashStorage)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;
    struct __FLASH_STORAGE_InternalTypeDef *pInternal = &hFlashStorage->Internal;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Cache != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->CacheSize != NULL);


    FLASH_STORAGE_RecordDescriptorTypeDef *pCache = pConfiguration->Cache;
    size_t cacheSize = pConfiguration->CacheSize;

    for (size_t idx = 0; idx < cacheSize; idx++)
    {
        pCache[idx].Base = NULL;
        pCache[idx].Actual = NULL;
    }

    pInternal->CacheIndex = 0;
}

static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_CalculatePageCrc32(FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_PageNumberTypeDef pageNumber, FLASH_STORAGE_VersionTypeDef pageVersion, uint32_t *pCrc32)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();

    struct __FLASH_STORAGE_ConfigurationTypeDef *pConfiguration = &hFlashStorage->Configuration;


    FLASH_STORAGE_ASSERT(hFlashStorage != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->PageSize != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page0Address != NULL);
    FLASH_STORAGE_ASSERT(pConfiguration->Page1Address != NULL);
    FLASH_STORAGE_ASSERT(pageVersion == FLASH_STORAGE_CURRENT_VERSION);
    FLASH_STORAGE_ASSERT(pageNumber == FLASH_STORAGE_PAGE_0 || pageNumber == FLASH_STORAGE_PAGE_1);


    size_t pageSize = pConfiguration->PageSize;
    uint32_t pageAddress = pageNumber == FLASH_STORAGE_PAGE_0 ? pConfiguration->Page0Address : pConfiguration->Page1Address;


    __align(4) uint8_t buffer[sizeof(uint32_t) + sizeof(size_t) + sizeof(FLASH_STORAGE_VersionTypeDef)]; /* __align(4) to use as pointer to 32-bit value further. */
    const uint_fast16_t bufferLength = sizeof(uint32_t) + sizeof(size_t) + sizeof(FLASH_STORAGE_VersionTypeDef);


    *(uint32_t *)buffer = pageAddress;
    *(size_t *)(buffer + sizeof(uint32_t)) = pageSize;
    *(FLASH_STORAGE_VersionTypeDef *)(buffer + sizeof(uint32_t) + sizeof(size_t)) = pageVersion;


    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_HAL_CalculateCrc32(buffer, bufferLength, pCrc32);

    if (error != FLASH_STORAGE_ERR_OK)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
    }

    return FLASH_STORAGE_ERR_OK;
}


static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_Write16(uint32_t writeAddress, uint16_t value)
{
    uint16_t buffer = value;

    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_HAL_Write(writeAddress, &buffer, (sizeof(uint16_t)/sizeof(uint16_t)));

    if (error != FLASH_STORAGE_ERR_OK)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
    }

    FLASH_STORAGE_ASSERT(*(__FLASH__ uint16_t *)writeAddress == value);

    return FLASH_STORAGE_ERR_OK;
}

static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_Write32(uint32_t writeAddress, uint32_t value)
{
    uint32_t buffer = value;

    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_HAL_Write(writeAddress, (uint16_t *)&buffer, (sizeof(uint32_t)/sizeof(uint16_t)));

    if (error != FLASH_STORAGE_ERR_OK)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
    }

    FLASH_STORAGE_ASSERT(*(__FLASH__ uint32_t *)writeAddress == value);

    return FLASH_STORAGE_ERR_OK;
}

static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_WriteFirstRecord(uint32_t recordAddress, uint8_t recordId, uint8_t recordContentSize, __FLASH__ uint16_t *pName, __FLASH__ uint16_t *pRecordContent)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();


    FLASH_STORAGE_ASSERT(recordAddress != NULL);
    FLASH_STORAGE_ASSERT((recordId | FLASH_STORAGE_FIRST_RECORD_ID_FLAG) != FLASH_STORAGE_INVALID_RECORD_ID);
    FLASH_STORAGE_ASSERT(recordContentSize != 0 && (recordContentSize % 2) == 0);
    FLASH_STORAGE_ASSERT(pName != NULL);
    FLASH_STORAGE_ASSERT(pRecordContent != NULL);


    FLASH_STORAGE_INFO("%s: at 0x%08X.\r\n", __FUNCTION__, recordAddress);

    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_ERR_OK;

    /* Write RECORD_INFO (RECORD_ID and RECORD_SIZE). */
    uint16_t recordInfo = recordContentSize;
    recordInfo <<= 8;
    recordInfo += (recordId | FLASH_STORAGE_FIRST_RECORD_ID_FLAG);

    if ((error = FLASH_STORAGE_Write16(recordAddress + FLASH_STORAGE_RECORD_INFO_OFFSET, recordInfo)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    /* Write RECORD_STATUS ACTUAL. */
    if ((error = FLASH_STORAGE_Write16(recordAddress + FLASH_STORAGE_RECORD_STATUS_OFFSET, (uint16_t)FLASH_STORAGE_RECORD_STATUS_ACTUAL)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    /* Write RECORD_NAME. */
    if ((error = FLASH_STORAGE_HAL_Write(recordAddress + FLASH_STORAGE_RECORD_NAME_OFFSET, (__FLASH__ uint16_t *)pName, (FLASH_STORAGE_RECORD_NAME_SIZE/sizeof(uint16_t)))) != FLASH_STORAGE_ERR_OK)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
    }


    /* Write RECORD_CONTENT. */
    uint32_t recordContentAddress = recordAddress + FLASH_STORAGE_RECORD_INFO_SIZE + FLASH_STORAGE_RECORD_META_SIZE;

    if ((error = FLASH_STORAGE_HAL_Write(recordContentAddress, pRecordContent, (recordContentSize/sizeof(uint16_t)))) != FLASH_STORAGE_ERR_OK)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
    }


    /* Compute and write destination RECORD_CRC32. */
    uint32_t crc32BaseAddress = recordAddress + FLASH_STORAGE_RECORD_NAME_OFFSET;
    uint_fast16_t crc32BaseLength = FLASH_STORAGE_RECORD_NAME_SIZE + recordContentSize;

    uint32_t crc32 = 0;

    if ((error = FLASH_STORAGE_HAL_CalculateCrc32((__FLASH__ uint8_t *)(crc32BaseAddress), crc32BaseLength, &crc32)) != FLASH_STORAGE_ERR_OK)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
    }

    if ((error = FLASH_STORAGE_Write32(recordContentAddress + recordContentSize, crc32)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    return FLASH_STORAGE_ERR_OK;
}

static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_WriteFollowingRecord(uint32_t recordAddress, uint8_t recordId, uint8_t recordContentSize, __FLASH__ uint16_t *pRecordContent)
{
    FLASH_STORAGE_TRACE_FUNCTION_INVOKED();


    FLASH_STORAGE_ASSERT(recordAddress != NULL);
    FLASH_STORAGE_ASSERT((recordId | FLASH_STORAGE_FIRST_RECORD_ID_FLAG) != FLASH_STORAGE_INVALID_RECORD_ID);
    FLASH_STORAGE_ASSERT(recordContentSize != 0 && (recordContentSize % 2) == 0);
    FLASH_STORAGE_ASSERT(pRecordContent != NULL);


    FLASH_STORAGE_INFO("%s: at 0x%08X.\r\n", __FUNCTION__, recordAddress);

    FLASH_STORAGE_ErrorTypeDef error = FLASH_STORAGE_ERR_OK;

    /* Write RECORD_INFO (RECORD_ID and RECORD_SIZE). */
    uint16_t recordInfo = recordContentSize;
    recordInfo <<= 8;
    recordInfo += recordId;

    if ((error = FLASH_STORAGE_Write16(recordAddress + FLASH_STORAGE_RECORD_INFO_OFFSET, recordInfo)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    /* Write RECORD_CONTENT. */
    uint32_t recordContentAddress = recordAddress + FLASH_STORAGE_RECORD_INFO_SIZE;

    if ((error = FLASH_STORAGE_HAL_Write(recordContentAddress, pRecordContent, (recordContentSize/sizeof(uint16_t)))) != FLASH_STORAGE_ERR_OK)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
    }


    /* Compute and write destination RECORD_CRC32. */
    uint32_t crc32 = 0;

    if ((error = FLASH_STORAGE_HAL_CalculateCrc32((__FLASH__ uint8_t *)(recordContentAddress), recordContentSize, &crc32)) != FLASH_STORAGE_ERR_OK)
    {
        return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(error);
    }

    if ((error = FLASH_STORAGE_Write32(recordContentAddress + recordContentSize, crc32)) != FLASH_STORAGE_ERR_OK)
    {
        return error;
    }


    return FLASH_STORAGE_ERR_OK;
}

static bool FLASH_STORAGE_IsNameValid(FLASH_STORAGE_RecordNameTypeDef *pName)
{
    FLASH_STORAGE_ASSERT(pName != NULL);

    /* First char must not be '\0'. */
    if (pName->Name[0] == '\0')
    {
        return false;
    }

    for (uint8_t charIndex = 0; charIndex < FLASH_STORAGE_RECORD_NAME_SIZE; charIndex++)
    {
        char charAtIndex = pName->Name[charIndex];

        /* Only printable ASCII characters allowed without Space (0x20) and Delete (0x7F); '\0' allowed for non-first chars. */
        if (!(charAtIndex >= '!' /* 0x21 */ && charAtIndex <= '~' /* 0x21 */) && charAtIndex != '\0')
        {
            return false;
        }
    }

    return true;
}

static void FLASH_STORAGE_TraceFunctionInvoked(const char *pFunctionName)
{
#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_TRACE)

    static const char sTraceFunctionInvoked[] = "%s invoked.\r\n";

#endif

    FLASH_STORAGE_TRACE(sTraceFunctionInvoked, pFunctionName);
}

static FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_ErrorAndReturn(FLASH_STORAGE_ErrorTypeDef error, const char *pFunctionName)
{
#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_ERROR)

    static const char sErrorAndReturn[] = "Error in %s: %s.\r\n";

#endif

    FLASH_STORAGE_ERROR(sErrorAndReturn, pFunctionName, FLASH_STORAGE_ErrorToString(error));

    return error;
}

#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_ERROR)

static const char * FLASH_STORAGE_ErrorToString(FLASH_STORAGE_ErrorTypeDef error)
{
#define __TO_STRING(x) (#x)
#define TO_STRING(x) __TO_STRING(x)

    static const char *ssErrorList[] =
    {
        TO_STRING(FLASH_STORAGE_ERR_OK),

        TO_STRING(FLASH_STORAGE_ERR_INVALID_PARAMETER),
        TO_STRING(FLASH_STORAGE_ERR_INVALID_CONFIGURATION),
        TO_STRING(FLASH_STORAGE_ERR_INVALID_INIT_STATUS),

        TO_STRING(FLASH_STORAGE_ERR_EMPTY),
        TO_STRING(FLASH_STORAGE_ERR_RECORD_NOT_FOUND),
        TO_STRING(FLASH_STORAGE_ERR_NOT_ENOUGH_SPACE),
        TO_STRING(FLASH_STORAGE_ERR_NOT_ENOUGH_ID),

        TO_STRING(FLASH_STORAGE_ERR_HAL_NOT_IMPLEMENTED),
        TO_STRING(FLASH_STORAGE_ERR_HAL_WRITE),
        TO_STRING(FLASH_STORAGE_ERR_HAL_CRC32),
        TO_STRING(FLASH_STORAGE_ERR_HAL_ERASE),

        TO_STRING(__FLASH_STORAGE_ERR_UNKNOWN),
    };

#undef TO_STRING
#undef __TO_STRING

    if (error > __FLASH_STORAGE_ERR_UNKNOWN)
    {
        error = __FLASH_STORAGE_ERR_UNKNOWN;
    }

    return ssErrorList[error];
}

#endif


__weak void FLASH_STORAGE_TraceFormat(const char *fmt, ...)
{
    (void)fmt;
}

__weak void FLASH_STORAGE_AssertFailed(const char *pFileName, const char *pFunctionName, uint32_t line)
{
    (void)pFileName;
    (void)pFunctionName;
    (void)line;

    while (true)
    {
    }
}


__weak FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_HAL_Write(uint32_t writeAddress, __FLASH__ uint16_t buffer[], uint_fast16_t bufferLength)
{
    (void)writeAddress;
    (void)buffer;
    (void)bufferLength;

    return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_HAL_NOT_IMPLEMENTED);
}

__weak FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_HAL_CalculateCrc32(__FLASH__ uint8_t buffer[], uint_fast16_t bufferLength, uint32_t *pCrc32)
{
    (void)buffer;
    (void)bufferLength;
    (void)pCrc32;

    return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_HAL_NOT_IMPLEMENTED);
}

__weak FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_HAL_ErasePage(uint32_t pageAddress)
{
    (void)pageAddress;

    return FLASH_STORAGE_TRACE_ERROR_AND_RETURN(FLASH_STORAGE_ERR_HAL_NOT_IMPLEMENTED);
}
