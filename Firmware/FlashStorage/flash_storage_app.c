#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include "trace.h"
#include "flash_storage.h"
#include "flash_storage_config.h"
#include "flash_storage_app.h"

#ifdef FLASH_STORAGE_CONFIG_OS2
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#endif

#ifdef STM32F103xB
    #include "stm32f1xx_hal.h"
#else
    #error "Device not specified."
#endif


#ifdef FLASH_STORAGE_CONFIG_APP

#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_ERROR)
    #define FLASH_STORAGE_APP_ERROR(fmt, args...)   FLASH_STORAGE_TraceFormat(fmt, ##args)
    #define FLASH_STORAGE_APP_ASSERT_FAILED()       FLASH_STORAGE_AssertFailed(__FILE__, __FUNCTION__, __LINE__)
    #define FLASH_STORAGE_APP_ASSERT(condition)     do { if (!(condition)) FLASH_STORAGE_APP_ASSERT_FAILED(); } while (false)
#else
    #define FLASH_STORAGE_APP_ERROR(fmt, args...)   do { } while (false)
    #define FLASH_STORAGE_APP_ASSERT_FAILED()       do { } while (false)
    #define FLASH_STORAGE_APP_ASSERT(condition)     do { } while (false)
#endif

#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_INFO)
    #define FLASH_STORAGE_APP_INFO(fmt, args...)    FLASH_STORAGE_TraceFormat(fmt, ##args)
#else
    #define FLASH_STORAGE_APP_INFO(fmt, args...)    do { } while (false)
#endif

#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_TRACE)
    #define FLASH_STORAGE_APP_TRACE(fmt, args...)   FLASH_STORAGE_TraceFormat(fmt, ##args)
#else
    #define FLASH_STORAGE_APP_TRACE(fmt, args...)   do { } while (false)
#endif


#ifdef FLASH_STORAGE_CONFIG_OS2
extern osMutexId_t FLASH_STORAGE_CONFIG_ACCESS_MUTEX;
#endif

static FLASH_STORAGE_HandleTypeDef _hFlashStorage = {0};

static FLASH_STORAGE_RecordDescriptorTypeDef _cache[FLASH_STORAGE_CONFIG_CACHE_SIZE] = {0};

static uint16_t _page0[FLASH_STORAGE_CONFIG_PAGE_SIZE/sizeof(uint16_t)] __attribute__((at(FLASH_STORAGE_CONFIG_PAGE_0_ADDR)));
static uint16_t _page1[FLASH_STORAGE_CONFIG_PAGE_SIZE/sizeof(uint16_t)] __attribute__((at(FLASH_STORAGE_CONFIG_PAGE_1_ADDR)));


#ifdef FLASH_STORAGE_CONFIG_OS2

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_APP_Initialise(void)
{
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);
    FLASH_STORAGE_APP_ASSERT(!osExIsInISR());
    FLASH_STORAGE_APP_ASSERT(osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) == osThreadGetId());


    memset(&_hFlashStorage, 0, sizeof(FLASH_STORAGE_HandleTypeDef));
    memset(&_cache, 0, (sizeof(FLASH_STORAGE_RecordDescriptorTypeDef) * FLASH_STORAGE_CONFIG_CACHE_SIZE));

    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_CONFIG_PAGE_SIZE;
    _hFlashStorage.Configuration.Page0Address = FLASH_STORAGE_CONFIG_PAGE_0_ADDR;
    _hFlashStorage.Configuration.Page1Address = FLASH_STORAGE_CONFIG_PAGE_1_ADDR;
    _hFlashStorage.Configuration.Cache = _cache;
    _hFlashStorage.Configuration.CacheSize = FLASH_STORAGE_CONFIG_CACHE_SIZE;

    return FLASH_STORAGE_Initialise(&_hFlashStorage);
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_APP_IsInitialised(bool *pIsInitialized)
{
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);
    FLASH_STORAGE_APP_ASSERT(!osExIsInISR());
    FLASH_STORAGE_APP_ASSERT(osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) == osThreadGetId());


    return FLASH_STORAGE_IsInitialised(&_hFlashStorage, pIsInitialized);
}

osStatus_t FLASH_STORAGE_APP_OpenTry(void)
{
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);
    FLASH_STORAGE_APP_ASSERT(!osExIsInISR());
    FLASH_STORAGE_APP_ASSERT(osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) != osThreadGetId());


    return osMutexAcquireTry(FLASH_STORAGE_CONFIG_ACCESS_MUTEX);
}

osStatus_t FLASH_STORAGE_APP_OpenWait(void)
{
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);
    FLASH_STORAGE_APP_ASSERT(!osExIsInISR());
    FLASH_STORAGE_APP_ASSERT(osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) != osThreadGetId());


    return osMutexAcquireWait(FLASH_STORAGE_CONFIG_ACCESS_MUTEX);
}

bool FLASH_STORAGE_APP_IsOpened(void)
{
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);

    return !osExIsInISR() && osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) == osThreadGetId();
}

osStatus_t FLASH_STORAGE_APP_Close(void)
{
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);
    FLASH_STORAGE_APP_ASSERT(!osExIsInISR());
    FLASH_STORAGE_APP_ASSERT(osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) == osThreadGetId());


    return osMutexRelease(FLASH_STORAGE_CONFIG_ACCESS_MUTEX);
}
#else
FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_APP_Initialise(void)
{
    memset(&_hFlashStorage, 0, sizeof(FLASH_STORAGE_HandleTypeDef));
    memset(&_cache, 0, (sizeof(FLASH_STORAGE_RecordDescriptorTypeDef) * FLASH_STORAGE_CONFIG_CACHE_SIZE));

    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_CONFIG_PAGE_SIZE;
    _hFlashStorage.Configuration.Page0Address = FLASH_STORAGE_CONFIG_PAGE_0_ADDR;
    _hFlashStorage.Configuration.Page1Address = FLASH_STORAGE_CONFIG_PAGE_1_ADDR;
    _hFlashStorage.Configuration.Cache = _cache;
    _hFlashStorage.Configuration.CacheSize = FLASH_STORAGE_CONFIG_CACHE_SIZE;

    return FLASH_STORAGE_Initialise(&_hFlashStorage);
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_APP_IsInitialised(bool *pIsInitialized)
{
    return FLASH_STORAGE_IsInitialised(&_hFlashStorage, pIsInitialized);
}
#endif

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_APP_WriteRecord(FLASH_STORAGE_RecordNameTypeDef *pName, void *pContent, uint_fast8_t contentSize)
{
#ifdef FLASH_STORAGE_CONFIG_OS2
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);
    FLASH_STORAGE_APP_ASSERT(!osExIsInISR());
    FLASH_STORAGE_APP_ASSERT(osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) == osThreadGetId());
#endif

    return FLASH_STORAGE_WriteRecord(&_hFlashStorage, pName, pContent, contentSize);
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_APP_ReadRecord(FLASH_STORAGE_RecordNameTypeDef *pName, void *pBuffer, size_t bufferSize, uint_fast8_t *pContentSize)
{
#ifdef FLASH_STORAGE_CONFIG_OS2
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);
    FLASH_STORAGE_APP_ASSERT(!osExIsInISR());
    FLASH_STORAGE_APP_ASSERT(osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) == osThreadGetId());
#endif

    return FLASH_STORAGE_ReadRecord(&_hFlashStorage, pName, pBuffer, bufferSize, pContentSize);
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_APP_RemoveRecord(FLASH_STORAGE_RecordNameTypeDef *pName)
{
#ifdef FLASH_STORAGE_CONFIG_OS2
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);
    FLASH_STORAGE_APP_ASSERT(!osExIsInISR());
    FLASH_STORAGE_APP_ASSERT(osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) == osThreadGetId());
#endif

    return FLASH_STORAGE_RemoveRecord(&_hFlashStorage, pName);
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_APP_GetRecordsCount(uint_fast8_t *pCount)
{
#ifdef FLASH_STORAGE_CONFIG_OS2
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);
    FLASH_STORAGE_APP_ASSERT(!osExIsInISR());
    FLASH_STORAGE_APP_ASSERT(osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) == osThreadGetId());
#endif

    return FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, pCount);
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_APP_GetRecordsNames(FLASH_STORAGE_RecordNameTypeDef *pNamesList, uint_fast8_t namesCount)
{
#ifdef FLASH_STORAGE_CONFIG_OS2
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);
    FLASH_STORAGE_APP_ASSERT(!osExIsInISR());
    FLASH_STORAGE_APP_ASSERT(osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) == osThreadGetId());
#endif

    return FLASH_STORAGE_GetRecordsNames(&_hFlashStorage, pNamesList, namesCount);
}

FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_APP_GetRecordSize(FLASH_STORAGE_RecordNameTypeDef *pName, uint_fast8_t *pContentSize)
{
#ifdef FLASH_STORAGE_CONFIG_OS2
    FLASH_STORAGE_APP_ASSERT(FLASH_STORAGE_CONFIG_ACCESS_MUTEX != NULL);
    FLASH_STORAGE_APP_ASSERT(!osExIsInISR());
    FLASH_STORAGE_APP_ASSERT(osMutexGetOwner(FLASH_STORAGE_CONFIG_ACCESS_MUTEX) == osThreadGetId());
#endif

    return FLASH_STORAGE_GetRecordSize(&_hFlashStorage, pName, pContentSize);
}

/* Weak function. */
void FLASH_STORAGE_TraceFormat(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    TRACE_VFormat(fmt, args);

    va_end(args);
}

/* Weak function. */
void FLASH_STORAGE_AssertFailed(const char *pFileName, const char *pFunctionName, uint32_t line)
{
    (void)pFileName;

    TRACE_Format("Assert failed in %s at line %u.\r\n", pFunctionName, line);

    while (true)
    {
    #ifdef FLASH_STORAGE_CONFIG_OS2
        osDelay(1000);
    #endif
    }
}

#endif
