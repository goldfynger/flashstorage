#ifndef __FLASH_STORAGE_H
#define __FLASH_STORAGE_H


#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


#define FLASH_STORAGE_PAGE_MIN_SIZE         ((size_t)   512)
#define FLASH_STORAGE_PAGE_MAX_SIZE         ((size_t)   4096)
#define FLASH_STORAGE_RECORD_NAME_SIZE      ((size_t)   8)

#define FLASH_STORAGE_DEBUG_LEVEL_NONE      0U
#define FLASH_STORAGE_DEBUG_LEVEL_ERROR     1U
#define FLASH_STORAGE_DEBUG_LEVEL_INFO      2U
#define FLASH_STORAGE_DEBUG_LEVEL_TRACE     3U


#ifndef FLASH_STORAGE_DEBUG_LEVEL
    #define FLASH_STORAGE_DEBUG_LEVEL       FLASH_STORAGE_DEBUG_LEVEL_NONE
#endif


typedef enum
{
    FLASH_STORAGE_ERR_OK                    = ((uint8_t)    0x00),

    FLASH_STORAGE_ERR_INVALID_PARAMETER,
    FLASH_STORAGE_ERR_INVALID_CONFIGURATION,
    FLASH_STORAGE_ERR_INVALID_INIT_STATUS,

    FLASH_STORAGE_ERR_EMPTY,
    FLASH_STORAGE_ERR_RECORD_NOT_FOUND,
    FLASH_STORAGE_ERR_NOT_ENOUGH_SPACE,
    FLASH_STORAGE_ERR_NOT_ENOUGH_ID,

    FLASH_STORAGE_ERR_HAL_NOT_IMPLEMENTED,
    FLASH_STORAGE_ERR_HAL_WRITE,
    FLASH_STORAGE_ERR_HAL_CRC32,
    FLASH_STORAGE_ERR_HAL_ERASE,

    __FLASH_STORAGE_ERR_UNKNOWN,
}
FLASH_STORAGE_ErrorTypeDef;

typedef enum
{
    FLASH_STORAGE_VERSION_00_10             = ((uint16_t)   0x0010),

    __FLASH_STORAGE_VERSION_UNKNOWN         = ((uint16_t)   0xFFFF), /* Prevent compiler optimization. */
}
FLASH_STORAGE_VersionTypeDef;

typedef enum
{
    FLASH_STORAGE_PAGE_0                    = ((uint8_t)    0x00),
    FLASH_STORAGE_PAGE_1                    = ((uint8_t)    0x01),

    FLASH_STORAGE_PAGE_NONE                 = ((uint8_t)    0xFF),
}
FLASH_STORAGE_PageNumberTypeDef;


typedef struct
{
    char Name[FLASH_STORAGE_RECORD_NAME_SIZE];
}
FLASH_STORAGE_RecordNameTypeDef;

typedef struct
{
    const volatile void * Base;
    const volatile void * Actual;
}
FLASH_STORAGE_RecordDescriptorTypeDef;

typedef struct
{
    struct __FLASH_STORAGE_ConfigurationTypeDef
    {
        uint32_t                                PageSize;

        uint32_t                                Page0Address;
        uint32_t                                Page1Address;

        FLASH_STORAGE_RecordDescriptorTypeDef   * Cache;
        size_t                                  CacheSize;
    }
    Configuration;

    struct __FLASH_STORAGE_InternalTypeDef
    {
        size_t                                  CacheIndex;

        uint32_t                                EmptyAddress;

        uint8_t                                 RecordsCount;
        uint8_t                                 NextRecordId;

        bool                                    IsInitialised;

        FLASH_STORAGE_PageNumberTypeDef         ActivePage;
    }
    Internal;
}
FLASH_STORAGE_HandleTypeDef;


FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_Initialise        (FLASH_STORAGE_HandleTypeDef *hFlashStorage);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_IsInitialised     (FLASH_STORAGE_HandleTypeDef *hFlashStorage, bool *pIsInitialised);

FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_WriteRecord       (FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName, void *pContent, uint_fast8_t contentSize);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_ReadRecord        (FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName, void *pBuffer, size_t bufferSize, uint_fast8_t *pContentSize);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_RemoveRecord      (FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName);

FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_GetRecordsCount   (FLASH_STORAGE_HandleTypeDef *hFlashStorage, uint_fast8_t *pCount);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_GetRecordsNames   (FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pNamesList, uint_fast8_t namesCount);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_GetRecordSize     (FLASH_STORAGE_HandleTypeDef *hFlashStorage, FLASH_STORAGE_RecordNameTypeDef *pName, uint_fast8_t *pContentSize);

/* __weak functions. */
void                        FLASH_STORAGE_TraceFormat       (const char *fmt, ...);
void                        FLASH_STORAGE_AssertFailed      (const char *pFileName, const char *pFunctionName, uint32_t line);

FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_HAL_Write         (uint32_t writeAddress, const volatile uint16_t buffer[], uint_fast16_t bufferLength);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_HAL_CalculateCrc32(const volatile uint8_t buffer[], uint_fast16_t bufferLength, uint32_t *pCrc32);
FLASH_STORAGE_ErrorTypeDef  FLASH_STORAGE_HAL_ErasePage     (uint32_t pageAddress);


#endif /* __FLASH_STORAGE_H */
