#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "trace_config.h"
#include "app_usb_customhid.h"
#include "freertos_app.h"
#include "customhid_app.h"


/* Kernel objects. */
extern osThreadId_t customhidTaskHandle;

extern osMessageQueueId_t customhidInQueueHandle;
extern osMessageQueueId_t customhidOutQueueHandle;

extern CMSIS_OS2_EX_TaskStateTypeDef customhidTaskState;
/* Kernel objects. */


static void CUSTOMHID_APP_ErrorHandler(uint32_t line);


void CUSTOMHID_APP_Task(void *argument)
{/*------------*/osDelay(osWaitForever);/*------------*/
    TRACE_String("CUSTOMHID_APP_Task started.\r\n");
    
    customhidTaskState = CMSIS_OS2_EX_TASK_STATE_START;
    
    APP_USB_CUSTOMHID_Initialize();
    
    while (!APP_USB_CUSTOMHID_IsConnected())
    {
        osDelay(10);
    }
    
    TRACE_String("CUSTOMHID_APP_Task connected.\r\n");
    
    customhidTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;
    
    
    static CUSTOMHID_APP_QueueMessageTypeDef itemToTransmit;
    
    
    while(true)
    {
        if (osExMessageQueueGetWait(customhidInQueueHandle, &itemToTransmit) != osOK)
        {
            CUSTOMHID_APP_ErrorHandler(__LINE__);
        }
                                
        while(APP_USB_CUSTOMHID_IsTransmitBusy())
        {
            osDelay(1);
        }
        
        APP_USB_CUSTOMHID_Transmit(itemToTransmit.Report);
    }
}

/* Posts message in USB HID queue. */
bool CUSTOMHID_APP_PostInQueue(CUSTOMHID_APP_QueueMessageTypeDef *pMessage)
{
    if (pMessage == NULL)
    {
        CUSTOMHID_APP_ErrorHandler(__LINE__);
    }
    
    osStatus_t result = osExMessageQueuePutTry(customhidInQueueHandle, pMessage);
    
    if (result == osOK)
    {
        return true;
    }
    else if (result == osErrorResource)
    {
        return false;
    }
    else
    {
        CUSTOMHID_APP_ErrorHandler(__LINE__);
        return false;
    }
}

/* Gets mesage from USB HID queue. */
bool CUSTOMHID_APP_GetFromQueue(CUSTOMHID_APP_QueueMessageTypeDef *pMessage)
{
    if (pMessage == NULL)
    {
        CUSTOMHID_APP_ErrorHandler(__LINE__);
    }
    
    osStatus_t result = osExMessageQueueGetTry(customhidOutQueueHandle, &pMessage);
    
    if (result == osOK)
    {
        return true;
    }
    else if (result == osErrorResource)
    {
        return false;
    }
    else
    {
        CUSTOMHID_APP_ErrorHandler(__LINE__);
        return false;
    }
}


static void CUSTOMHID_APP_ErrorHandler(uint32_t line)
{
    const char *errorFormat = "CUSTOMHID_APP_ErrorHandler on line %d.\r\n";
    
    if (osThreadGetId() == customhidTaskHandle)
    {
        customhidTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;
        
        TRACE_Format(errorFormat, line);
        
        while(true)
        {
            osDelay(osWaitForever);
        }
    }
    else
    {
        oxExDisableInterrupts();
        
        printf(errorFormat, line);
        
        while (true)
        {
        }
    }    
}


/* Weak callback. */
void APP_USB_APP_CUSTOMHID_ReceiveCompleteCallback(uint8_t *pBuffer)
{
    if (osExMessageQueuePutTry(customhidOutQueueHandle, pBuffer) != osOK)
    {
        CUSTOMHID_APP_ErrorHandler(__LINE__);
    }
}
