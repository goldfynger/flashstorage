#ifndef __CMSIS_OS2_EX_H
#define __CMSIS_OS2_EX_H


#include <stdbool.h>
#include <stdint.h>
#include "cmsis_os2.h"


typedef enum
{
    CMSIS_OS2_EX_TASK_STATE_IDLE    = 0,
    CMSIS_OS2_EX_TASK_STATE_START   = 1,
    CMSIS_OS2_EX_TASK_STATE_RUN     = 2,
    CMSIS_OS2_EX_TASK_STATE_STOP    = 3,
    CMSIS_OS2_EX_TASK_STATE_ERROR   = 4,
}
CMSIS_OS2_EX_TaskStateTypeDef;


osStatus_t  osExMessageQueuePutToFront      (osMessageQueueId_t mq_id, const void *msg_ptr, uint8_t msg_prio, uint32_t timeout);
osStatus_t  osExMessageQueuePeek            (osMessageQueueId_t mq_id, void *msg_ptr, uint8_t *msg_prio, uint32_t timeout);

osStatus_t  osExMessageQueuePutTry          (osMessageQueueId_t mq_id, const void *msg_ptr);
osStatus_t  osExMessageQueuePutWait         (osMessageQueueId_t mq_id, const void *msg_ptr);
osStatus_t  osExMessageQueuePutToFrontTry   (osMessageQueueId_t mq_id, const void *msg_ptr);
osStatus_t  osExMessageQueuePutToFrontWait  (osMessageQueueId_t mq_id, const void *msg_ptr);
osStatus_t  osExMessageQueuePeekTry         (osMessageQueueId_t mq_id, void *msg_ptr);
osStatus_t  osExMessageQueuePeekWait        (osMessageQueueId_t mq_id, void *msg_ptr);
osStatus_t  osExMessageQueueGetTry          (osMessageQueueId_t mq_id, void *msg_ptr);
osStatus_t  osExMessageQueueGetWait         (osMessageQueueId_t mq_id, void *msg_ptr);

bool        osExIsInISR                     (void);
void        oxExDisableInterrupts           (void);
void        oxExEnableInterrupts            (void);

void        osExEnterCritical               (void);
void        osExExitCritical                (void);
uint32_t    osExEnterCriticalInISR          (void);
void        osExExitCriticalInISR           (uint32_t status);

void      * osExMalloc                      (size_t size);
void        osExFree                        (void *ptr);


#endif /* __CMSIS_OS2_EX_H */
