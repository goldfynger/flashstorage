#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include "cmsis_armcc.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "portmacro.h"
#include "task.h"
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"


/* Copy of cmsis_os2.c content. */
#if   ((__ARM_ARCH_7M__      == 1U) || \
       (__ARM_ARCH_7EM__     == 1U) || \
       (__ARM_ARCH_8M_MAIN__ == 1U))
#define IS_IRQ_MASKED()           ((__get_PRIMASK() != 0U) || ((osKernelGetState() == osKernelRunning) && (__get_BASEPRI() != 0U)))
#elif  (__ARM_ARCH_6M__      == 1U)
#define IS_IRQ_MASKED()           ((__get_PRIMASK() != 0U) &&  (osKernelGetState() == osKernelRunning))
#elif (__ARM_ARCH_7A__       == 1)
#define IS_IRQ_MASKED()           (0U)
#else
#define IS_IRQ_MASKED()           (__get_PRIMASK() != 0U)
#endif

#if    (__ARM_ARCH_7A__      == 1U)
/* CPSR mode bitmasks */
#define CPSR_MODE_USER            0x10U
#define CPSR_MODE_SYSTEM          0x1FU

#define IS_IRQ_MODE()             ((__get_mode() != CPSR_MODE_USER) && (__get_mode() != CPSR_MODE_SYSTEM))
#else
#define IS_IRQ_MODE()             (__get_IPSR() != 0U)
#endif

#define IS_IRQ()                  (IS_IRQ_MODE() || IS_IRQ_MASKED())
/* Copy of cmsis_os2.c content. */


/* Based on cmsis_os2.c osMessageQueuePut function. */
osStatus_t osExMessageQueuePutToFront (osMessageQueueId_t mq_id, const void *msg_ptr, uint8_t msg_prio, uint32_t timeout) {
  QueueHandle_t hQueue = (QueueHandle_t)mq_id;
  osStatus_t stat;
  BaseType_t yield;

  (void)msg_prio; /* Message priority is ignored */

  stat = osOK;

  if (IS_IRQ()) {
    if ((hQueue == NULL) || (msg_ptr == NULL) || (timeout != 0U)) {
      stat = osErrorParameter;
    }
    else {
      yield = pdFALSE;

      if (xQueueSendToFrontFromISR (hQueue, msg_ptr, &yield) != pdTRUE) {
        stat = osErrorResource;
      } else {
        portYIELD_FROM_ISR (yield);
      }
    }
  }
  else {
    if ((hQueue == NULL) || (msg_ptr == NULL)) {
      stat = osErrorParameter;
    }
    else {
      if (xQueueSendToFront (hQueue, msg_ptr, (TickType_t)timeout) != pdPASS) {
        if (timeout != 0U) {
          stat = osErrorTimeout;
        } else {
          stat = osErrorResource;
        }
      }
    }
  }

  return (stat);
}
/* Based on cmsis_os2.c osMessageQueuePut function. */

/* Based on cmsis_os2.c osMessageQueueGet function. */
osStatus_t osExMessageQueuePeek (osMessageQueueId_t mq_id, void *msg_ptr, uint8_t *msg_prio, uint32_t timeout) {
  QueueHandle_t hQueue = (QueueHandle_t)mq_id;
  osStatus_t stat;

  (void)msg_prio; /* Message priority is ignored */

  stat = osOK;

  if (IS_IRQ()) {
    if ((hQueue == NULL) || (msg_ptr == NULL) || (timeout != 0U)) {
      stat = osErrorParameter;
    }
    else {
      if (xQueuePeekFromISR (hQueue, msg_ptr) != pdPASS) {
        stat = osErrorResource;
      } else {
        portYIELD_FROM_ISR (pdFALSE);
      }
    }
  }
  else {
    if ((hQueue == NULL) || (msg_ptr == NULL)) {
      stat = osErrorParameter;
    }
    else {
      if (xQueuePeek (hQueue, msg_ptr, (TickType_t)timeout) != pdPASS) {
        if (timeout != 0U) {
          stat = osErrorTimeout;
        } else {
          stat = osErrorResource;
        }
      }
    }
  }

  return (stat);
}
/* Based on cmsis_os2.c osMessageQueueGet function. */


osStatus_t osExMessageQueuePutTry(osMessageQueueId_t mq_id, const void *msg_ptr)
{
    return osMessageQueuePut(mq_id, msg_ptr, NULL, 0U);
}

osStatus_t osExMessageQueuePutWait(osMessageQueueId_t mq_id, const void *msg_ptr)
{
    return osMessageQueuePut(mq_id, msg_ptr, NULL, osWaitForever);
}

osStatus_t osExMessageQueuePutToFrontTry(osMessageQueueId_t mq_id, const void *msg_ptr)
{
    return osExMessageQueuePutToFront(mq_id, msg_ptr, NULL, 0U);
}

osStatus_t osExMessageQueuePutToFrontWait(osMessageQueueId_t mq_id, const void *msg_ptr)
{
    return osExMessageQueuePutToFront(mq_id, msg_ptr, NULL, osWaitForever);
}

osStatus_t osExMessageQueuePeekTry(osMessageQueueId_t mq_id, void *msg_ptr)
{
    return osExMessageQueuePeek(mq_id, msg_ptr, NULL, 0U);
}

osStatus_t osExMessageQueuePeekWait(osMessageQueueId_t mq_id, void *msg_ptr)
{
    return osExMessageQueuePeek(mq_id, msg_ptr, NULL, osWaitForever);
}

osStatus_t osExMessageQueueGetTry(osMessageQueueId_t mq_id, void *msg_ptr)
{
    return osMessageQueueGet(mq_id, msg_ptr, NULL, 0U);
}

osStatus_t osExMessageQueueGetWait(osMessageQueueId_t mq_id, void *msg_ptr)
{
    return osMessageQueueGet(mq_id, msg_ptr, NULL, osWaitForever);
}


bool osExIsInISR(void)
{
    return IS_IRQ();
}

void oxExDisableInterrupts(void)
{
    taskDISABLE_INTERRUPTS();
}

void oxExEnableInterrupts(void)
{
    taskENABLE_INTERRUPTS();
}


void osExEnterCritical(void)
{
    taskENTER_CRITICAL();
}

void osExExitCritical(void)
{
    taskEXIT_CRITICAL();
}

uint32_t osExEnterCriticalInISR(void)
{
    return taskENTER_CRITICAL_FROM_ISR();
}

void osExExitCriticalInISR(uint32_t status)
{
    taskEXIT_CRITICAL_FROM_ISR(status);
}


void * osExMalloc(size_t size)
{
    return pvPortMalloc(size);  
}

void osExFree(void *ptr)
{
    vPortFree(ptr);
}
