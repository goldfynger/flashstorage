#include <stdbool.h>
#include <stdint.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "board.h"
#include "customhid_app.h"
#include "trace_config.h"
#include "flash_storage_os2.h"
#include "freertos_app.h"


osThreadId_t customhidTaskHandle;           /* Used as external */
const osThreadAttr_t _customhidTask_attributes = {
  .name = "customhidTask",
  .priority = (osPriority_t) osPriorityAboveNormal,
  .stack_size = 128 * 4
};

osThreadId_t traceTaskHandle;               /* Used as external */
const osThreadAttr_t _traceTask_attributes = {
  .name = "traceTask",
  .priority = (osPriority_t) osPriorityHigh1,
  .stack_size = 128 * 4
};

osThreadId_t _flashStorageTaskHandle;
const osThreadAttr_t _flashStorageTask_attributes = {
  .name = "flashStorageTask",
  .priority = (osPriority_t) osPriorityHigh,
  .stack_size = 256 * 4
};


osMessageQueueId_t customhidInQueueHandle;  /* Used as external */
const osMessageQueueAttr_t _customhidInQueue_attributes = {
  .name = "customhidInQueue"
};

osMessageQueueId_t customhidOutQueueHandle; /* Used as external */
const osMessageQueueAttr_t _customhidOutQueue_attributes = {
  .name = "customhidOutQueue"
};

osMessageQueueId_t traceQueueHandle;            /* Used as external */
const osMessageQueueAttr_t _traceSwoQueue_attributes = {
  .name = "traceSwoQueue"
};


osTimerId_t _stateIndicationTimerHandle;
const osTimerAttr_t _stateIndicationTimer_attributes = {
  .name = "stateIndicationTimer"
};


CMSIS_OS2_EX_TaskStateTypeDef customhidTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;    /* Used as external */
CMSIS_OS2_EX_TaskStateTypeDef traceTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;        /* Used as external */
CMSIS_OS2_EX_TaskStateTypeDef flashStorageTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE; /* Used as external */


void FREERTOS_APP_Start(void)
{
    osKernelInitialize();
    
    
    _stateIndicationTimerHandle = osTimerNew(BOARD_StateIndicationTimerCallback, osTimerPeriodic, NULL, &_stateIndicationTimer_attributes);
    
    
    customhidInQueueHandle = osMessageQueueNew(CUSTOMHID_APP_IN_QUEUE_COUNT, sizeof(CUSTOMHID_APP_QueueMessageTypeDef), &_customhidInQueue_attributes);
    
    customhidOutQueueHandle = osMessageQueueNew(CUSTOMHID_APP_OUT_QUEUE_COUNT, sizeof(CUSTOMHID_APP_QueueMessageTypeDef), &_customhidOutQueue_attributes);
    
    traceQueueHandle = osMessageQueueNew(TRACE_OS2_QUEUE_COUNT, sizeof(TRACE_OS2_QueueMessageTypeDef), &_traceSwoQueue_attributes);
    
    
    customhidTaskHandle = osThreadNew(CUSTOMHID_APP_Task, NULL, &_customhidTask_attributes);
    
    traceTaskHandle = osThreadNew(TRACE_OS2_Task, NULL, &_traceTask_attributes);
    
    _flashStorageTaskHandle = osThreadNew(FLASH_STORAGE_OS2_Task, NULL, &_flashStorageTask_attributes);
    
    
    osTimerStart(_stateIndicationTimerHandle, BOARD_STATE_INDICATION_TIMER_PERIOD);
    
    
    osKernelStart();
    
    
    /* Should never get here. */
    while (true)
    {
    }
}
