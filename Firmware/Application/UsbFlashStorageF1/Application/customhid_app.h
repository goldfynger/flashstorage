#ifndef __CUSTOMHID_APP_H
#define __CUSTOMHID_APP_H


#include <stdint.h>
#include "app_usb_customhid.h"


#define CUSTOMHID_APP_IN_QUEUE_COUNT  4
#define CUSTOMHID_APP_OUT_QUEUE_COUNT 4


typedef struct
{
    uint8_t Report[APP_USB_CUSTOMHID_REPORT_SIZE];
}
CUSTOMHID_APP_QueueMessageTypeDef;


void CUSTOMHID_APP_Task           (void *argument);
bool CUSTOMHID_APP_PostInQueue    (CUSTOMHID_APP_QueueMessageTypeDef *pMessage);
bool CUSTOMHID_APP_GetFromQueue   (CUSTOMHID_APP_QueueMessageTypeDef *pMessage);


#endif /* __CUSTOMHID_APP_H */
