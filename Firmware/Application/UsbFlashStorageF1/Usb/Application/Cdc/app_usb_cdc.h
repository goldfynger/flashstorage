#ifndef __APP_USB_CDC_H
#define __APP_USB_CDC_H


#include <stdbool.h>
#include <stdint.h>


#define APP_USB_CDC_VID                         1155    /* Vendor ID. */
#define APP_USB_CDC_PID                         22336   /* Product ID. */
#define APP_USB_CDC_MAX_STRING_DESCRIPTOR_SIZE  64U     /* Max length of string descriptor (2 + (strlen(str) * 2)). strlen() - length of string w/o ending zero. USB used UTF-16. */

                                              /* 4 8  16  24  32  40  48  56  64 */
                                              /* ^ ^   ^   ^   ^   ^   ^   ^   ^ */
#define APP_USB_CDC_MANUFACTURER_STRING         "STMicroelectronics"
#define APP_USB_CDC_PRODUCT_STRING_FS           "STM32 Virtual ComPort"
#define APP_USB_CDC_CONFIGURATION_STRING_FS     "CDC Config"
#define APP_USB_CDC_INTERFACE_STRING_FS         "CDC Interface"
                                              /* ^ ^   ^   ^   ^   ^   ^   ^   ^ */
                                              /* 4 8  16  24  32  40  48  56  64 */

#define APP_USB_CDC_SELF_POWERED                0U      /* 1U - device powered from self source. */
#define APP_USB_CDC_MAX_POWER                   50U     /* (Value x2) == max current in mA. 250 (500 mA) max. */
#define APP_USB_CDC_POLLING_INTERVAL            1U      /* EP pool interval 1...255. */

#define APP_USB_CDC_DEBUG_LEVEL                 3       /* 0...3 */

#define APP_USB_CDC_RX0_INT_PREEMPTION_PRIORITY 0       /* 1...15 if preemtion priority used 4 bits. */
#define APP_USB_CDC_TX_INT_PREEMPTION_PRIORITY  0       /* 1...15 if preemtion priority used 4 bits. */


bool APP_USB_CDC_Transmit(uint8_t *pBuffer, uint16_t length);
bool APP_USB_CDC_IsTransmitBusy(void);
void APP_USB_CDC_Initialize(void);
bool APP_USB_CDC_IsConnected(void);

void APP_USB_CDC_TransmitCompleteCallback(void);
void APP_USB_CDC_ReceiveCompleteCallback(uint8_t *pBuffer, uint16_t length);


#endif /* __APP_USB_CDC_H */
