#include <stdbool.h>
#include <stdint.h>
#include "usbd_def.h"
#include "usb_device.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"
#include "app_usb_cdc.h"


bool APP_USB_CDC_Transmit(uint8_t *pBuffer, uint16_t length)
{
    if (APP_USB_CDC_IsTransmitBusy())
    {
        return false;
    }
    
    return CDC_Transmit_FS(pBuffer, length) == USBD_OK;
}

bool APP_USB_CDC_IsTransmitBusy(void)
{
    return CDC_IsTransmitBusy();
}

void APP_USB_CDC_Initialize(void)
{
    MX_USB_DEVICE_Init();
    MX_USB_DEVICE_HardwareStart();
}

bool APP_USB_CDC_IsConnected(void)
{
    return MX_USB_DEVICE_IsConfigured();
}

__weak void APP_USB_CDC_TransmitCompleteCallback(void)
{
}

__weak void APP_USB_CDC_ReceiveCompleteCallback(uint8_t *pBuffer, uint16_t length)
{
    UNUSED(pBuffer);
}
