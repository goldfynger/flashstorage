#ifndef __APP_USB_CUSTOMHID_H
#define __APP_USB_CUSTOMHID_H


#include <stdbool.h>
#include <stdint.h>


#define APP_USB_CUSTOMHID_VID                           1155    /* Vendor ID. */
#define APP_USB_CUSTOMHID_PID                           22352   /* Product ID. */
#define APP_USB_CUSTOMHID_MAX_STRING_DESCRIPTOR_SIZE    64U     /* Max length of string descriptor (2 + (strlen(str) * 2)). strlen() - length of string w/o ending zero. USB used UTF-16. */

                                                      /* 4 8  16  24  32  40  48  56  64 */
                                                      /* ^ ^   ^   ^   ^   ^   ^   ^   ^ */
#define APP_USB_CUSTOMHID_MANUFACTURER_STRING           "STMicroelectronics"
#define APP_USB_CUSTOMHID_PRODUCT_STRING_FS             "STM32 Custom Human interface"
#define APP_USB_CUSTOMHID_CONFIGURATION_STRING_FS       "Custom HID Config"
#define APP_USB_CUSTOMHID_INTERFACE_STRING_FS           "Custom HID Interface"
                                                      /* ^ ^   ^   ^   ^   ^   ^   ^   ^ */
                                                      /* 4 8  16  24  32  40  48  56  64 */

#define APP_USB_CUSTOMHID_SELF_POWERED                  0U      /* 1U - device powered from self source. */
#define APP_USB_CUSTOMHID_MAX_POWER                     50U     /* (Value x2) == max current in mA. 250 (500 mA) max. */
#define APP_USB_CUSTOMHID_POLLING_INTERVAL              1U      /* EP pool interval 1...255. */

#define APP_USB_CUSTOMHID_EPIN_SIZE                     0x40U   /* Maximum size of custom HID in EP. */
#define APP_USB_CUSTOMHID_EPOUT_SIZE                    0x40U   /* Maximum size of custom HID out EP. */

#define APP_USB_CUSTOMHID_REPORT_SIZE                   64U     /* Size of custom HID report. */

#define APP_USB_CUSTOMHID_DEBUG_LEVEL                   3       /* 0...3 */

#define APP_USB_CUSTOMHID_RX0_INT_PREEMPTION_PRIORITY   5       /* 1...15 if preemtion priority used 4 bits. */
#define APP_USB_CUSTOMHID_TX_INT_PREEMPTION_PRIORITY    5       /* 1...15 if preemtion priority used 4 bits. */


bool APP_USB_CUSTOMHID_Transmit(uint8_t *pBuffer);
bool APP_USB_CUSTOMHID_IsTransmitBusy(void);
void APP_USB_CUSTOMHID_Initialize(void);
bool APP_USB_CUSTOMHID_IsConnected(void);

void APP_USB_CUSTOMHID_TransmitCompleteCallback(void);
void APP_USB_CUSTOMHID_ReceiveCompleteCallback(uint8_t *pBuffer);


#endif /* __APP_USB_CUSTOMHID_H */
