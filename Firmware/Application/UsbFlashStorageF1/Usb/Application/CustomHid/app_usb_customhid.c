#include <stdbool.h>
#include <stdint.h>
#include "usbd_def.h"
#include "usb_device.h"
#include "usbd_customhid.h"
#include "usbd_custom_hid_if.h"
#include "app_usb_customhid.h"


bool APP_USB_CUSTOMHID_Transmit(uint8_t *pBuffer)
{
    if (APP_USB_CUSTOMHID_IsTransmitBusy())
    {
        return false;
    }
    
    return USBD_CUSTOM_HID_SendReport_FS(pBuffer, APP_USB_CUSTOMHID_REPORT_SIZE) == USBD_OK;
}

bool APP_USB_CUSTOMHID_IsTransmitBusy(void)
{
    return USBD_CUSTOM_HID_IsTransmitBusy();
}

void APP_USB_CUSTOMHID_Initialize(void)
{
    MX_USB_DEVICE_Init();
    MX_USB_DEVICE_HardwareStart();
}

bool APP_USB_CUSTOMHID_IsConnected(void)
{
    return MX_USB_DEVICE_IsConfigured();
}

__weak void APP_USB_CUSTOMHID_TransmitCompleteCallback(void)
{
}

__weak void APP_USB_CUSTOMHID_ReceiveCompleteCallback(uint8_t *pBuffer)
{
    UNUSED(pBuffer);
}
