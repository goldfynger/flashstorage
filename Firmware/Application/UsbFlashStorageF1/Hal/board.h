#ifndef __BOARD_H
#define __BOARD_H


#define BOARD_STATE_INDICATION_TIMER_PERIOD 500


void BOARD_SetUsbPullup(void);
void BOARD_ResetUsbPullup(void);
void BOARD_ToggleStateLed(void);

void BOARD_StateIndicationTimerCallback(void *argument);


#endif /* __BOARD_H */
