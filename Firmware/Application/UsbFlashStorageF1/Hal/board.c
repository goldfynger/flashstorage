#include "stm32f1xx_hal.h"
#include "main.h"
#include "board.h"

void BOARD_SetUsbPullup(void)
{
    HAL_GPIO_WritePin(USB_PULL_UP_GPIO_Port, USB_PULL_UP_Pin, GPIO_PIN_SET);
}

void BOARD_ResetUsbPullup(void)
{
    HAL_GPIO_WritePin(USB_PULL_UP_GPIO_Port, USB_PULL_UP_Pin, GPIO_PIN_RESET);
}

void BOARD_ToggleStateLed(void)
{
    HAL_GPIO_TogglePin(LED_STATE_GPIO_Port, LED_STATE_Pin);
}


void BOARD_StateIndicationTimerCallback(void *argument)
{
    BOARD_ToggleStateLed();
}


/* Weak callback. */
void MX_USB_DEVICE_HardwareStart(void)
{
    BOARD_SetUsbPullup();
}
