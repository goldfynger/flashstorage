/* See test settings in flash_storage_test.h */

#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "gpio.h"
#include "stm32f1xx_hal.h"
#include "flash_storage.h"
#include "trace.h"
#include "flash_storage_test.h"


#define FLASH_STORAGE_TEST_ERASED_U8    ((uint8_t)      0xFF)
#define FLASH_STORAGE_TEST_CACHE_MAX    ((size_t)      4)
#define FLASH_STORAGE_TEST_04_CONT_SIZE ((uint_fast8_t) 0x04)
#define FLASH_STORAGE_TEST_08_CONT_SIZE ((uint_fast8_t) 0x08)
#define FLASH_STORAGE_TEST_FE_CONT_SIZE ((uint_fast8_t) 0xFE)
#define FLASH_STORAGE_TEST_READ_SIZE    ((size_t)       0xFF)
#define FLASH_STORAGE_TEST_READ_NAMES   ((size_t)       4)
#define FLASH_STORAGE_TEST_HEADER_SIZE  ((size_t)       8)
#define FLASH_STORAGE_TEST_FIR_SIZE     ((size_t)       2 + 2 + 8 + 4)
#define FLASH_STORAGE_TEST_SEC_SIZE     ((size_t)       2 + 4)
#define FLASH_STORAGE_TEST_04_FIR_SIZE  ((size_t)       FLASH_STORAGE_TEST_FIR_SIZE + FLASH_STORAGE_TEST_04_CONT_SIZE)
#define FLASH_STORAGE_TEST_08_FIR_SIZE  ((size_t)       FLASH_STORAGE_TEST_FIR_SIZE + FLASH_STORAGE_TEST_08_CONT_SIZE)
#define FLASH_STORAGE_TEST_FE_FIR_SIZE  ((size_t)       FLASH_STORAGE_TEST_FIR_SIZE + FLASH_STORAGE_TEST_FE_CONT_SIZE)
#define FLASH_STORAGE_TEST_04_SEC_SIZE  ((size_t)       FLASH_STORAGE_TEST_SEC_SIZE + FLASH_STORAGE_TEST_04_CONT_SIZE)
#define FLASH_STORAGE_TEST_08_SEC_SIZE  ((size_t)       FLASH_STORAGE_TEST_SEC_SIZE + FLASH_STORAGE_TEST_08_CONT_SIZE)
#define FLASH_STORAGE_TEST_FE_SEC_SIZE  ((size_t)       FLASH_STORAGE_TEST_SEC_SIZE + FLASH_STORAGE_TEST_FE_CONT_SIZE)


#define FLASH_STORAGE_TEST_ASSERT_FAILED()       FLASH_STORAGE_TEST_AssertFailed(__FUNCTION__, __LINE__)
#define FLASH_STORAGE_TEST_ASSERT(condition)     do { if (!(condition)) FLASH_STORAGE_TEST_ASSERT_FAILED(); } while (false)

#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_INFO)
    #define FLASH_STORAGE_TEST_INFO(fmt, args...)    TRACE_Format(fmt, ##args)
#else
    #define FLASH_STORAGE_TEST_INFO(fmt, args...)    do { } while (false)
#endif

#if (FLASH_STORAGE_DEBUG_LEVEL >= FLASH_STORAGE_DEBUG_LEVEL_TRACE)
    #define FLASH_STORAGE_TEST_TRACE(fmt, args...)   TRACE_Format(fmt, ##args)
#else
    #define FLASH_STORAGE_TEST_TRACE(fmt, args...)   do { } while (false)
#endif


#ifdef FLASH_STORAGE_USE_RAM_INSTEAD_OF_FLASH
static uint16_t _page0[FLASH_STORAGE_TEST_PAGE_SIZE/sizeof(uint16_t)];
static uint16_t _page1[FLASH_STORAGE_TEST_PAGE_SIZE/sizeof(uint16_t)];
#else
static uint16_t _page0[FLASH_STORAGE_TEST_PAGE_SIZE/sizeof(uint16_t)] __attribute__((at(FLASH_STORAGE_TEST_PAGE_0_ADDR)));
static uint16_t _page1[FLASH_STORAGE_TEST_PAGE_SIZE/sizeof(uint16_t)] __attribute__((at(FLASH_STORAGE_TEST_PAGE_1_ADDR)));
#endif

static FLASH_STORAGE_HandleTypeDef _hFlashStorage = {0};
static FLASH_STORAGE_RecordDescriptorTypeDef _cache[FLASH_STORAGE_TEST_CACHE_MAX] = {0};

static FLASH_STORAGE_RecordNameTypeDef _record00Name = {0};
static FLASH_STORAGE_RecordNameTypeDef _record04Name = {0};
static FLASH_STORAGE_RecordNameTypeDef _record08Name = {0};
static FLASH_STORAGE_RecordNameTypeDef _recordFEName = {0};

static uint8_t _record04Content[FLASH_STORAGE_TEST_04_CONT_SIZE] = {0};
static uint8_t _record08Content[FLASH_STORAGE_TEST_08_CONT_SIZE] = {0};
static uint8_t _recordFEContent[FLASH_STORAGE_TEST_FE_CONT_SIZE] = {0};

static uint8_t _recordReadBuffer[FLASH_STORAGE_TEST_READ_SIZE] = {0};
static uint_fast8_t _recordReadContentSize = 0;
static uint_fast8_t _recordsCount = 0;
static FLASH_STORAGE_RecordNameTypeDef _recordReadNames[FLASH_STORAGE_TEST_READ_NAMES] = {0};

extern CRC_HandleTypeDef hcrc;


static void FLASH_STORAGE_TEST_CheckInvalidParams(void);
static void FLASH_STORAGE_TEST_CheckInit(void);
static void FLASH_STORAGE_TEST_CheckReadWriteRemove(void);

static void FLASH_STORAGE_TEST_WriteAllRecords(uint32_t writeTimes);

static void FLASH_STORAGE_TEST_InitVariables(void);
static void FLASH_STORAGE_TEST_WriteAtAddress(uint32_t address, uint16_t value);
static void FLASH_STORAGE_TEST_CopyPage(uint32_t srcAddress, uint32_t dstAddress);

static uint32_t FLASH_STORAGE_TEST_CalculateCrc32(CRC_HandleTypeDef *hcrc, __I uint8_t pBuffer[], uint32_t bufferLength);

__NO_RETURN static void FLASH_STORAGE_TEST_AssertFailed(const char *pFunctionName, uint32_t line);


__NO_RETURN void FLASH_STORAGE_TEST_Process(void)
{
    FLASH_STORAGE_TEST_INFO("%s test started.\r\n", __FUNCTION__);

    FLASH_STORAGE_TEST_INFO("Page 0 at %p and page 1 at %p.\r\n", _page0, _page1);


    FLASH_STORAGE_TEST_CheckInvalidParams();

    FLASH_STORAGE_TEST_CheckInit();

    FLASH_STORAGE_TEST_CheckReadWriteRemove();


    TRACE_Format("%s completed.\r\n", __FUNCTION__);

    while(true)
    {
    }
}


static void FLASH_STORAGE_TEST_CheckInvalidParams(void)
{
    FLASH_STORAGE_TEST_INFO("%s test started.\r\n", __FUNCTION__);

    FLASH_STORAGE_TEST_InitVariables();


    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);

    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_TEST_PAGE_SIZE;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);

    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);

    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);

    _hFlashStorage.Configuration.Cache = _cache;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);

    _hFlashStorage.Configuration.CacheSize = FLASH_STORAGE_TEST_CACHE_MAX;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    _hFlashStorage.Configuration.PageSize = 0;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_PAGE_MIN_SIZE - 1;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_PAGE_MIN_SIZE;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);
    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_PAGE_MIN_SIZE + 1;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_PAGE_MAX_SIZE - 1;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_PAGE_MAX_SIZE;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION); /* Page0Address + FLASH_STORAGE_PAGE_MAX_SIZE is greater than Page1Address. */
    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_PAGE_MAX_SIZE + 1;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_TEST_PAGE_SIZE;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0;
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page0;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);

    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page1;
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0;
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    _hFlashStorage.Configuration.Cache = NULL;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    _hFlashStorage.Configuration.Cache = _cache;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    _hFlashStorage.Configuration.CacheSize = 0;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_INVALID_CONFIGURATION);
    _hFlashStorage.Configuration.CacheSize = FLASH_STORAGE_TEST_CACHE_MAX;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);


    /* Initialized. */


    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(NULL, NULL, NULL, 0) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, NULL, NULL, 0) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record04Name, NULL, 0) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record04Name, &_record04Content, 0) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record04Name, &_record04Content, 1) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record04Name, &_record04Content, 255) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record04Name, &_record04Content, 256) == FLASH_STORAGE_ERR_INVALID_PARAMETER);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record00Name, &_record04Content, FLASH_STORAGE_TEST_04_CONT_SIZE) == FLASH_STORAGE_ERR_INVALID_PARAMETER);


    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(NULL, NULL, NULL, 0, NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, NULL, NULL, 0, NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, NULL, 0, NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, 0, NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, 0, &_recordReadContentSize) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_EMPTY);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record00Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_INVALID_PARAMETER);


    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_RemoveRecord(NULL, NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_RemoveRecord(&_hFlashStorage, NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_RemoveRecord(&_hFlashStorage, &_record04Name) == FLASH_STORAGE_ERR_EMPTY);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_RemoveRecord(&_hFlashStorage, &_record00Name) == FLASH_STORAGE_ERR_INVALID_PARAMETER);


    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(NULL, NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);


    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsNames(NULL, NULL, 0) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsNames(&_hFlashStorage, NULL, 0) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsNames(&_hFlashStorage, _recordReadNames , 0) == FLASH_STORAGE_ERR_EMPTY);


    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(NULL, NULL, NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, NULL, NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_record04Name, NULL) == FLASH_STORAGE_ERR_INVALID_PARAMETER);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_record04Name, &_recordReadContentSize) == FLASH_STORAGE_ERR_EMPTY);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_record00Name, &_recordReadContentSize) == FLASH_STORAGE_ERR_INVALID_PARAMETER);


    _hFlashStorage.Internal.IsInitialised = false;

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record04Name, &_record04Content, FLASH_STORAGE_TEST_04_CONT_SIZE) == FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_RemoveRecord(&_hFlashStorage, &_record04Name) == FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsNames(&_hFlashStorage, _recordReadNames , _recordsCount) == FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_record04Name, &_recordReadContentSize) == FLASH_STORAGE_ERR_INVALID_INIT_STATUS);
}

static void FLASH_STORAGE_TEST_CheckInit(void)
{
    FLASH_STORAGE_TEST_INFO("%s test started.\r\n", __FUNCTION__);


    /* P0:P1 ERASED:ERASED */
    FLASH_STORAGE_TEST_InitVariables();

    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_TEST_PAGE_SIZE;
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0;
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    _hFlashStorage.Configuration.Cache = _cache;
    _hFlashStorage.Configuration.CacheSize = FLASH_STORAGE_TEST_CACHE_MAX;

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_WriteAllRecords(1); /* Write something before set status "ERASED". */


    /* P0:P1 ERASED:ERASED */
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0, 0xFFFF); /* Status "ERASED". Now written records is garbage and will be erased. */

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);

    FLASH_STORAGE_TEST_WriteAllRecords(2); /* Write "first" (1) and "following" (2) records. */
    size_t record04Size = FLASH_STORAGE_TEST_04_FIR_SIZE + FLASH_STORAGE_TEST_04_SEC_SIZE;
    size_t record08Size = FLASH_STORAGE_TEST_08_FIR_SIZE + FLASH_STORAGE_TEST_08_SEC_SIZE;
    size_t recordFESize = FLASH_STORAGE_TEST_FE_FIR_SIZE + FLASH_STORAGE_TEST_FE_SEC_SIZE;
    uint32_t emptyOffset = FLASH_STORAGE_TEST_HEADER_SIZE + record04Size + record08Size + recordFESize;


    /* P0:P1 ACTIVE:ERASED */
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);


    /* P0:P1 ERASED:ACTIVE */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page1; /* Change pages, now active page is page 1. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page0;
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);


    /* P0:P1 TRANSIENT:ERASED */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0; /* Change pages, now active page is page 0. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0, 0xEEEE); /* Status "TRANSIENT". All records should be recovered. */
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);


    /* P0:P1 ERASED:TRANSIENT */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page1; /* Change pages, now active page is page 1. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page0;
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0, 0xEEEE); /* Status "TRANSIENT". All records should be recovered. */
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);


    /* P0:P1 TRANSIENT:TRANSIENT */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0; /* Change pages, now active page is page 0. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0, 0xEEEE); /* Status "TRANSIENT". All records should be recovered. */
    FLASH_STORAGE_TEST_CopyPage((uint32_t)_page0, (uint32_t)_page1); /* Both pages have same values. Page 0 should be recovered and page 1 - erased. */
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(_page1[0] == 0xFFFF); /* Page 1 erased. */


    /* P0:P1 ACTIVE:TRANSIENT */
    FLASH_STORAGE_TEST_CopyPage((uint32_t)_page0, (uint32_t)_page1); /* Both pages have same values. */
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1, 0xEEEE); /* Status "TRANSIENT". Page 1 should be erased. */
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(_page1[0] == 0xFFFF); /* Page 1 erased. */


    /* P0:P1 TRANSIENT:ACTIVE */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page1; /* Change pages, now active page is page 1. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page0;
    FLASH_STORAGE_TEST_CopyPage((uint32_t)_page0, (uint32_t)_page1); /* Both pages have same values. */
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1, 0xEEEE); /* Status "TRANSIENT". Page 1 should be erased. */
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(_page1[0] == 0xFFFF); /* Page 1 erased. */


    /* P0:P1 ACTIVE:ACTIVE */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0; /* Change pages, now active page is page 0. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    FLASH_STORAGE_TEST_CopyPage((uint32_t)_page0, (uint32_t)_page1); /* Both pages have same values. Page 1 should be erased. */
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(_page1[0] == 0xFFFF); /* Page 1 erased. */


    /* P0:P1 ACTIVE:OTHER */
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1, 0x1234);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(_page1[0] == 0xFFFF); /* Page 1 erased. */


    /* P0:P1 OTHER:ACTIVE */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page1; /* Change pages, now active page is page 1. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page0;
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1, 0x1234);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(_page1[0] == 0xFFFF); /* Page 1 erased. */


    /* P0:P1 TRANSIENT:OTHER */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0; /* Change pages, now active page is page 1. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0, 0xEEEE);
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1, 0x1234);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(_page1[0] == 0xFFFF); /* Page 1 erased. */


    /* P0:P1 OTHER:TRANSIENT */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page1; /* Change pages, now active page is page 1. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page0;
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0, 0xEEEE);
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1, 0x1234);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(_page1[0] == 0xFFFF); /* Page 1 erased. */


    /* P0:P1 OTHER:ERASED */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0; /* Change pages, now active page is page 0. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0, 0x1234);
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1, 0xFFFF);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + FLASH_STORAGE_TEST_HEADER_SIZE);
    FLASH_STORAGE_TEST_ASSERT(_page1[0] == 0xFFFF); /* Page 1 erased. */

    FLASH_STORAGE_TEST_WriteAllRecords(1); /* Write something before set status "0x1234". */


    /* P0:P1 ERASED:OTHER */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page1; /* Change pages, now active page is page 1. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page0;
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0, 0x1234);
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1, 0xFFFF);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page1 + FLASH_STORAGE_TEST_HEADER_SIZE);
    FLASH_STORAGE_TEST_ASSERT(_page0[0] == 0xFFFF); /* Page 0 erased. */

    FLASH_STORAGE_TEST_WriteAllRecords(1); /* Write something before set status "0x1234". */


    /* P0:P1 OTHER:OTHER */
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0; /* Change pages, now active page is page 0. */
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0, 0x1234);
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1, 0x1234);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + FLASH_STORAGE_TEST_HEADER_SIZE);

    FLASH_STORAGE_TEST_WriteAllRecords(2);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(_page1[0] == 0xFFFF); /* Page 1 erased. */


    /* Corrupted memory P0 ACTIVE*/
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0 + emptyOffset, 0x0000); /* Write "0x0000" in empty space. This should initiate transfer to another page. */
    emptyOffset = FLASH_STORAGE_TEST_HEADER_SIZE + FLASH_STORAGE_TEST_04_FIR_SIZE + FLASH_STORAGE_TEST_08_FIR_SIZE + FLASH_STORAGE_TEST_FE_FIR_SIZE;

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page1 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(_page0[0] == 0xFFFF); /* Page 0 erased. */


    /* Corrupted memory P1 ACTIVE*/
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1 + emptyOffset, 0x0000); /* Write "0x0000" in empty space. This should initiate transfer to another page. */

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(_page1[0] == 0xFFFF); /* Page 1 erased. */


    /* Corrupted CRC P0 ACTIVE*/
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0 + emptyOffset - 2, 0x0000); /* Write bad CRC32 for Record FE. This should initiate transfer to another page. */
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page0 + emptyOffset, 0x0000);
    emptyOffset = FLASH_STORAGE_TEST_HEADER_SIZE + FLASH_STORAGE_TEST_04_FIR_SIZE + FLASH_STORAGE_TEST_08_FIR_SIZE;

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 2);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 2);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 2);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_1);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page1 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_RECORD_NOT_FOUND);


    /* Corrupted CRC P1 ACTIVE*/
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1 + emptyOffset - 2, 0x0000); /* Write bad CRC32 for Record 08. This should initiate transfer to another page. */
    FLASH_STORAGE_TEST_WriteAtAddress((uint32_t)_page1 + emptyOffset, 0x0000);
    emptyOffset = FLASH_STORAGE_TEST_HEADER_SIZE + FLASH_STORAGE_TEST_04_FIR_SIZE;

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 1);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 1);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 1);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
    FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_RECORD_NOT_FOUND);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_RECORD_NOT_FOUND);
}

static void FLASH_STORAGE_TEST_CheckReadWriteRemove(void)
{
    FLASH_STORAGE_TEST_INFO("%s test started.\r\n", __FUNCTION__);

    FLASH_STORAGE_TEST_InitVariables();

    /* Writes records with same name and size several times. */

    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_TEST_PAGE_SIZE;
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0;
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    _hFlashStorage.Configuration.Cache = _cache;
    _hFlashStorage.Configuration.CacheSize = 2;

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    uint32_t emptyOffset = FLASH_STORAGE_TEST_HEADER_SIZE;

    for (uint32_t i = 0; i < 3; i++)
    {
        for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_04_CONT_SIZE; idx++)
        {
            _record04Content[idx] = (uint8_t)idx + i;
        }

        emptyOffset += i == 0 ? FLASH_STORAGE_TEST_04_FIR_SIZE : FLASH_STORAGE_TEST_04_SEC_SIZE;

        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record04Name, &_record04Content, FLASH_STORAGE_TEST_04_CONT_SIZE) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordsCount == 1);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 1);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 1);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsNames(&_hFlashStorage, _recordReadNames , _recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[0], &_record04Name, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_record04Name, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    }

    for (uint32_t i = 0; i < 3; i++)
    {
        for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_08_CONT_SIZE; idx++)
        {
            _record08Content[idx] = (uint8_t)idx + i;
        }

        emptyOffset += i == 0 ? FLASH_STORAGE_TEST_08_FIR_SIZE : FLASH_STORAGE_TEST_08_SEC_SIZE;

        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record08Name, &_record08Content, FLASH_STORAGE_TEST_08_CONT_SIZE) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordsCount == 2);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 0);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 2);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsNames(&_hFlashStorage, _recordReadNames , _recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[0], &_record04Name, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[1], &_record08Name, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_record08Name, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    }

    for (uint32_t i = 0; i < 3; i++)
    {
        for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_FE_CONT_SIZE; idx++)
        {
            _recordFEContent[idx] = (uint8_t)idx + i;
        }

        emptyOffset += i == 0 ? FLASH_STORAGE_TEST_FE_FIR_SIZE : FLASH_STORAGE_TEST_FE_SEC_SIZE;

        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_recordFEName, &_recordFEContent, FLASH_STORAGE_TEST_FE_CONT_SIZE) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 1);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsNames(&_hFlashStorage, _recordReadNames , _recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[0], &_record04Name, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[1], &_record08Name, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[2], &_recordFEName, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_recordFEName, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    }


    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_RemoveRecord(&_hFlashStorage, &_record04Name) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 2);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_RemoveRecord(&_hFlashStorage, &_record08Name) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 1);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_RemoveRecord(&_hFlashStorage, &_recordFEName) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 0);


    FLASH_STORAGE_TEST_InitVariables();

    /* Writes records with same name and different size several times. */

    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_TEST_PAGE_SIZE;
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0;
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    _hFlashStorage.Configuration.Cache = _cache;
    _hFlashStorage.Configuration.CacheSize = FLASH_STORAGE_TEST_CACHE_MAX;

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    emptyOffset = FLASH_STORAGE_TEST_HEADER_SIZE;

    for (uint32_t i = 0; i < 2; i++)
    {
        for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_04_CONT_SIZE; idx++)
        {
            _record04Content[idx] = (uint8_t)idx + i;
        }

        emptyOffset += (FLASH_STORAGE_TEST_04_FIR_SIZE - (i * 2));

        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record04Name, &_record04Content, FLASH_STORAGE_TEST_04_CONT_SIZE - (i * 2)) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordsCount == 1);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == 1 + i);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 1 + i);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsNames(&_hFlashStorage, _recordReadNames , _recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[0], &_record04Name, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_record04Name, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE - (i * 2));
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE - (i * 2));
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    }

    for (uint32_t i = 0; i < 3; i++)
    {
        for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_08_CONT_SIZE; idx++)
        {
            _record08Content[idx] = (uint8_t)idx + i;
        }

        emptyOffset += (FLASH_STORAGE_TEST_08_FIR_SIZE - (i * 2));

        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record08Name, &_record08Content, FLASH_STORAGE_TEST_08_CONT_SIZE - (i * 2)) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordsCount == 2);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == (i == 0 ? 3 : i == 1 ? 0 : 1));
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 3 + i);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsNames(&_hFlashStorage, _recordReadNames , _recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[0], &_record04Name, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[1], &_record08Name, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_record08Name, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE - (i * 2));
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE - (i * 2));
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    }

    for (uint32_t i = 0; i < 3; i++)
    {
        for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_FE_CONT_SIZE; idx++)
        {
            _recordFEContent[idx] = (uint8_t)idx + i;
        }

        emptyOffset += (FLASH_STORAGE_TEST_FE_FIR_SIZE - (i * 2));

        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_recordFEName, &_recordFEContent, FLASH_STORAGE_TEST_FE_CONT_SIZE - (i * 2)) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.CacheIndex == (i == 0 ? 2 : i == 1 ? 3 : 0));
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.NextRecordId == 6 + i);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.ActivePage == FLASH_STORAGE_PAGE_0);
        FLASH_STORAGE_TEST_ASSERT(_hFlashStorage.Internal.EmptyAddress == (uint32_t)_page0 + emptyOffset);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsNames(&_hFlashStorage, _recordReadNames , _recordsCount) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[0], &_record04Name, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[1], &_record08Name, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadNames[2], &_recordFEName, sizeof(FLASH_STORAGE_RecordNameTypeDef)) == 0);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_recordFEName, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE - (i * 2));
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE - (i * 2));
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    }


    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_RemoveRecord(&_hFlashStorage, &_record04Name) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 2);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_RemoveRecord(&_hFlashStorage, &_record08Name) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 1);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_RemoveRecord(&_hFlashStorage, &_recordFEName) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 0);


    FLASH_STORAGE_TEST_InitVariables();

    /* Writes records several times with several transients. */

    _hFlashStorage.Configuration.PageSize = FLASH_STORAGE_TEST_PAGE_SIZE;
    _hFlashStorage.Configuration.Page0Address = (uint32_t)_page0;
    _hFlashStorage.Configuration.Page1Address = (uint32_t)_page1;
    _hFlashStorage.Configuration.Cache = _cache;
    _hFlashStorage.Configuration.CacheSize = 1;

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_Initialise(&_hFlashStorage) == FLASH_STORAGE_ERR_OK);

    FLASH_STORAGE_TEST_WriteAllRecords(33);

    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordsCount(&_hFlashStorage, &_recordsCount) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordsCount == 3);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);
    FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
    FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
    FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
}


static void FLASH_STORAGE_TEST_WriteAllRecords(uint32_t writeTimes)
{
    /* Write all records (04, 08 and FE) some times and check results. */

    for (uint32_t i = 0; i < writeTimes; i++)
    {
        for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_04_CONT_SIZE; idx++)
        {
            _record04Content[idx] = (uint8_t)idx + i;
        }

        for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_08_CONT_SIZE; idx++)
        {
            _record08Content[idx] = (uint8_t)idx + i;
        }

        for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_FE_CONT_SIZE; idx++)
        {
            _recordFEContent[idx] = (uint8_t)idx + i;
        }

        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record04Name, &_record04Content, FLASH_STORAGE_TEST_04_CONT_SIZE) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_record04Name, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record04Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_04_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record04Content, _recordReadContentSize) == 0);

        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_record08Name, &_record08Content, FLASH_STORAGE_TEST_08_CONT_SIZE) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_record08Name, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_record08Name, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_08_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_record08Content, _recordReadContentSize) == 0);

        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_WriteRecord(&_hFlashStorage, &_recordFEName, &_recordFEContent, FLASH_STORAGE_TEST_FE_CONT_SIZE) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_GetRecordSize(&_hFlashStorage, &_recordFEName, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(FLASH_STORAGE_ReadRecord(&_hFlashStorage, &_recordFEName, &_recordReadBuffer, FLASH_STORAGE_TEST_READ_SIZE, &_recordReadContentSize) == FLASH_STORAGE_ERR_OK);
        FLASH_STORAGE_TEST_ASSERT(_recordReadContentSize == FLASH_STORAGE_TEST_FE_CONT_SIZE);
        FLASH_STORAGE_TEST_ASSERT(memcmp(&_recordReadBuffer, &_recordFEContent, _recordReadContentSize) == 0);
    }
}


static void FLASH_STORAGE_TEST_InitVariables(void)
{
    /* Erase pages, fill buffers with names and content of records. */

    #ifdef FLASH_STORAGE_USE_RAM_INSTEAD_OF_FLASH

    memset(_page0, FLASH_STORAGE_TEST_ERASED_U8, FLASH_STORAGE_TEST_PAGE_SIZE);
    memset(_page1, FLASH_STORAGE_TEST_ERASED_U8, FLASH_STORAGE_TEST_PAGE_SIZE);

    #else

    bool p0EraseNeeded = false;
    bool p1EraseNeeded = false;

    uint32_t currentAddress = FLASH_STORAGE_TEST_PAGE_0_ADDR;
    uint32_t nextPageAddress = FLASH_STORAGE_TEST_PAGE_0_ADDR + FLASH_STORAGE_TEST_PAGE_SIZE;

    while (currentAddress < nextPageAddress)
    {
        if (*(const volatile uint32_t *)(currentAddress) != 0xFFFFFFFF)
        {
            p0EraseNeeded = true;

            break;
        }

        currentAddress += sizeof(uint32_t);
    }

    currentAddress = FLASH_STORAGE_TEST_PAGE_1_ADDR;
    nextPageAddress = FLASH_STORAGE_TEST_PAGE_1_ADDR + FLASH_STORAGE_TEST_PAGE_SIZE;

    while (currentAddress < nextPageAddress)
    {
        if (*(const volatile uint32_t *)(currentAddress) != 0xFFFFFFFF)
        {
            p0EraseNeeded = true;

            break;
        }

        currentAddress += sizeof(uint32_t);
    }

    if (p0EraseNeeded || p1EraseNeeded)
    {
        HAL_FLASH_Unlock();

        FLASH_EraseInitTypeDef eraseInit =
        {
            .TypeErase = FLASH_TYPEERASE_PAGES,
            .NbPages = 1
        };

        uint32_t pageError = 0; /* Error page address */

        if (p0EraseNeeded)
        {
            eraseInit.PageAddress = FLASH_STORAGE_TEST_PAGE_0_ADDR;
            FLASH_STORAGE_TEST_ASSERT(HAL_FLASHEx_Erase(&eraseInit, &pageError) == HAL_OK);
        }

        if (p1EraseNeeded)
        {
            eraseInit.PageAddress = FLASH_STORAGE_TEST_PAGE_1_ADDR;
            FLASH_STORAGE_TEST_ASSERT(HAL_FLASHEx_Erase(&eraseInit, &pageError) == HAL_OK);
        }

        HAL_FLASH_Lock();
    }

    #endif

    memset(&_hFlashStorage, 0, sizeof(FLASH_STORAGE_HandleTypeDef));
    memset(&_cache, 0, (sizeof(FLASH_STORAGE_RecordDescriptorTypeDef) * FLASH_STORAGE_TEST_CACHE_MAX));

    memset(_record00Name.Name, '\0', sizeof(FLASH_STORAGE_RecordNameTypeDef));
    memcpy(_record04Name.Name, "Record04", sizeof(FLASH_STORAGE_RecordNameTypeDef));
    memcpy(_record08Name.Name, "Record08", sizeof(FLASH_STORAGE_RecordNameTypeDef));
    memcpy(_recordFEName.Name, "RecordFE", sizeof(FLASH_STORAGE_RecordNameTypeDef));

    for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_04_CONT_SIZE; idx++)
    {
        _record04Content[idx] = idx;
    }

    for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_08_CONT_SIZE; idx++)
    {
        _record08Content[idx] = idx;
    }

    for (uint8_t idx = 0; idx < FLASH_STORAGE_TEST_FE_CONT_SIZE; idx++)
    {
        _recordFEContent[idx] = idx;
    }

    memset(_recordReadBuffer, 0, FLASH_STORAGE_TEST_READ_SIZE);

    _recordReadContentSize = 0;

    _recordsCount = 0;

    memset(_recordReadNames, 0, (sizeof(FLASH_STORAGE_RecordNameTypeDef) * FLASH_STORAGE_TEST_READ_NAMES));
}

static void FLASH_STORAGE_TEST_WriteAtAddress(uint32_t address, uint16_t value)
{
    #ifdef FLASH_STORAGE_USE_RAM_INSTEAD_OF_FLASH

    *((uint16_t *)address) = value;

    #else

    HAL_FLASH_Unlock();

    if (*(const volatile uint16_t *)address == 0xFFFF || value == 0x0000) /* Cell at address in flash is erased or value is 0x0000. */
    {
        FLASH_STORAGE_TEST_ASSERT(HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, address, value) == HAL_OK);
    }
    else /* Otherwise we need erase page to change value in cell at address. */
    {
        static uint16_t buffer[FLASH_STORAGE_TEST_PAGE_SIZE/sizeof(uint16_t)];

        bool atPage0 = (address >= FLASH_STORAGE_TEST_PAGE_0_ADDR) && (address < (FLASH_STORAGE_TEST_PAGE_0_ADDR + FLASH_STORAGE_TEST_PAGE_SIZE));
        bool atPage1 = (address >= FLASH_STORAGE_TEST_PAGE_1_ADDR) && (address < (FLASH_STORAGE_TEST_PAGE_1_ADDR + FLASH_STORAGE_TEST_PAGE_SIZE));

        FLASH_STORAGE_TEST_ASSERT(atPage0 || atPage1);

        uint32_t pageAddress = atPage0 ? FLASH_STORAGE_TEST_PAGE_0_ADDR : FLASH_STORAGE_TEST_PAGE_1_ADDR;

        memcpy(buffer, (uint16_t *)pageAddress, FLASH_STORAGE_TEST_PAGE_SIZE);

        FLASH_EraseInitTypeDef eraseInit =
        {
            .TypeErase = FLASH_TYPEERASE_PAGES,
            .PageAddress = pageAddress,
            .NbPages = 1
        };

        uint32_t pageError = 0; /* Error page address */

        FLASH_STORAGE_TEST_ASSERT(HAL_FLASHEx_Erase(&eraseInit, &pageError) == HAL_OK);

        *((uint16_t *)((uint32_t)buffer + (address - pageAddress))) = value;

        for (uint16_t idx = 0; idx < FLASH_STORAGE_TEST_PAGE_SIZE; idx += sizeof(uint16_t))
        {
            FLASH_STORAGE_TEST_ASSERT(HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, pageAddress + idx, *(uint16_t *)((uint32_t)buffer + idx)) == HAL_OK);
        }
    }

    HAL_FLASH_Lock();

    #endif
}

static void FLASH_STORAGE_TEST_CopyPage(uint32_t srcAddress, uint32_t dstAddress)
{
    #ifdef FLASH_STORAGE_USE_RAM_INSTEAD_OF_FLASH

    memcpy((uint16_t *)dstAddress, (uint16_t *)srcAddress, FLASH_STORAGE_TEST_PAGE_SIZE);

    #else

    HAL_FLASH_Unlock();

    uint32_t currentAddress = dstAddress;
    uint32_t nextPageAddress = dstAddress + FLASH_STORAGE_TEST_PAGE_SIZE;

    bool eraseNeeded = false;

    while (currentAddress < nextPageAddress)
    {
        if (*(const volatile uint32_t *)(currentAddress) != 0xFFFFFFFF)
        {
            eraseNeeded = true;

            break;
        }

        currentAddress += sizeof(uint32_t);
    }

    if (eraseNeeded)
    {
        FLASH_EraseInitTypeDef eraseInit =
        {
            .TypeErase = FLASH_TYPEERASE_PAGES,
            .PageAddress = dstAddress,
            .NbPages = 1
        };

        uint32_t pageError = 0; /* Error page address */

        FLASH_STORAGE_TEST_ASSERT(HAL_FLASHEx_Erase(&eraseInit, &pageError) == HAL_OK);
    }

    for (uint16_t idx = 0; idx < FLASH_STORAGE_TEST_PAGE_SIZE; idx += sizeof(uint16_t))
    {
        FLASH_STORAGE_TEST_ASSERT(HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, dstAddress + idx, *(const volatile uint16_t *)(srcAddress + idx)) == HAL_OK);
    }

    HAL_FLASH_Lock();

    #endif
}


/* For better performance input buffer should be 4-bit aligned. */
/* Poly: 0x4C11DB7, Init: 0xFFFFFFFF, RefIn: true, RefOut: true, XorOut: 0xFFFFFFFF, Check: 0xCBF43926 */
/* Based on: */
/* http://we.easyelectronics.ru/STM32/crc32-na-stm32-kak-na-kompe-ili-na-kompe-kak-na-stm32.html */
/* stm32f1xx_hal_crc.c HAL_CRC_Calculate function. */
/* Test here: http://www.sunshine2k.de/coding/javascript/crc/crc_js.html (main CRC32).*/
static uint32_t FLASH_STORAGE_TEST_CalculateCrc32(CRC_HandleTypeDef *hcrc, __I uint8_t pBuffer[], uint32_t bufferLength)
{
    uint32_t temp = 0U; /* CRC output (read from hcrc->Instance->DR register) */

    uint32_t bufferAddress = (uint32_t)pBuffer;
    uint32_t bufferValue = 0;
    size_t len32 = bufferLength >> 2;

    /* Change CRC peripheral state */
    hcrc->State = HAL_CRC_STATE_BUSY;

    /* Reset CRC Calculation Unit (hcrc->Instance->INIT is written in hcrc->Instance->DR) */
    __HAL_CRC_DR_RESET(hcrc);

    /* Enter 32-bit input data to the CRC calculator */
    uint8_t alignment = ((bufferAddress % 4) == 0) ? 4 : (bufferAddress % 2) == 0 ? 2 : 1;

    switch (alignment)
    {
        case 4:
        {
            while (len32--)
            {
                bufferValue = *(uint32_t *)bufferAddress;

                hcrc->Instance->DR = __RBIT(bufferValue);

                bufferAddress += 4;
            }

            bufferValue = *(uint32_t *)bufferAddress;
        }
        break;

        case 2:
        {
            while (len32--)
            {
                bufferValue = *(uint16_t *)(bufferAddress + 2);
                bufferValue <<= 16;
                bufferValue += *(uint16_t *)(bufferAddress);

                hcrc->Instance->DR = __RBIT(bufferValue);

                bufferAddress += 4;
            }

            bufferValue = *(uint16_t *)(bufferAddress + 2);
            bufferValue <<= 16;
            bufferValue += *(uint16_t *)(bufferAddress);
        }
        break;

        default:
        {
            FLASH_STORAGE_TEST_ASSERT_FAILED(); /* Should never get here - address alignment must be 4 or 2. */

            while (len32--)
            {
                bufferValue = *(uint8_t *)(bufferAddress + 3);
                bufferValue <<= 8;
                bufferValue += *(uint8_t *)(bufferAddress + 2);
                bufferValue <<= 8;
                bufferValue += *(uint8_t *)(bufferAddress + 1);
                bufferValue <<= 8;
                bufferValue += *(uint8_t *)(bufferAddress);

                hcrc->Instance->DR = __RBIT(bufferValue);

                bufferAddress += 4;
            }

            bufferValue = *(uint8_t *)(bufferAddress + 3);
            bufferValue <<= 8;
            bufferValue += *(uint8_t *)(bufferAddress + 2);
            bufferValue <<= 8;
            bufferValue += *(uint8_t *)(bufferAddress + 1);
            bufferValue <<= 8;
            bufferValue += *(uint8_t *)(bufferAddress);
        }
        break;
    }

    temp = __RBIT(hcrc->Instance->DR);
    size_t len8 = bufferLength % 4;

    /* Enter 8-bit remainder (if exists).*/
    if (len8)
    {
        hcrc->Instance->DR = __RBIT(temp);

        switch (len8)
        {
            case 1:
            {
                hcrc->Instance->DR = __RBIT((bufferValue & 0x000000FF) ^ temp) >> 24;
                temp = (temp >> 8) ^ __RBIT(hcrc->Instance->DR);
            }
            break;

            case 2:
            {
                hcrc->Instance->DR = (__RBIT((bufferValue & 0x0000FFFF) ^ temp) >> 16);
                temp = (temp >> 16) ^ __RBIT(hcrc->Instance->DR);
            }
            break;

            case 3:
            {
                hcrc->Instance->DR = __RBIT((bufferValue & 0x00FFFFFF) ^ temp) >> 8;
                temp = (temp >> 24) ^ __RBIT(hcrc->Instance->DR);
            }
            break;
        }
    }

    /* Change CRC peripheral state */
    hcrc->State = HAL_CRC_STATE_READY;

    /* Return the CRC computed value */
    return ~temp;
}


__NO_RETURN static void FLASH_STORAGE_TEST_AssertFailed(const char *pFunctionName, uint32_t line)
{
    TRACE_Format("Assert failed in %s at line %u.\r\n", pFunctionName, line);

    while (true)
    {
        GPIO_TogglePin_LED_STATE();
    }
}


/* Weak function. */
FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_HAL_Write(uint32_t writeAddress, const volatile uint16_t buffer[], uint_fast16_t bufferLength)
{
    FLASH_STORAGE_TEST_TRACE("%s invoked.\r\n", __FUNCTION__);

    #ifdef FLASH_STORAGE_USE_RAM_INSTEAD_OF_FLASH

    uint32_t page0Address = (uint32_t)_page0;
    uint32_t page1Address = (uint32_t)_page1;

    #else

    uint32_t page0Address = FLASH_STORAGE_TEST_PAGE_0_ADDR;
    uint32_t page1Address = FLASH_STORAGE_TEST_PAGE_1_ADDR;

    #endif

    bool atPage0 = (writeAddress >= page0Address) && (writeAddress < (page0Address + FLASH_STORAGE_TEST_PAGE_SIZE));
    bool atPage1 = (writeAddress >= page1Address) && (writeAddress < (page1Address + FLASH_STORAGE_TEST_PAGE_SIZE));

    if (atPage0 && (writeAddress + (bufferLength * sizeof(uint16_t))) > (page0Address + FLASH_STORAGE_TEST_PAGE_SIZE))
    {
        FLASH_STORAGE_TEST_ASSERT_FAILED();
    }
    else if (atPage1 && (writeAddress + (bufferLength * sizeof(uint16_t))) > (page1Address + FLASH_STORAGE_TEST_PAGE_SIZE))
    {
        FLASH_STORAGE_TEST_ASSERT_FAILED();
    }
    else if (!atPage0 && !atPage1)
    {
        FLASH_STORAGE_TEST_ASSERT_FAILED();
    }


    #ifndef FLASH_STORAGE_USE_RAM_INSTEAD_OF_FLASH

    HAL_FLASH_Unlock();

    #endif

    for (uint_fast16_t idx = 0; idx < bufferLength; idx++)
    {
        #ifdef FLASH_STORAGE_USE_RAM_INSTEAD_OF_FLASH

        *((uint16_t *)writeAddress + idx) = buffer[idx];

        #else

        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, writeAddress + (idx * sizeof(uint16_t)), buffer[idx]) != HAL_OK)
        {
            HAL_FLASH_Lock();

            return FLASH_STORAGE_ERR_HAL_WRITE;
        }

        if (*((const volatile uint16_t *)writeAddress + idx) != buffer[idx])
        {
            HAL_FLASH_Lock();

            return FLASH_STORAGE_ERR_HAL_WRITE;
        }

        #endif
    }

    #ifndef FLASH_STORAGE_USE_RAM_INSTEAD_OF_FLASH

    HAL_FLASH_Lock();

    #endif

    return FLASH_STORAGE_ERR_OK;
}

/* Weak function. */
FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_HAL_CalculateCrc32(const volatile uint8_t buffer[], uint_fast16_t bufferLength, uint32_t *pCrc32)
{
    FLASH_STORAGE_TEST_TRACE("%s invoked.\r\n", __FUNCTION__);

    if (((uint32_t)buffer % 2) != 0)
    {
        FLASH_STORAGE_TEST_ASSERT_FAILED();
    }

    *pCrc32 = FLASH_STORAGE_TEST_CalculateCrc32(&hcrc, buffer, bufferLength);

    return FLASH_STORAGE_ERR_OK;
}

/* Weak function. */
FLASH_STORAGE_ErrorTypeDef FLASH_STORAGE_HAL_ErasePage(uint32_t pageAddress)
{
    FLASH_STORAGE_TEST_TRACE("%s invoked.\r\n", __FUNCTION__);

    #ifdef FLASH_STORAGE_USE_RAM_INSTEAD_OF_FLASH

    uint32_t page0Address = (uint32_t)_page0;
    uint32_t page1Address = (uint32_t)_page1;

    if (pageAddress == page0Address)
    {
        memset(_page0, FLASH_STORAGE_TEST_ERASED_U8, FLASH_STORAGE_TEST_PAGE_SIZE);
    }
    else if (pageAddress == page1Address)
    {
        memset(_page1, FLASH_STORAGE_TEST_ERASED_U8, FLASH_STORAGE_TEST_PAGE_SIZE);
    }
    else
    {
        FLASH_STORAGE_TEST_ASSERT_FAILED();
    }

    #else

    if (pageAddress == FLASH_STORAGE_TEST_PAGE_0_ADDR || pageAddress == FLASH_STORAGE_TEST_PAGE_1_ADDR)
    {
        FLASH_EraseInitTypeDef eraseInit =
        {
            .TypeErase = FLASH_TYPEERASE_PAGES,
            .PageAddress = pageAddress,
            .NbPages = 1
        };

        uint32_t pageError = 0; /* Error page address */

        HAL_FLASH_Unlock();

        if (HAL_FLASHEx_Erase(&eraseInit, &pageError) != HAL_OK)
        {
            HAL_FLASH_Lock();

            return FLASH_STORAGE_ERR_HAL_ERASE;
        }

        HAL_FLASH_Lock();

        uint32_t currentAddress = pageAddress;
        uint32_t nextPageAddress = pageAddress + FLASH_STORAGE_TEST_PAGE_SIZE;

        while (currentAddress < nextPageAddress)
        {
            if (*(const volatile uint32_t *)(currentAddress) != 0xFFFFFFFF)
            {
                return FLASH_STORAGE_ERR_HAL_ERASE;
            }

            currentAddress += sizeof(uint32_t);
        }
    }
    else
    {
        FLASH_STORAGE_TEST_ASSERT_FAILED();
    }

    #endif

    return FLASH_STORAGE_ERR_OK;
}


/* Weak function. */
void FLASH_STORAGE_TraceFormat(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    TRACE_VFormat(fmt, args);

    va_end(args);
}

/* Weak function. */
void FLASH_STORAGE_AssertFailed(const char *pFileName, const char *pFunctionName, uint32_t line)
{
    (void)pFileName;

    FLASH_STORAGE_TEST_AssertFailed(pFunctionName, line);
}
