#ifndef __FLASH_STORAGE_TEST_H
#define __FLASH_STORAGE_TEST_H


#include <stdint.h>
#include "stm32f1xx_hal.h"


/* Allocates pages in RAM. Comment out for use flash pages at addresses below. */
#define FLASH_STORAGE_USE_RAM_INSTEAD_OF_FLASH


#define FLASH_STORAGE_TEST_PAGE_SIZE    FLASH_PAGE_SIZE

#define FLASH_STORAGE_TEST_PAGE_0_ADDR  ((uint32_t) 0x0800F800)

#define FLASH_STORAGE_TEST_PAGE_1_ADDR  ((uint32_t) 0x0800FC00)


__NO_RETURN void FLASH_STORAGE_TEST_Process(void);


#endif /* __FLASH_STORAGE_TEST_H */
